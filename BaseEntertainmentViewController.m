//
//  BaseEntertainmentViewController.m
//  Dash
//
//  Created by stplmacmini8 on 6/23/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import "BaseEntertainmentViewController.h"
#import "ViewController.h"
#import "TabBarMenuViewController.h"

@interface BaseEntertainmentViewController ()<MenuViewControllerDelegate>

@property (nonatomic, strong) TabBarMenuViewController * menuView;

@end

@implementation BaseEntertainmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)displayDiningHomeView
{
    NSString * nibName = (IS_IPHONE_5 ? @"ViewController" : @"ViewControlleri4");
    ViewController *diningHomeView =[[ViewController alloc] initWithNibName:nibName bundle:nil];
    [self performSegueToViewController:diningHomeView];
}

- (void)menuButtonTapped
{
    if (self.menuView == nil) {
        self.menuView = [[TabBarMenuViewController alloc] initWithNibName:@"TabBarMenuViewController" bundle:nil];
        [self addChildViewController:self.menuView];
        [self.menuView didMoveToParentViewController:self];
    }
    [self.view addSubview:self.menuView.view];
    self.menuView.delegate = self;
}

- (void)displayNotificationView
{
    NSString * nibName = (IS_IPHONE_5 ? @"NotificationViewController" : @"NotificationViewControlleri4" );
    
    NotificationViewController * notfctnView =[[NotificationViewController alloc] initWithNibName:nibName bundle:nil];
    [self performSegueToViewController:notfctnView];
}

#pragma mark MenuViewController delegate methods

- (void)searchDailyDealsTapped
{
//    [self displayDailySearchView];
}

- (void)contactUsTapped
{
//    [self displayContactsView];
}


- (void)searchRestaurantsTapped
{
//    [self displayRestaurantSearchView];
}


- (void)settingsTapped
{
//    [self displaySettingsView];
}

- (void)manageNotificationTapped{
    
}
-(void)showNetConnectivityAlert
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Internet is not connected!" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    
    [alert show];
}

- (void)performSegueToViewController:(UIViewController *)viewController
{
    for (UIViewController *vc in [self.navigationController viewControllers]) {
        if ([vc isKindOfClass:[viewController class]])
        {
            [self.navigationController popToViewController:vc animated:YES];
            return;
        }
    }
    [self.navigationController pushViewController:viewController animated:YES];
}



@end
