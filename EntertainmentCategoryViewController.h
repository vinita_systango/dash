//
//  EntertainmentCategoryViewController.h
//  Dash
//
//  Created by stplmacmini8 on 6/16/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseEntertainmentViewController.h"

@interface EntertainmentCategoryViewController : BaseEntertainmentViewController

@end
