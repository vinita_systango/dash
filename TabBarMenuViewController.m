//
//  MenuViewController.m
//  Dash
//
//  Created by stplmacmini8 on 6/17/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import "TabBarMenuViewController.h"

@interface TabBarMenuViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *menuData;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomConstraint;

@end


NSInteger const kSettingsOption = 0;
NSInteger const kSearchRestaurantsOption = 1;
NSInteger const kSearchDailyDealsOption = 2;
NSInteger const kManageNotifications = 3;
NSInteger const kContactUsOption = 4;




@implementation TabBarMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupMenuData];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupMenuData
{
    NSString *searchMenu = [[singletonClass sharedInstance] isDiningSectionSelected] ? @"Search Restaurants" : @"Search Places";
    self.menuData = [[NSArray alloc] initWithObjects:@"Select City", searchMenu, @"Search Daily Deals",@"Notification Settings", @"Contact Us",nil];
    
    self.tableViewBottomConstraint.constant = (IS_IPHONE_5) ? 48 : 136;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self dismissViewFromSuperView];

}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (self.menuData.count);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"MenuTableCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = self.menuData[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self didSelectMenuAtRow:indexPath.row];
    [self dismissViewFromSuperView];
}

- (void)dismissViewFromSuperView
{
    [self.view removeFromSuperview];
}

- (void)didSelectMenuAtRow:(NSInteger)row
{
    switch (row) {
        case kSettingsOption:
            [self settingsOptionSelected];
            break;
        case kSearchRestaurantsOption:
            [self searchRestaurantsOptionSelected];
            break;
        case kSearchDailyDealsOption:
            [self searchDailyDealsOptionSelected];
            break;
        case kManageNotifications:
            [self manageNotificationsSelected];
            break;
        case kContactUsOption:
            [self contactUsOptionSelected];
            break;
        default:
            break;
    }
}

#pragma mark Invoke Delegate

- (void)settingsOptionSelected
{
    if ([self.delegate respondsToSelector:@selector(settingsTapped)]) {
        [self.delegate settingsTapped];
    }
}

- (void)searchRestaurantsOptionSelected
{
    if ([self.delegate respondsToSelector:@selector(searchRestaurantsTapped)]) {
        [self.delegate searchRestaurantsTapped];
    }
}

- (void)searchDailyDealsOptionSelected
{
    if ([self.delegate respondsToSelector:@selector(searchDailyDealsTapped)]) {
        [self.delegate searchDailyDealsTapped];
    }
}

- (void)contactUsOptionSelected
{
    if ([self.delegate respondsToSelector:@selector(contactUsTapped)]) {
        [self.delegate contactUsTapped];
    }
}
- (void)manageNotificationsSelected
{
    if ([self.delegate respondsToSelector:@selector(manageNotificationTapped)]) {
        [self.delegate manageNotificationTapped];
    }

    
}

@end
