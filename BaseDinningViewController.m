//
//  BaseDinningViewController.m
//  Dash
//
//  Created by stplmacmini8 on 6/16/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import "BaseDinningViewController.h"
#import "clientContactViewController.h"
#import "settingsViewController.h"
#import "DailyDealSearchViewController.h"
#import "EntertainmentCategoryViewController.h"
#import "TabBarMenuViewController.h"
#import "ViewController.h"
#import "restaurantViewController.h"
#import "JSBadgeView.h"
#import "NotificationSettingsViewController.h"
@interface BaseDinningViewController ()<MenuViewControllerDelegate>

@property (nonatomic, strong) TabBarMenuViewController * menuView;
@property (nonatomic, strong) JSBadgeView * badgeCountView;
@property (nonatomic, weak) IBOutlet UIButton * notifBtnInParent;

@end

@implementation BaseDinningViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateNotificationBadgeCount:[[RemoteNotificationManager sharedInstance] currentSectionBadge]];
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)displayNotificationView
{
    NSString * nibName = (IS_IPHONE_5 ? @"NotificationViewController" : @"NotificationViewControlleri4" );
    
    NotificationViewController * notfctnView =[[NotificationViewController alloc] initWithNibName:nibName bundle:nil];
    UIViewController *vc = [self performSegueToViewController:notfctnView];
   
    if([vc isKindOfClass:[NotificationViewController class]]) // notification view is already visible
    {
        // perform API to fetch data
        [(NotificationViewController *)vc getNotfcations];
        [[RemoteNotificationManager sharedInstance] clearNotificationTrayBlock:^{} clearAll:YES];
    }
}


- (void)displayContactsView
{
    NSString * nibName = (IS_IPHONE_5 ? @"clientContactViewController" : @"clientContactViewControlleri4" );
    clientContactViewController *clientContctview =[[clientContactViewController alloc] initWithNibName:nibName bundle:nil];
    [self performSegueToViewController:clientContctview];
}

- (void)displaySettingsView
{
    NSString * nibName = (IS_IPHONE_5 ? @"settingsViewController" : @"settingsViewControlleri4" );
    settingsViewController *settingsView =[[settingsViewController alloc] initWithNibName:nibName bundle:nil];
    [self performSegueToViewController:settingsView];
}

- (void)displayDailySearchView
{
    DailyDealSearchViewController * dailiesSearchView = [[DailyDealSearchViewController alloc] initWithNibName:@"DailyDealsSearchView" bundle:nil];
    [self performSegueToViewController:dailiesSearchView];
}

-(void)displayManageNotificationView
{
    NotificationSettingsViewController *notificationView=[[NotificationSettingsViewController alloc]initWithNibName:@"NotificationSettingsViewController" bundle:nil];
    [self performSegueToViewController:notificationView];

}
- (void)displayDiningHomeView
{
    NSString * nibName = (IS_IPHONE_5 ? @"ViewController" : @"ViewControlleri4");
    ViewController *diningHomeView =[[ViewController alloc] initWithNibName:nibName bundle:nil];
    [self performSegueToViewController:diningHomeView];
}

- (void)menuButtonTapped
{
    if (self.menuView == nil) {
        self.menuView = [[TabBarMenuViewController alloc] initWithNibName:@"TabBarMenuViewController" bundle:nil];
        [self addChildViewController:self.menuView];
        [self.menuView didMoveToParentViewController:self];
    }
    [self.view addSubview:self.menuView.view];
    self.menuView.delegate = self;
}

- (void)displayEntertainmentHomeView
{
    ViewController *catgryView;
    if (IS_IPHONE_5)
    {
        catgryView=[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    }
    else
    {
        catgryView=[[ViewController alloc] initWithNibName:@"ViewControlleri4" bundle:nil];
    }
    
    
    [self.navigationController pushViewController:catgryView animated:YES];
}

- (void)displayRestaurantSearchView
{
    [self checkInternetConectivity];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isAll"];
    [[NSUserDefaults standardUserDefaults]synchronize ];

    NSString * nibName = (IS_IPHONE_5 ? @"restaurantViewController" : @"restaurantViewControlleri4");
    restaurantViewController *restaurantView =[[restaurantViewController alloc] initWithNibName:nibName bundle:nil];
    restaurantView.categryId = 0;
    [self performSegueToViewController:restaurantView];
}

#pragma mark MenuViewController delegate methods

- (void)searchDailyDealsTapped
{
    [self displayDailySearchView];
}

- (void)contactUsTapped
{
    [self displayContactsView];
}


- (void)searchRestaurantsTapped
{
    [self displayRestaurantSearchView];
}


- (void)settingsTapped
{
    [self displaySettingsView];
}


- (void)manageNotificationTapped
{
    [self displayManageNotificationView];
}

- (void)checkInternetConectivity
{
    if(![[singletonClass sharedInstance] isInternetConnected])
    {
        [self showNetConnectivityAlert];
        return;
    }
}

-(void)showNetConnectivityAlert
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Internet is not connected!" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    
    [alert show];
}

- (UIViewController *)performSegueToViewController:(UIViewController *)viewController
{
    
    for (UIViewController *vc in [self.navigationController viewControllers]) {
        if ([vc isKindOfClass:[viewController class]])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController popToViewController:vc animated:YES];
            });
            
            return vc;
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:viewController animated:YES];
    });
    
    return nil;
}

- (void)updateNotificationBadgeCount:(int)sectionBadge
{
    if (self.badgeCountView == nil)
    {
        self.badgeCountView = [[JSBadgeView alloc] initWithParentView:self.notifBtnInParent alignment:JSBadgeViewAlignmentTopRight];
        self.badgeCountView.hidden = YES;
        self.badgeCountView .badgePositionAdjustment = CGPointMake(-20.0, 10.0);
    }
    
    self.badgeCountView.badgeText = [NSString stringWithFormat:@"%d", sectionBadge];
        
    self.badgeCountView.hidden = (sectionBadge == 0);
}

@end
