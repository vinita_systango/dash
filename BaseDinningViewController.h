//
//  BaseDinningViewController.h
//  Dash
//
//  Created by stplmacmini8 on 6/16/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseDinningViewController : UIViewController

- (void)displayNotificationView;
- (void)updateNotificationBadgeCount:(int)sectionBadge;

- (void)displayContactsView;

- (void)displaySettingsView;

- (void)displayDailySearchView;

- (void)displayDiningHomeView;

- (void)displayEntertainmentHomeView;

- (void)menuButtonTapped;

- (void)manageNotificationTapped;

-(void)displayManageNotificationView;

@end
