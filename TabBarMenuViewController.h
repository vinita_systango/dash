//
//  MenuViewController.h
//  Dash
//
//  Created by stplmacmini8 on 6/17/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuViewControllerDelegate <NSObject>

- (void)settingsTapped;
- (void)searchRestaurantsTapped;
- (void)searchDailyDealsTapped;
- (void)contactUsTapped;
- (void)manageNotificationTapped;


@end

@interface TabBarMenuViewController : UIViewController

@property (nonatomic, weak) id<MenuViewControllerDelegate> delegate;

@end
