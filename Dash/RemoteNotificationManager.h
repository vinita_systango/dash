//
//  RemoteNotificationManager.h
//  Dash
//
//  Copyright (c) 2014 Systango. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RemoteNotificationManager : NSObject

@property (nonatomic) int badge;
@property (nonatomic) int dBadge;
@property (nonatomic) int eBadge;

+ (RemoteNotificationManager *)sharedInstance;

- (void)clearNotificationTrayBlock:(void (^)(void))block clearAll:(BOOL)clearAll;

- (void)updateNotificationBadgeCountWithAppBadge:(NSInteger)appBadge sectionBadge:(int)sectionBadge block:(void (^)(void))block;

- (void)parseBadgesResponse:(NSMutableDictionary *)badgesInfo;

- (int)currentSectionBadge;

- (void)processNotificationUserInfo:(NSDictionary *)userInfo;

@end
