//
//  DialyDealSearchViewController.h
//  Dash
//
//  Created by stplmacmini8 on 6/5/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseDinningViewController.h"

@interface DailyDealSearchViewController : BaseDinningViewController<UIAlertViewDelegate>

@end
