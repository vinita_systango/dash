//
//  NSDictionary+HasValueForKey.m
//  Dash
//
//  Created by stplmacmini8 on 6/9/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import "NSDictionary+HasValueForKey.h"

@implementation NSDictionary (HasValueForKey)

- (BOOL)hasValueForKey:(NSString *)key
{
    if([self valueForKey:key] && [self valueForKey:key] != [NSNull alloc])
        return YES;
    else
        return NO;
}

- (id)getValueForKey:(NSString *)key{
    
    if ([self hasValueForKey:key]) {
        return [self objectForKey:key];
    }
    return nil;
}

@end
