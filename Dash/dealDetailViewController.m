//
//  dealDetailViewController.m
//  Dash
//
//  Created by CS_Mac4 on 15/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import "dealDetailViewController.h"
#import "restaurantDetailViewController.h"

@interface dealDetailViewController ()

@end

@implementation dealDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:NO];
    
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(callService)
                                        object:nil];
    [queue addOperation:operation];

}

-(void)viewWillDisappear:(BOOL)animated
{
    [advertsmntTimer invalidate];
}

-(void)getServerResponse
{
    
    NSMutableArray *weekDays=[[NSMutableArray alloc] initWithObjects:@"",@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday",@"Sunday", nil];
    NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"self" ascending: YES];
    
    if ([dailyDealDictionary[@"response"] count]==0)
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        return;
    }
    
    NSArray *keysArray= [[dailyDealDictionary[@"response"] allKeys] sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
    
    CGFloat dayLabelTopPos = 0;
    CGFloat lineTopPos = 0;
    UILabel *dayLabel = nil;
    UILabel *dealDescpritionLabel = nil;
    UILabel *lineLabel = nil;
    
    for (int i = 0; i<[dailyDealDictionary[@"response"] count]; i++)
    {
        dayLabelTopPos = (i == 0) ? 8 : CGRectGetMaxY(dealDescpritionLabel.frame) + 30;
        
        dayLabel = [[UILabel alloc]initWithFrame:CGRectMake(85, dayLabelTopPos, 150, 80)];
        
        dayLabel.text =[weekDays objectAtIndex:[[keysArray objectAtIndex:i] intValue]];
        
        dayLabel.adjustsFontSizeToFitWidth = YES;
        dayLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:20];
        dayLabel.minimumScaleFactor = 10.0f/12.0f;
        dayLabel.clipsToBounds = YES;
        dayLabel.backgroundColor = [UIColor clearColor];
        dayLabel.textColor = [UIColor blackColor];
        dayLabel.textAlignment = NSTextAlignmentCenter;
        [_dealsScrollvw addSubview:dayLabel];
        
        // Display only the data of @"deal_desc"
        for (int j=0; j<[[dailyDealDictionary [@"response"]objectForKey:[keysArray objectAtIndex:i]]count]; j++) {
            
            if(![[dailyDealDictionary [@"response"]objectForKey:[keysArray objectAtIndex:i]][j][@"deal_desc"]isEqualToString:@"" ])
            {
                // set divider line label
                if(j == 0) // 0th index
                {
                    lineTopPos = CGRectGetMaxY(dayLabel.frame) - 12;
                }
                else
                {
                    lineTopPos = CGRectGetMaxY(dealDescpritionLabel.frame) + 68;
                }
                
                lineLabel = [[UILabel alloc]initWithFrame:CGRectMake(144, lineTopPos, 32,2)];
                
                lineLabel.backgroundColor = [UIColor lightGrayColor];
                lineLabel.textAlignment = NSTextAlignmentCenter;
                [_dealsScrollvw addSubview:lineLabel];
                
                // set description label
                dealDescpritionLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, (lineTopPos + 2), 280, 0)];
                dealDescpritionLabel.text =[dailyDealDictionary [@"response"]objectForKey:[keysArray objectAtIndex:i]][j][@"deal_desc"];
                
                dealDescpritionLabel.adjustsFontSizeToFitWidth = YES;
                dealDescpritionLabel.clipsToBounds = YES;
                dealDescpritionLabel.backgroundColor = [UIColor clearColor];
                dealDescpritionLabel.textColor = [UIColor darkGrayColor];
                dealDescpritionLabel.textAlignment = NSTextAlignmentCenter;
                dealDescpritionLabel.numberOfLines = 0;
                dealDescpritionLabel.lineBreakMode = NSLineBreakByWordWrapping;
                
                [dealDescpritionLabel setHeightValue:[dealDescpritionLabel getLabelHeight]];
                
                [_dealsScrollvw addSubview:dealDescpritionLabel];
            }
            
        }
        
        _dealsScrollvw.contentSize=CGSizeMake(320, CGRectGetMaxY(dealDescpritionLabel.frame) + 40);
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
}


-(void)startAdvertisementTimer
{
    if ([[[BannerView sharedBannerInstance]lowCostAdvertisementImages][@"response"][0][@"id"]intValue]!=-1)
    {
        advertsmntTimer= [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(animateAdvertisements) userInfo:nil repeats:YES];
        
    }
}

-(void)animateAdvertisements
{
    [[BannerView sharedBannerInstance]animateAdvertisements:_advertseImgView ofType:NO];
    
}
-(void)callService
{
    [self.navigationController.navigationBar setHidden:NO];
    
    if ( ![self.paidStatus isEqualToString:@"PAID"])
    {
        
        [self performSelectorOnMainThread:@selector(startAdvertisementTimer) withObject:nil waitUntilDone:NO];

    }
  
   dailyDealDictionary= [[singletonClass sharedInstance] getDealDetail:[NSString stringWithFormat:@"%d",_restaurantId]];
    [self performSelectorOnMainThread:@selector(getServerResponse) withObject:nil waitUntilDone:NO];
 
}

- (IBAction)bckButtonClckd:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)popToHome
{
    [self .navigationController popToRootViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIButton *homeButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    
    [homeButton setBackgroundImage:[UIImage imageNamed:@"home_icon_black"] forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(popToHome) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *homeBarBtn=[[UIBarButtonItem alloc] initWithCustomView:homeButton];
    
    
    [self.navigationItem setRightBarButtonItem:homeBarBtn];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
       
    self.title=@"DAILY DEALS";
    
    
    if (IS_IPHONE_5
        )
    {
         _advertseImgView=[[BannerView sharedBannerInstance] getAdvertisementWithRect:CGRectMake(0, 440, 320, 66) OnView:self ];
    }
    else
    {
       _advertseImgView=[[BannerView sharedBannerInstance] getAdvertisementWithRect:CGRectMake(0, 350, 320, 66) OnView:self ];
    }
   
    [self.view addSubview:_advertseImgView];
   
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:_bckBtn]];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
