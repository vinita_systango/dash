//
//  customTableCell.h
//  Dash
//
//  Created by CS_Mac4 on 12/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIImageView+WebCache.h"

@interface customTableCell : UITableViewCell

@property(nonatomic,strong) AsyncImageView *asyncImageView;
@property(nonatomic,strong) UILabel *restaurantNameLabel;
@property(nonatomic,strong) UILabel *labelRestaurantName;
@property(nonatomic,strong) UILabel *availabilityNameLabel;
@property(nonatomic,strong) UILabel *timeLabel;
@property(nonatomic,strong) UILabel *addressLabel;
@property(nonatomic,strong) UILabel *categryLabel;

@property(nonatomic,strong) UIButton *notificationONOFF;
@end
