//
//  restaurantDetailViewController.m
//  Dash
//
//  Created by CS_Mac4 on 15/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import "restaurantDetailViewController.h"
#import "menuViewController.h"
#import "couponViewController.h"
#import "specltyViewController.h"
#import "dealDetailViewController.h"
#import "AsyncImageView.h"

NSString * const storeHoursUnavialableText = @"No Store Hours";

@interface restaurantDetailViewController ()

@property(nonatomic, assign) BOOL isFacebookUrlAvailable;
@property(nonatomic, assign) BOOL isTwitterUrlAvailable;
@property(nonatomic, assign) BOOL isWebsitekUrlAvailable;
@property(nonatomic, assign) BOOL isEmailAdressAvailable;


@end


@implementation restaurantDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
       [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(callService)
                                        object:nil];
    [queue addOperation:operation];
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [advertsmntTimer invalidate];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
     [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:
     UIStatusBarAnimationSlide];
}

-(void)popToHome
{
    [self .navigationController popToRootViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BannerView *bannerView=[BannerView sharedBannerInstance];
    bannerView.lowCostAdvertisementImages=nil;
    [bannerView fetchBannerLowCostImagesFromServer];
    

    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
      imgsCount=0;
    _categryName.text=_catNameString;
    
    socialView=[[socialViewController alloc] initWithNibName:@"socialViewController" bundle:nil];

    self.isFacebookUrlAvailable = YES;
    self.isTwitterUrlAvailable = YES;
    self.isWebsitekUrlAvailable = YES;
    self.isEmailAdressAvailable = YES;
    if (IS_IPHONE_5)
        {
            
            _advertseImgView=[[BannerView sharedBannerInstance] getAdvertisementWithRect:CGRectMake(0, 460, 320, 66) OnView:self];
        }
        else
        {
            _advertseImgView=[[BannerView sharedBannerInstance] getAdvertisementWithRect:CGRectMake(0, 370, 320, 66) OnView:self];
        }
    _advertseImgView.image=nil;
    _advertseImgView.backgroundColor=[UIColor whiteColor];
        [self.view addSubview:_advertseImgView];
    
   
}

-(void)startAdvertisementTimer
{
    if ([[[BannerView sharedBannerInstance]highCostAdvertisementImages][@"response"][0][@"id"]intValue]!=-1)
    {
        advertsmntTimer= [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(animateAdvertisements) userInfo:nil repeats:YES];
        
    }
}

-(void)animateAdvertisements
{
    [[BannerView sharedBannerInstance]animateAdvertisements:_advertseImgView ofType:YES];
    
}

-(void)callService
{
    
    NSString * selectedSectionId = [[singletonClass sharedInstance] getSelectedSectionId];
   
    restaurantDictionary=  [[singletonClass sharedInstance] getRestaurantDetail:[NSString stringWithFormat:@"%d",_restaurantId] :[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] :self.clickedBannerId : selectedSectionId];
    
    if (![restaurantDictionary[@"response"][0][@"paid_status"] isEqualToString:@"PAID"])
    {
        _advertseImgView.image=[UIImage imageNamed:@"IphoneBackdrop.png"];
        [self performSelectorOnMainThread:@selector(startAdvertisementTimer) withObject:nil waitUntilDone:NO];
           }
    NSLog(@"******%@******",restaurantDictionary[@"response"][0][@"paid_status"]);

    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self performSelectorOnMainThread:@selector(getServerResponse) withObject:nil waitUntilDone:NO];
    
   
    
    }

-(void)getServerResponse
{
    [_addressBtn.titleLabel setTextAlignment: NSTextAlignmentCenter];
     [_addressBtn setTitle:restaurantDictionary[@"response"][0][@"address"] forState:UIControlStateNormal];
    

    if ([restaurantDictionary[@"response"][0][@"is_menu_status"] isEqualToString:@"Y"])
    {
        NSString *imageName = [[singletonClass sharedInstance] isDiningSectionSelected] ? @"menu_btn" : @"Dollar_btn";
        
        [_menuBtn setUserInteractionEnabled:YES];
        [_menuBtn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    else
    {
        NSString *imageName = [[singletonClass sharedInstance] isDiningSectionSelected] ? @"menu_btn_hover" : @"Dollar_btn_hover";
        
        [_menuBtn setUserInteractionEnabled:NO];
        [_menuBtn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    
    
    if ([restaurantDictionary[@"response"][0][@"is_special_status"] isEqualToString:@"Y"])
    {
        NSString *imageName = [[singletonClass sharedInstance] isDiningSectionSelected] ? @"Special_btn" : @"Schedule_btn";
        
        [_specialsBtn setUserInteractionEnabled:YES];
        [_specialsBtn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    else
    {
        NSString *imageName = [[singletonClass sharedInstance] isDiningSectionSelected] ? @"Special_btn_hover" : @"Schedule_btn_hover";
        
        [_specialsBtn setUserInteractionEnabled:NO];
        [_specialsBtn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];

    }

    
    
    if ([restaurantDictionary[@"response"][0][@"is_rescoupon_status"] isEqualToString:@"Y"])
    {
        [_couponBtn setUserInteractionEnabled:YES];
        
        [_couponBtn setBackgroundImage:[UIImage imageNamed:@"coupan_btn"] forState:UIControlStateNormal];
    }
    else
    {
        
          [_couponBtn setBackgroundImage:[UIImage imageNamed:@"coupan_btn_hover"] forState:UIControlStateNormal];
    }

    
    
    if ([restaurantDictionary[@"response"][0][@"is_deal_status"] isEqualToString:@"Y"])
    {
        [_dailyBtn setUserInteractionEnabled:YES];
        
            [_dailyBtn setBackgroundImage:[UIImage imageNamed:@"daily_btn"] forState:UIControlStateNormal];
    }
    else
    {
        
        [_dailyBtn setUserInteractionEnabled:NO];
          [_dailyBtn setBackgroundImage:[UIImage imageNamed:@"daily_btn_hover"] forState:UIControlStateNormal];
    }

    
    if([restaurantDictionary[@"response"][0][@"reg_id"] intValue] ==-1)
    {
        [_fbButton setUserInteractionEnabled:NO];
        [_instgrmBtn setUserInteractionEnabled:NO];
        
        [_TwtrBtn setUserInteractionEnabled:NO];
        [_WbsteLnkBtn setUserInteractionEnabled:NO];
        [_pintrstBtn setUserInteractionEnabled:NO];
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"No profile data available for this restaurant" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        
        _pageCtrl.numberOfPages=0;
        return;
    }
    
    _addressLabel.text=restaurantDictionary[@"response"][0][@"address"];
    
    
    if ([restaurantDictionary[@"response"][0][@"facebookurl"] isEqualToString:@""])
    {
        self.isFacebookUrlAvailable = NO;
    }
    if ([restaurantDictionary[@"response"][0][@"instagramurl"] isEqualToString:@""])
    {
        [_instgrmBtn setUserInteractionEnabled:NO];
       
    }
    if ([restaurantDictionary[@"response"][0][@"twitterurl"] isEqualToString:@""])
    {
        self.isTwitterUrlAvailable = NO;
    }
    if ([restaurantDictionary[@"response"][0][@"websiteurl"] isEqualToString:@""])
    {
        self.isWebsitekUrlAvailable = NO;
    }
    
    if ([restaurantDictionary[@"response"][0][@"pinteristurl"] isEqualToString:@""])
    {
        [_pintrstBtn setUserInteractionEnabled:NO];
        
    }
    
    if ([restaurantDictionary[@"response"][0][@"email"] isEqualToString:@""])
    {
        self.isEmailAdressAvailable = NO;
    }
    
    
    
    [ _contctNumberBtn setTitle:restaurantDictionary[@"response"][0][@"phone_no"] forState:UIControlStateNormal];
    
    _pageCtrl.numberOfPages=[restaurantDictionary[@"response"][0][@"adv_img"]count];
    
    
    _restaurantNameLabel.text=restaurantDictionary[@"response"][0][@"res_nam"];
    
    int xValue=0;
    for (int i=0; i<[restaurantDictionary[@"response"][0][@"adv_img"]count]; i++)
    {
        AsyncImageView *spcltyImageView=[[AsyncImageView alloc] initWithFrame:CGRectMake(xValue, 0, 320,268)];
        
        
        NSString *UrlString = [Constant baseURLWithPath:[NSString stringWithFormat:@"cmsAdmin/panel/res_images/%@",restaurantDictionary[@"response"][0][@"adv_img"][i]]];
        
        UrlString=[UrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        spcltyImageView.imageURL=[NSURL URLWithString:UrlString];
        [_scrolView addSubview:spcltyImageView];
        xValue=xValue+320;
        
    }
    
    _scrolView.contentSize=CGSizeMake(xValue,  0);
 
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageNum = (int)(scrollView.contentOffset.x / scrollView.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                frame.size.width);
    _pageCtrl.currentPage=pageNum;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)singleTap:(id)sender
{
    restaurantDetailViewController *restaurantDetailView;
    
    if (IS_IPHONE_5)
    {
        restaurantDetailView=[[restaurantDetailViewController alloc] initWithNibName:@"restaurantDetailViewController" bundle:nil];
    }
    else
    {
        restaurantDetailView=[[restaurantDetailViewController alloc] initWithNibName:@"restaurantDetailViewControlleri4" bundle:nil];
        
    }

    NSMutableArray   *categoryArray = [[singletonClass sharedInstance] getCategories];
    
    
    if (imgsCount==0)
    {
        restaurantDetailView.catNameString=[categoryArray objectAtIndex:[_advertisementImages[@"response"][randomValue][@"catid"][0]intValue]];
        
        
        
        restaurantDetailView.restaurantId=[_advertisementImages[@"response"][randomValue][@"id"]intValue];
        
    
        
    }
    else
    {
        restaurantDetailView.catNameString=[categoryArray objectAtIndex:[_advertisementImages[@"response"][randomValue][@"catid"][0]intValue]];
        restaurantDetailView.restaurantId=[_advertisementImages[@"response"][randomValue][@"id"]intValue];
        
      
    }

    restaurantDetailView.clickedBannerId = _advertisementImages[@"response"][randomValue][@"add_id"];
    [self.navigationController pushViewController:restaurantDetailView animated:YES];
    

}

- (IBAction)menuButtonClicked:(id)sender
{
    menuViewController *menuView=[[menuViewController alloc] initWithNibName:@"menuViewController" bundle:nil];
    menuView.restaurantId=_restaurantId;
    menuView.paidStatus=restaurantDictionary[@"response"][0][@"paid_status"];
    [self.navigationController pushViewController:menuView animated:YES];
}

- (IBAction)couponButtonClicke:(id)sender
{
    couponViewController *couponView=[[couponViewController alloc] initWithNibName:@"couponViewController" bundle:nil];
    couponView.restaurantId=_restaurantId;
    couponView.paidStatus=restaurantDictionary[@"response"][0][@"paid_status"];
    [self.navigationController pushViewController:couponView animated:YES];
}

- (IBAction)specialtyBtnClckd:(id)sender
{
    specltyViewController *specltyView=[[specltyViewController alloc] initWithNibName:@"specltyViewController" bundle:nil];
    specltyView.restaurantId=_restaurantId;
    specltyView.paidStatus=restaurantDictionary[@"response"][0][@"paid_status"];
    [self.navigationController pushViewController:specltyView animated:YES];

}

- (IBAction)dailyButtonClicked:(id)sender
{
    dealDetailViewController *dealDetail=[[dealDetailViewController alloc] initWithNibName:@"dealDetailViewController" bundle:nil];
    dealDetail.restaurantId=_restaurantId;
    dealDetail.paidStatus=restaurantDictionary[@"response"][0][@"paid_status"];
    [self.navigationController pushViewController:dealDetail animated:YES];
    
}

- (IBAction)bckButtonClckd:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)fbBtnClckd:(id)sender
{
    if (self.isFacebookUrlAvailable) {
    socialView.socialUrlString=restaurantDictionary[@"response"][0][@"facebookurl"];
    [self.navigationController pushViewController:socialView animated:YES];
    }
    else
    {
        [self displayUnavailableServiceFor:@"Facebook URL is"];
    }


}

- (IBAction)callBtnClckd:(id)sender {
    NSString *phNo = [_contctNumberBtn currentTitle];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [calert show];
    }

}

- (IBAction)twterBtnClckd:(id)sender
{
    if (self.isTwitterUrlAvailable) {
        socialView.socialUrlString=restaurantDictionary[@"response"][0][@"twitterurl"];
        [self.navigationController pushViewController:socialView animated:YES];
    }
    else
    {
        [self displayUnavailableServiceFor:@"Twitter URL is"];
    }
    
}

- (IBAction)adressBtnClckd:(id)sender
{
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:restaurantDictionary[@"response"][0][@"address_link"]]])
    {
        //  NSLog(@"Failed to open url:");
    }
}

- (IBAction)bubbleBtnClckd:(id)sender
{
    [_bubbleImgVw removeFromSuperview];
}




- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    [self dismissViewControllerAnimated:YES
                             completion:nil];
    
}

- (IBAction)popToRoot:(id)sender
{
    [self .navigationController popToRootViewControllerAnimated:YES];
}

-(void)getStoreHoursData
{
    
    
    storeHoursDictionary=  [[singletonClass sharedInstance] getRestaurantHours:[NSString stringWithFormat:@"%d",_restaurantId]];
    [self performSelectorOnMainThread:@selector(getHoursServerResponse) withObject:nil waitUntilDone:NO];
}

-(void)getHoursServerResponse
{
    
        [spinner stopAnimating];
    spinner.hidden=YES;
    [spinner removeFromSuperview];
    
    NSLog(@"%@",storeHoursDictionary[@"response"][@"main_score"]);
    
    
    UILabel *restaurantTxtLabel ;
    int yValue;
    int titlefontSize;
    
    int fontSize;
    int yaddValue;
    
    if (IS_IPHONE_5) {
        restaurantTxtLabel = [[UILabel alloc]initWithFrame:CGRectMake(35, 72, 250, 50)];
        restaurantTxtLabel.font=[UIFont fontWithName:@"CourierNewPS-BoldMT" size:20];
    
        yValue=100;
        yaddValue=22;
        titlefontSize=20;
        fontSize =18;
    }
    else
    {
        restaurantTxtLabel = [[UILabel alloc]initWithFrame:CGRectMake(35, 53, 200, 50)];
        restaurantTxtLabel.font=[UIFont fontWithName:@"CourierNewPS-BoldMT" size:17];
        yValue=75;
        yaddValue=20;
        
        titlefontSize=18;
        fontSize =16;
    }
    
    restaurantTxtLabel.text =storeHoursDictionary[@"response"][@"main_title"];
    restaurantTxtLabel.textColor = [UIColor blackColor];
    [restaurantTxtLabel setTextAlignment:NSTextAlignmentCenter];
    
    
    [_bubbleImgVw addSubview:restaurantTxtLabel];
    

    
    if ([storeHoursDictionary[@"response"][@"id"]intValue] != -1)
    {
         NSLog(@"%d",[storeHoursDictionary[@"response"][@"id"]intValue]);
        NSArray *mainTimeScoreArr=[storeHoursDictionary[@"response"][@"main_score"] componentsSeparatedByString:@"\n"];
        NSLog(@"%@",mainTimeScoreArr);
        
        for (int i=0; i<[mainTimeScoreArr count]; i++)
        {
            UILabel *restaurantTxtLabel = [[UILabel alloc]initWithFrame:CGRectMake(25, yValue, 300, 50)];
            restaurantTxtLabel.text =[mainTimeScoreArr objectAtIndex:i];
            restaurantTxtLabel.textColor = [UIColor blackColor];
            restaurantTxtLabel.font=[UIFont fontWithName:@"CourierNewPSMT" size:fontSize];
            
            
            [_bubbleImgVw addSubview:restaurantTxtLabel];
            
            
            yValue=yValue+yaddValue;
        }
        [self.view addSubview: _bubbleImgVw];
    }
    else
    {
        [self displayUnavailableServiceFor:@"Store Hours are"];
        
    }
    
    
    UILabel *loungeTxtLabel;
    
     int loungeYvalue;
    if (IS_IPHONE_5)
    {
        loungeTxtLabel = [[UILabel alloc]initWithFrame:CGRectMake(35, 265, 250, 50)];
        loungeYvalue=290;

    }
    else
    {
        loungeTxtLabel = [[UILabel alloc]initWithFrame:CGRectMake(35, 215, 150, 50)];
        loungeYvalue=235;

    }
    loungeTxtLabel.text =storeHoursDictionary[@"response"][@"secondary_title"];
    loungeTxtLabel.textColor = [UIColor blackColor];
    loungeTxtLabel.font=[UIFont fontWithName:@"CourierNewPS-BoldMT" size:titlefontSize];
    [loungeTxtLabel setTextAlignment:NSTextAlignmentCenter];
    
    [_bubbleImgVw addSubview:loungeTxtLabel];
    
   
    
    if (![storeHoursDictionary[@"response"][@"secondary_score"] isEqualToString:@""])
    {
     NSArray *secndryimeScoreArr=[storeHoursDictionary[@"response"][@"secondary_score"] componentsSeparatedByString:@"\n"];
    
    for (int i=0; i<[secndryimeScoreArr count]; i++)
    {
        UILabel *restaurantTxtLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, loungeYvalue, 300, 50)];
        restaurantTxtLabel.text =[secndryimeScoreArr objectAtIndex:i];
        restaurantTxtLabel.textColor = [UIColor blackColor];
        restaurantTxtLabel.font=[UIFont fontWithName:@"CourierNewPSMT" size:fontSize];
        
        
        [_bubbleImgVw addSubview:restaurantTxtLabel];
        
        
        loungeYvalue=loungeYvalue+22;
    }
    }
    

}
- (IBAction)clockBtnClckd:(id)sender
{
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    spinner.center=_bubbleImgVw.center;
    [_bubbleImgVw addSubview:spinner];
    
    [spinner startAnimating];

    
    NSOperationQueue *nwqueue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(getStoreHoursData)
                                        object:nil];
    [nwqueue addOperation:operation];

    
    }

- (IBAction)websiteBtnClckd:(id)sender
{
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:restaurantDictionary[@"response"][0][@"websiteurl"]]])
    {
        //  NSLog(@"Failed to open url:");
    }
}
- (IBAction)emailBtnClckd:(id)sender
{
    //NSLog(@"%@",restaurantDictionary[@"response"][0][@"email"]);
    NSArray *usersTo = [NSArray arrayWithObject: restaurantDictionary[@"response"][0][@"email"]];
    if ([MFMailComposeViewController canSendMail] && self.isEmailAdressAvailable) {
        
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        
        [mailViewController setToRecipients:usersTo];
        
        [mailViewController setSubject:@"Subject Goes Here."];
        [mailViewController setMessageBody:@"Your message goes here." isHTML:NO];
        
        [self presentViewController:mailViewController animated:YES completion:nil];
        
    }
    
    else {
        
        [self displayUnavailableServiceFor:@"Email is"];
    }
    
}

- (void)displayUnavailableServiceFor:(NSString *)service
{
    NSString * selectedCategory = [[singletonClass sharedInstance] isDiningSectionSelected] ? @"restaurant" : @"place";
    
UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:[NSString stringWithFormat:@"%@ not available for this %@", service, selectedCategory] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}



@end
