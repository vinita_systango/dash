//
//  specltyViewController.h
//  Dash
//
//  Created by CS_Mac4 on 15/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface specltyViewController : UIViewController
{
    NSMutableDictionary *specialtyDictionary;
    NSTimer *advertsmntTimer;
    AsyncImageView *_advertseImgView;
}
@property (strong, nonatomic) IBOutlet UIScrollView *specltyScrollView;
- (IBAction)bckButtonClckd:(id)sender;
@property int restaurantId;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UIButton *bckBtn;
@property (strong, nonatomic)NSMutableDictionary *advertisementImages;
@property (strong, nonatomic) NSString *paidStatus;
@end
