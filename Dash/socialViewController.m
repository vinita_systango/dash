//
//  socialViewController.m
//  Dash
//
//  Created by CS_Mac4 on 18/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import "socialViewController.h"

@interface socialViewController ()<UIWebViewDelegate>

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView * activityIndicator;
@end

@implementation socialViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:NO];
    NSLog(@"%@",_socialUrlString);
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:_bckBtn]];
    NSURLRequest *urlReqst=[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:_socialUrlString]];
    socialNetworkWebView=[[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 520)];
    socialNetworkWebView.delegate = self;
    [socialNetworkWebView loadRequest:urlReqst];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [socialNetworkWebView removeFromSuperview];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)bckButtonClckd:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.activityIndicator stopAnimating];
    [self.view addSubview:socialNetworkWebView];
}

@end
