//
//  NotificationSettingsViewController.m
//  Dash
//
//  Created by NR on 10/09/2015.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import "NotificationSettingsViewController.h"
#import "UpdateNotificationSettingViewController.h"

@interface NotificationSettingsViewController ()
@property(nonatomic, weak) IBOutlet UIButton *dinningNotificationBtn,*enterNotifictaionBtn;

@end

@implementation NotificationSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
        NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor darkGrayColor],
                                               NSForegroundColorAttributeName,
                                               [UIFont fontWithName:@"Rockwell" size:16.0],
                                               NSFontAttributeName,
                                               nil];
    
    
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    
    self.title=@"NOTIFICATION SETTINGS";
    
    UIButton *bckBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 22, 18)];
    
    [bckBtn setBackgroundImage:[UIImage imageNamed:@"back_black_btn"] forState:UIControlStateNormal];
    
    [bckBtn addTarget:self action:@selector(popToBack) forControlEvents:UIControlEventTouchUpInside];
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:bckBtn]];
    


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)popToBack
{
    [self.navigationController popViewControllerAnimated:YES];

}
-(void)viewWillAppear:(BOOL)animated
{
    [self.dinningNotificationBtn setTitleColor:[UIColor colorWithRed:82/255.0 green:177/255.0 blue:208/255.0 alpha:1] forState:UIControlStateNormal];
    if ([[singletonClass sharedInstance] getIsEntertainmentDisabled]==YES)
    {
        self.enterNotifictaionBtn.userInteractionEnabled=NO;
        [self.enterNotifictaionBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    }
    else{
        self.enterNotifictaionBtn.userInteractionEnabled=YES;
        [self.enterNotifictaionBtn setTitleColor:[UIColor colorWithRed:82/255.0 green:177/255.0 blue:208/255.0 alpha:1] forState:UIControlStateNormal];
       
    }
    [self.navigationController.navigationBar setHidden:NO];
    [self.navigationController setNavigationBarHidden: NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:
     UIStatusBarAnimationSlide];

}
-(IBAction)dinningNotificationClicked:(id)sender
{
    
     UpdateNotificationSettingViewController *updateNotificationView =[[UpdateNotificationSettingViewController alloc] initWithNibName:@"UpdateNotificationSettingViewController" bundle:nil];
    updateNotificationView.selectedNotificationSection=DiningSection;
    [self.navigationController pushViewController:updateNotificationView animated:YES];

}
-(IBAction)entertainmentNotificationClicked:(id)sender
{
    UpdateNotificationSettingViewController *updateNotificationView =[[UpdateNotificationSettingViewController alloc] initWithNibName:@"UpdateNotificationSettingViewController" bundle:nil];
    updateNotificationView.selectedNotificationSection=EntertainmentSection;

    [self.navigationController pushViewController:updateNotificationView animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
