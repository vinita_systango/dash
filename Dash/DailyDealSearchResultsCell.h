//
//  DailyDealSearchResultsCell.h
//  Dash
//
//  Created by stplmacmini8 on 6/10/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DailyDealSearchResultsCell : UITableViewCell

@property(nonatomic, strong) IBOutlet UILabel * restaurantName;
@property(nonatomic, strong) IBOutlet UILabel * dealDescription;

@end
