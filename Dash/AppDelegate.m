//
//  AppDelegate.m
//  Dash
//
//  Created by CS_Mac4 on 12/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import "AppDelegate.h"
#import "clientContactViewController.h"
#import "settingsViewController.h"
#import "settingsViewController.h"
#import "ViewController.h"
#import "Harpy.h"
#import "UpdateNotificationSettingViewController.h"
#import"ACTReporter.h"

NSString *const kAppId = @"932252590";

@implementation AppDelegate
@synthesize dinningPopUpArray,entertainmentPopUpArray;

-(void)setCityEntertainmentStatus
{
   NSArray* citiesArray=[[NSMutableArray alloc] initWithArray:[[singletonClass sharedInstance] getCities]];
    for (NSDictionary *dic in citiesArray) {
        NSString *currentCityId=[[NSUserDefaults standardUserDefaults] objectForKey:@"cityId"];
        if ([currentCityId isEqualToString:[dic objectForKey:@"city_id"]]) {
            
            BOOL isEntertainmentEnabled= [[dic objectForKey:@"entertainment_status"]boolValue];
            [[singletonClass sharedInstance] assignEntertainmentDisabled:!isEntertainmentEnabled];
 
        }
    }

}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[singletonClass sharedInstance] assignSelectedSection:DiningSection];
    
    // Enable automated usage reporting.
    // Adwords Apple Conversions
    // Google iOS Download tracking snippet
    // To track downloads of your app, add this snippet to your
    
    [ACTConversionReporter reportWithConversionID:@"962443034" label:@"bC0mCPOv2F4Qmu72ygM" value:@"3.00" isRepeatable:NO];

    // initialize RemoteNotificationManager to add observers
    [RemoteNotificationManager sharedInstance];
    
    [self setCityEntertainmentStatus];
    
    self.dinningPopUpArray=[[singletonClass sharedInstance]getDinningPopupArray];
    self.entertainmentPopUpArray=[[singletonClass sharedInstance]getEntertainmentPopupArray];
    
    self.window=[[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    [self.window makeKeyAndVisible];
    [[singletonClass sharedInstance] assignRootViewControllerWithAnimation:NO];
    // Register for Apple Push Notification
    [self registerAPNs:application];
    
    // set Status bar and NavigationBar apperance
    [self setNavigationBarAndStatusBar];
    
    
    // If the app is in background mode and launched by tapping notifications
    if(launchOptions != nil) {
        NSDictionary *notificationUserInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (notificationUserInfo != nil) {
            
            [[RemoteNotificationManager sharedInstance] processNotificationUserInfo:notificationUserInfo];
        }
    }
    
    // Added this to register the app, even if user switches off push notifications for the app. Required for analytics.
    UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
        if (![[UIApplication sharedApplication] isRegisteredForRemoteNotifications]) {
            [[singletonClass sharedInstance] registerUser:[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]];
        }
    }
    // Fix for checking if device has registered for notifications for ios 7.0 and less.
    else if (types == UIRemoteNotificationTypeNone){
        [[singletonClass sharedInstance] registerUser:[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"]];
    }

    
    
    
    return YES;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotification" object:nil userInfo:userInfo];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    NSString *mydeviceToken = [[[deviceToken description]
                                stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]]
                               stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    NSLog(@"mydeviceToken: %@",mydeviceToken);
    [[NSUserDefaults standardUserDefaults] setObject:mydeviceToken forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[singletonClass sharedInstance] registerUser:mydeviceToken];
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
   // NSLog(@"Failed to get token, error: %@", error);
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
//     [[Harpy sharedInstance] checkVersion];
    [self checkForNewVersion];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Private methods

- (void)registerAPNs:(UIApplication *)application
{
    if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
    {
        // iOS 8 Notifications
        UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeBadge | UIUserNotificationTypeAlert;
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
        
        [application registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8 Notifications
        [application registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    }
}

- (void)setNavigationBarAndStatusBar
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor darkGrayColor],
                                               NSForegroundColorAttributeName,
                                               [UIFont fontWithName:@"Rockwell" size:16.0],
                                               NSFontAttributeName,
                                               nil];
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    
    [[UINavigationBar appearance]  setBackgroundImage:[UIImage imageNamed:@"bar.png"] forBarMetrics:UIBarMetricsDefault];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

-(void)checkForNewVersion
{
   
    NSString *appName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    [[Harpy sharedInstance] setAppID:kAppId];
    [[Harpy sharedInstance] setAlertType:HarpyAlertTypeForce];
    [[Harpy sharedInstance] setPresentingViewController:_window.rootViewController];
    [[Harpy sharedInstance] setAppName:appName];
    [[Harpy sharedInstance] checkVersion];
}


@end
