//
//  specltyViewController.m
//  Dash
//
//  Created by CS_Mac4 on 15/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import "specltyViewController.h"
#import "restaurantDetailViewController.h"

@interface specltyViewController ()

@end

@implementation specltyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:NO];
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(callService)
                                        object:nil];
    [queue addOperation:operation];

}
-(void)popToHome
{
    [self .navigationController popToRootViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIButton *homeButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    
    [homeButton setBackgroundImage:[UIImage imageNamed:@"home_icon_black"] forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(popToHome) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *homeBarBtn=[[UIBarButtonItem alloc] initWithCustomView:homeButton];
    
    
    [self.navigationItem setRightBarButtonItem:homeBarBtn];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
   
    if (IS_IPHONE_5
        )
    {
        [_specltyScrollView setFrame:CGRectMake(0, 0, 320, 504)];
        _advertseImgView=[[BannerView sharedBannerInstance] getAdvertisementWithRect:CGRectMake(0, 440, 320, 66) OnView:self ];
    }
    else
    {
        
        [_specltyScrollView setFrame:CGRectMake(0, 0, 320, 503)];
        
        _advertseImgView=[[BannerView sharedBannerInstance] getAdvertisementWithRect:CGRectMake(0, 350, 320, 66) OnView:self ];
    }
       [self.view addSubview:_advertseImgView];
      
    self.title = [[singletonClass sharedInstance] isDiningSectionSelected] ? @"SPECIALS" : @"SCHEDULES";
        
    _specltyScrollView.contentSize = CGSizeMake(320,0);

     [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:_bckBtn]];
    
    if (!IS_IPHONE_5)
    
    {
        _pageControl.frame=CGRectMake(_pageControl.frame.origin.x, 300, _pageControl.frame.size.width,  _pageControl.frame.size.height);
        
    }

    // Do any additional setup after loading the view from its nib.
}


-(void)viewWillDisappear:(BOOL)animated
{
    [advertsmntTimer invalidate];
}
-(void)getServerResponse
{
     int xValue=0;
    if ([specialtyDictionary[@"response"][@"adv_img"][0] isEqualToString:@"-1"])
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        return;
    }
    
    if ([specialtyDictionary[@"response"][@"adv_img"]count]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:@"special/events does not exist for this restaurant " delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
        [alert show];
    }
    else{
    _pageControl.numberOfPages=[specialtyDictionary[@"response"][@"adv_img"]count];
    
    for (int i=0; i<[specialtyDictionary[@"response"][@"adv_img"]count]; i++)
    {
        
        
          NSString *UrlString = [Constant baseURLWithPath:[NSString stringWithFormat:@"cmsAdmin/panel/sep_images/%@",specialtyDictionary[@"response"][@"adv_img"][i]]];
        
        AsyncImageView *spcltyImageView;
        if (IS_IPHONE_5)
        {
            spcltyImageView=[[AsyncImageView alloc] initWithFrame:CGRectMake(xValue, 0, 320, 440)];
            
        }
        else
        {
            spcltyImageView=[[AsyncImageView alloc] initWithFrame:CGRectMake(xValue, 0, 320, 352)];
            
        }
        
         UrlString=[UrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        spcltyImageView.imageURL=[NSURL URLWithString:UrlString];
        
         [_specltyScrollView addSubview:spcltyImageView];
        xValue=xValue+320;
    }
    _specltyScrollView.contentSize=CGSizeMake(xValue,  0);
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
}
-(void)startAdvertisementTimer
{
    if ([[[BannerView sharedBannerInstance]lowCostAdvertisementImages][@"response"][0][@"id"]intValue]!=-1)
    {
        advertsmntTimer= [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(animateAdvertisements) userInfo:nil repeats:YES];
        
    }
}

-(void)animateAdvertisements
{
    [[BannerView sharedBannerInstance]animateAdvertisements:_advertseImgView ofType:NO];
    
}

-(void)callService
{
    if ( ![self.paidStatus isEqualToString:@"PAID"])
    {
        
    [self performSelectorOnMainThread:@selector(startAdvertisementTimer) withObject:nil waitUntilDone:NO];

    }
    
       specialtyDictionary=  [[singletonClass sharedInstance] getSpecialtyDetail:[NSString stringWithFormat:@"%d",_restaurantId]];
    
    [self performSelectorOnMainThread:@selector(getServerResponse) withObject:nil waitUntilDone:NO];
    
    
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageNum = (int)(scrollView.contentOffset.x / scrollView.frame.size.width);
    _pageControl.currentPage=pageNum;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)bckButtonClckd:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
