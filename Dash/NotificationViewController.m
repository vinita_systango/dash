//
//  NotificationViewController.m
//  Dash
//
//  Created by CS_Mac4 on 07/10/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import "NotificationViewController.h"
#import "customTableCell.h"
#import "clientContactViewController.h"
#import "settingsViewController.h"
#import "DailyDealSearchViewController.h"
#import "restaurantDetailViewController.h"

@interface NotificationViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UIButton * searchButton;
@property (nonatomic, weak) IBOutlet UIButton * notificationsButton;
@property (nonatomic, weak) IBOutlet UIButton * contactButton;
@property (nonatomic, weak) IBOutlet UIButton * settingsButton;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView * activityIndicator;

@end

@implementation NotificationViewController

- (void)viewDidLoad
{
    
    
    UIButton *homeButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    
    [homeButton setBackgroundImage:[UIImage imageNamed:@"home_icon_black"] forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(popToHome) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *homeBarBtn=[[UIBarButtonItem alloc] initWithCustomView:homeButton];
    
    [self.navigationItem setRightBarButtonItem:homeBarBtn];
    
    
      [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:_bckBtn]];
    
    self.title=@"NOTIFICATIONS";
    
    [self setupButtonImages];
    [super viewDidLoad];
    
     // Do any additional setup after loading the view from its nib.
}

-(void)popToHome
{
    [self .navigationController popToRootViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    notificationsDictnry=[[NSMutableDictionary alloc] init];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    viewControllers = [self.navigationController viewControllers];
    
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(getNotfcations)
                                        object:nil];
    [queue addOperation:operation];
    
    // clear NotificationTray
    [[RemoteNotificationManager sharedInstance] clearNotificationTrayBlock:^{} clearAll:YES];
    [self.activityIndicator startAnimating];
    
    [super viewWillAppear:animated];   
}

-(void)getNotfcations
{
    [self performSelectorOnMainThread:@selector(getNotfcationsResponse) withObject:nil waitUntilDone:NO];
    
}

-(void)getNotfcationsResponse
{
    notificationsDictnry=[[singletonClass sharedInstance] getAllNotifications:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]:[[NSUserDefaults standardUserDefaults] objectForKey:@"cityId"]
                          ];
    [self.activityIndicator stopAnimating];
    
    if([notificationsDictnry[@"response"][0][@"reg_id"]intValue]==-1)
    {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
         UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"no notifications available" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [_notfcatnsTableView reloadData];
    }
}

#define tableViewDelegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [notificationsDictnry[@"response"]  count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    customTableCell *cell =(customTableCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[customTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString * notificationTime = notificationsDictnry[@"response"][indexPath.row][@"noti_time"];
    
    NSDate *notfctnDate=[df dateFromString:notificationTime];
    NSInteger distanceBetweenDates = [notfctnDate timeIntervalSinceDate:[NSDate date]];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    NSInteger minutes = distanceBetweenDates/ 60;

    
    
    if (minutes/60==0)
    {
        if (minutes >= 0)
        {
            // do positive stuff
        }
        else
        {
            minutes=-minutes;
            // do negative stuff
        }
        cell.timeLabel.text=[NSString stringWithFormat:@"%zd minutes ago",minutes];
        
    }
    else
    {
        if (hoursBetweenDates >= 0)
        {
            // do positive stuff
        }
        else
        {
            hoursBetweenDates=-hoursBetweenDates;
            // do negative stuff
        }

        cell.timeLabel.text=[NSString stringWithFormat:@"%zd hours ago",hoursBetweenDates];
    }
    
    if ([notificationsDictnry[@"response"][indexPath.row][@"res_nam"]isEqualToString:@"res_nam"])
    {
        cell.labelRestaurantName.text=@"Dash";
        
    }
    else
    {
        cell.labelRestaurantName.text=notificationsDictnry[@"response"][indexPath.row][@"res_nam"];
        
    }
    
    cell.availabilityNameLabel.text=[NSString stringWithFormat:@"%@",notificationsDictnry[@"response"][indexPath.row][@"res_dsc"]];
    
    
    
 
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 128;
}

#pragma mark UITableView delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![notificationsDictnry[@"response"][indexPath.row][@"reg_id"]  isEqual: @"reg_id"]) {
        NSString * nibName = (IS_IPHONE_5 ? @"restaurantDetailViewController" : @"restaurantDetailViewControlleri4");
        restaurantDetailViewController *restaurantDetailView =[[restaurantDetailViewController alloc] initWithNibName:nibName bundle:nil];
        restaurantDetailView.restaurantId = [notificationsDictnry[@"response"][indexPath.row][@"reg_id"] intValue];
        [self.navigationController pushViewController:restaurantDetailView animated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




 /*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setupButtonImages
{
    [[self.searchButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.settingsButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.contactButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.notificationsButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
}


- (IBAction)backBtnClckd:(id)sender
{
   // [self.navigationController popViewControllerAnimated:YES];
    
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isRestaurntListView"])
//    {
//        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isRestaurntListView"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.navigationController popViewControllerAnimated:YES];
//    }
//    else
//    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (IBAction)contanctBtnClckd:(id)sender
{
    clientContactViewController *clientContctview;
    if (IS_IPHONE_5)
    {
        clientContctview =[[clientContactViewController alloc] initWithNibName:@"clientContactViewController" bundle:nil];
        
    }
    else
    {
        clientContctview =[[clientContactViewController alloc] initWithNibName:@"clientContactViewControlleri4" bundle:nil];
    }
    for (UIViewController *vc in viewControllers) {
        if ([vc isKindOfClass:[clientContactViewController class]])
        {
            
            [self.navigationController popToViewController:vc animated:YES];
            return;
            
        }
        
    }
    

    [self.navigationController pushViewController:clientContctview animated:YES];
    

}

- (IBAction)settingsBtnClckd:(id)sender
{
    settingsViewController *settingsView;
    if (IS_IPHONE_5)
    {
        settingsView=[[settingsViewController alloc] initWithNibName:@"settingsViewController" bundle:nil];
    }
    else {
        settingsView=[[settingsViewController alloc] initWithNibName:@"settingsViewControlleri4" bundle:nil];
    }
    
    for (UIViewController *vc in viewControllers) {
        if ([vc isKindOfClass:[settingsViewController class]])
        {
            
            [self.navigationController popToViewController:vc animated:YES];
            return;
            
        }
        
    }

    [self.navigationController pushViewController:settingsView animated:YES];
    
}

- (IBAction)searchButtonClicked:(id)sender
{
    
    DailyDealSearchViewController * dailiesSearchView = [[DailyDealSearchViewController alloc] initWithNibName:@"DailyDealsSearchView" bundle:nil];
    
    for (UIViewController *vc in viewControllers) {
        if ([vc isKindOfClass:[DailyDealSearchViewController class]])
        {
            [self.navigationController popToViewController:vc animated:YES];
            return;
        }
        
    }
    
    [self.navigationController pushViewController:dailiesSearchView animated:YES];
    
    
}

- (IBAction)menuButtonClicked:(id)sender
{
    [super menuButtonTapped];
    
}

- (IBAction)entertainmentButtonClicked:(id)sender
{
    [super displayEntertainmentHomeView];
}

- (IBAction)diningButtonClicked:(id)sender
{
    [super displayDiningHomeView];
}


@end
