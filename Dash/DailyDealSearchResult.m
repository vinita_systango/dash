//
//  DailyDealSearchResult.m
//  Dash
//
//  Created by stplmacmini8 on 6/9/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import "DailyDealSearchResult.h"
#import "NSDictionary+HasValueForKey.h"

NSString * const kDealDescKey = @"deal_desc";
NSString * const kDealidKey = @"id";
NSString * const kRestaurantNameKey = @"rest_name";
NSString * const kRestaurantIdKey = @"rest_id";
NSString * const kRestaurantPaidStatusKey =@"paid_status";

@implementation DailyDealSearchResult

#pragma mark init methods

- (instancetype) initWithSearchResults:(NSDictionary *) searchResults
{
    self = [super init];
    if (self) {
        [self getDailyDealSearchResultFromJSONDict:searchResults];
    }
    return self;
}

#pragma mark Private methods

- (void)getDailyDealSearchResultFromJSONDict:(NSDictionary *)response
{
    _deal_desc = [response getValueForKey:kDealDescKey];
    _deal_Id = [[response getValueForKey:kDealidKey] integerValue];
    _restaurantId = [[response getValueForKey:kRestaurantIdKey] integerValue];
    _restaurantName = [response getValueForKey:kRestaurantNameKey];
    _restaurantPaidStatus=[response getValueForKey:kRestaurantPaidStatusKey];
}


@end


