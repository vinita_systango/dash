//
//  couponViewController.m
//  Dash
//
//  Created by CS_Mac4 on 15/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import "couponViewController.h"
#import "restaurantDetailViewController.h"

CGFloat  firstX;
CGFloat  firstY;

@interface couponViewController ()

@end

@implementation couponViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:
     UIStatusBarAnimationSlide];
    

     [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(callService)
                                        object:nil];
    [queue addOperation:operation];
}
-(void)popToHome
{
    [self .navigationController popToRootViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton *homeButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    
    [homeButton setBackgroundImage:[UIImage imageNamed:@"home_icon_black"] forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(popToHome) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *homeBarBtn=[[UIBarButtonItem alloc] initWithCustomView:homeButton];
    
    
    [self.navigationItem setRightBarButtonItem:homeBarBtn];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:
     UIStatusBarAnimationSlide];
    

    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    imgsCount=0;
    
    if (IS_IPHONE_5)
    {
        
        [_couponScrollView setFrame:CGRectMake(0, 0, 320, 504)];
         _advertseImgView=[[BannerView sharedBannerInstance] getAdvertisementWithRect:CGRectMake(0, 440, 320, 66) OnView:self ];
    }
    else
    {
        [_couponScrollView setFrame:CGRectMake(0, 0, 320, 503)];
        _advertseImgView=[[BannerView sharedBannerInstance] getAdvertisementWithRect:CGRectMake(0, 350, 320, 66) OnView:self ];
    }

    [self.view addSubview:_advertseImgView];
  

    self.title=@"COUPONS";
    
    _couponScrollView.contentSize = CGSizeMake(320,0);
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:_bckBtn]];
  
    if (!IS_IPHONE_5)
    {
        _pageControl.frame=CGRectMake(_pageControl.frame.origin.x, 300, _pageControl.frame.size.width,  _pageControl.frame.size.height);
        
    }
    
    // Do any additional setup after loading the view from its nib.
}


-(void)viewWillDisappear:(BOOL)animated
{
    [advertsmntTimer invalidate];
}
-(void)getServerResponse
{
    int xValue=0;
    if ([couponsDictionary[@"response"][@"couimg"][0] isEqualToString:@"-1"])
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        return;
    }
    
    if ([couponsDictionary[@"response"][@"couimg"] count]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:@"Coupons does not exist for this restaurant " delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
        [alert show];
    }
    else{
    _pageControl.numberOfPages=[couponsDictionary[@"response"][@"couimg"]count];
    
    
    CGRect innerScrollFrame = _couponScrollView.bounds;
    for (int i=0; i<[couponsDictionary[@"response"][@"couimg"]count]; i++)
    {
      
        AsyncImageView *spcltyImageView;
        if (IS_IPHONE_5)
        {
            spcltyImageView=[[AsyncImageView alloc] initWithFrame:CGRectMake(xValue, 0, 320, 440)];
            
        }
        else
        {
            spcltyImageView=[[AsyncImageView alloc] initWithFrame:CGRectMake(xValue, 0, 320, 352)];
            
        }

        
        
        [spcltyImageView setUserInteractionEnabled:YES
         ];
        
        
        NSString *UrlString= [Constant baseURLWithPath:[NSString stringWithFormat:@"cmsAdmin/panel/coup_images/%@",couponsDictionary[@"response"][@"couimg"][i]]];
        
        UrlString=[UrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        spcltyImageView.imageURL=[NSURL URLWithString:UrlString];
        
        spcltyImageView.tag = 1;
  
        UIScrollView *pageScrollView = [[UIScrollView alloc] initWithFrame:innerScrollFrame];
        pageScrollView.minimumZoomScale = 1.0f;
        pageScrollView.maximumZoomScale = 2.0f;
        pageScrollView.zoomScale = 1.0f;
        pageScrollView.contentSize = spcltyImageView.bounds.size;
        pageScrollView.delegate = self;
        pageScrollView.showsHorizontalScrollIndicator = NO;
        pageScrollView.showsVerticalScrollIndicator = NO;
        
        [pageScrollView addSubview:spcltyImageView];
        
        [_couponScrollView addSubview:pageScrollView];
       
        innerScrollFrame.origin.x += innerScrollFrame.size.width;
       
    }
    _couponScrollView.contentSize = CGSizeMake(innerScrollFrame.origin.x +
                                               innerScrollFrame.size.width-320, _couponScrollView.bounds.size.height);
    
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return [scrollView viewWithTag:1];
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

-(void)panned:(UIPanGestureRecognizer *)sender
{
    UIImageView *imgVw=(UIImageView *)sender.view;
    
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:imgVw];
    
    if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan)
    {
        firstX = [[sender view] center].x;
        
        firstY = [[sender view] center].y;
        
    }
    
    if (firstX>0)
    {
        
    }
    translatedPoint = CGPointMake(firstX+translatedPoint.x, firstY+translatedPoint.y);
    
    [[sender view] setCenter:translatedPoint];
    
}

-(void)startAdvertisementTimer
{
    if ([[[BannerView sharedBannerInstance]lowCostAdvertisementImages][@"response"][0][@"id"]intValue]!=-1)
    {
        advertsmntTimer= [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(animateAdvertisements) userInfo:nil repeats:YES];
        
    }
}

-(void)animateAdvertisements
{
    [[BannerView sharedBannerInstance]animateAdvertisements: _advertseImgView ofType:NO];
    
}


-(void)callService
{
     [self.navigationController.navigationBar setHidden:NO];
    if ( ![self.paidStatus isEqualToString:@"PAID"])
    {
      [self performSelectorOnMainThread:@selector(startAdvertisementTimer) withObject:nil waitUntilDone:NO];

    }
       couponsDictionary=  [[singletonClass sharedInstance] getCouponDetail:[NSString stringWithFormat:@"%d",_restaurantId]];
    
    [self performSelectorOnMainThread:@selector(getServerResponse) withObject:nil waitUntilDone:NO];
    
   }

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageNum = (int)(scrollView.contentOffset.x / scrollView.frame.size.width);
    _pageControl.currentPage=pageNum;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)bckButtonClckd:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
