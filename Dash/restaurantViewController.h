//
//  restaurantViewController.h
//  Dash
//
//  Created by CS_Mac4 on 12/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseDinningViewController.h"

@interface restaurantViewController : BaseDinningViewController
{
    NSMutableDictionary *restaurantDictionary;
    NSMutableArray *categryArray;
    
    NSMutableArray *dataArray;
    NSMutableArray *backupDataArray;
    AsyncImageView *_advertseImgView;
    NSTimer *advertsmntTimer;
    NSString *selectedRestId;
    UIButton *tempNotificationBtn;
   
    BOOL  isBlocked;
}

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndctr;
@property CGFloat previousContentDelta;
@property CGFloat initialContentOffset;
@property int categryId;

@property (strong, nonatomic) IBOutlet UITextField *serchTextField;

@property (strong, nonatomic) IBOutlet UITableView *restaurantTableView;
@property (strong, nonatomic) IBOutlet UIView *bottomBar;
- (IBAction)bckButtonClckd:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *bckBtn;
@property (strong, nonatomic) IBOutlet UIView *bottomBarView;
- (IBAction)settingsBtnClckd:(id)sender;
- (IBAction)ownerContctBtnClckd:(id)sender;
- (IBAction)notifctnBtnClckd:(id)sender;

@end
