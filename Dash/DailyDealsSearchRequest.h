//
//  SearchDailyDeals
//  Dash
//
//  Created by stplmacmini8 on 6/8/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DailyDealsSearchRequest : NSObject

@property (nonatomic, strong) NSString * searchText;
@property (nonatomic, assign) NSInteger searchDay;
@property (nonatomic, strong) NSString * cityId;

- (instancetype) initWithSearchText:(NSString *) searchText searchDay:(NSInteger)searchDay cityId:(NSString *)cityId;

@end
