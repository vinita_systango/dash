//
//  BannerView.m
//  Dash
//
//  Created by NR on 03/09/2015.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import "BannerView.h"
#import "restaurantDetailViewController.h"

@interface BannerView ()

@end
@implementation BannerView

@synthesize highCostAdvertisementImages,lowCostAdvertisementImages;
static BannerView *shareBannerInstance=Nil;

+(id)sharedBannerInstance
{
    if (shareBannerInstance==Nil)
    {
        shareBannerInstance=[[BannerView alloc]init];
    }
    
    return shareBannerInstance;
}


-(void)fetchBannerLowCostImagesFromServer
{
    
    self.lowCostAdvertisementImages= [[singletonClass sharedInstance] getAllAdvrtsmnt:@"0":[[NSUserDefaults standardUserDefaults] objectForKey:@"cityId"]];
       //NSLog(@"lowCostAdvertisementImages count=%lu",(unsigned long)[self.lowCostAdvertisementImages [@"response"]count]);
  //NSLog(@"lowCostAdvertisementImages =%@",[self.lowCostAdvertisementImages description]);

    self.nextLowCostImage= arc4random() % [self.lowCostAdvertisementImages [@"response"]count] ;

}
-(void)fetchBannerHighCostImagesFromServer
{
    
        self.highCostAdvertisementImages= [[singletonClass sharedInstance] getAllAdvrtsmnt:@"1":[[NSUserDefaults standardUserDefaults] objectForKey:@"cityId"]];
       // NSLog(@"highCostAdvertisementImages count=%lu",(unsigned long)[self.highCostAdvertisementImages [@"response"]count]);
    // NSLog(@"highCostAdvertisementImages =%@",[self.highCostAdvertisementImages description]);
    self.nextHighCostImage = arc4random() % [self.highCostAdvertisementImages [@"response"]count];

}
-(AsyncImageView*)getAdvertisementWithRect:(CGRect)frame OnView:(id)delegate
{
   
    AsyncImageView *bannerView=[[AsyncImageView alloc] init];
    bannerView.frame=frame;
    bannerView.image=[UIImage imageNamed:@"IphoneBackdrop.png"];
    [bannerView setUserInteractionEnabled:YES];
    UITapGestureRecognizer *singleTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openAdvertisementLink:)];
    singleTap.delegate=delegate;
    [bannerView addGestureRecognizer:singleTap];
    self.delegate=delegate;
    
    if (self.lowCostAdvertisementImages==nil) {
        [self fetchBannerLowCostImagesFromServer];
    }
    if (self.highCostAdvertisementImages==nil) {
        [self fetchBannerHighCostImagesFromServer];
    }
    return bannerView;
}

-(void)animateAdvertisements:(AsyncImageView*)advertiseMentView ofType:(BOOL)isHighCost
{
    NSString *UrlString;
    if (isHighCost==YES)
    {
        self.isCurrentBannerHighCost=YES;
        if (self.nextHighCostImage>[self.highCostAdvertisementImages [@"response"]count]-1)
        {
            self.nextHighCostImage=0;
        }
        NSString *imageName=self.highCostAdvertisementImages[@"response"][self.nextHighCostImage][@"adv_img"][0];
        if ([imageName length]==0) {
            self.nextHighCostImage++;
            if (self.nextHighCostImage>[self.highCostAdvertisementImages [@"response"]count]-1)
            {
                self.nextHighCostImage=0;
            }
            imageName=self.highCostAdvertisementImages[@"response"][self.nextHighCostImage][@"adv_img"][0];
        }
       // NSLog(@"highcost add with index %d",self.nextHighCostImage);
       UrlString = [Constant baseURLWithPath:[NSString stringWithFormat:@"cmsAdmin/panel/adv_images/%@",imageName]];
        
    }
    else{
        self.isCurrentBannerHighCost=NO;
        if (self.nextLowCostImage>[self.lowCostAdvertisementImages [@"response"]count]-1)
        {
            self.nextLowCostImage=0;
        }
        
        NSString *imageName=self.lowCostAdvertisementImages[@"response"][self.nextLowCostImage][@"adv_img"][0];
        if ([imageName length]==0) {
            self.nextLowCostImage++;
            if (self.nextLowCostImage>[self.lowCostAdvertisementImages [@"response"]count]-1)
            {
                self.nextLowCostImage=0;
            }
            imageName=self.lowCostAdvertisementImages[@"response"][self.nextLowCostImage][@"adv_img"][0];
        }
        //NSLog(@"lowcost add  with index =%d",self.nextLowCostImage);

        UrlString = [Constant baseURLWithPath:[NSString stringWithFormat:@"cmsAdmin/panel/adv_images/%@",
                                               imageName]];
    }
   
    UrlString=[UrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (isHighCost==YES){
        self.currentAnimatedAdd=self.nextHighCostImage;
    }else{
        self.currentAnimatedAdd=self.nextLowCostImage;

    }
 
    [UIView transitionWithView:advertiseMentView
                      duration:2.0
                       options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionTransitionCurlDown
                    animations:^  {
                        
                        advertiseMentView.imageURL=[NSURL URLWithString:UrlString];
                    }
                    completion:^(BOOL finished) {
                        if (isHighCost==YES)
                        {
                         self.nextHighCostImage++;

                        }
                        else{
                            self.nextLowCostImage++;

                        }
                        
                    }];
    
    
}


-(void)openAdvertisementLink:(id)sender
{
    UITapGestureRecognizer *gesture=(UITapGestureRecognizer*)sender;
    AsyncImageView *imageView=( AsyncImageView *)gesture.view;
    
        NSData *data1 = UIImagePNGRepresentation([UIImage imageNamed:@"IphoneBackdrop.png"]);
       NSData *data2 = UIImagePNGRepresentation(imageView.image);
      if([data1 isEqual:data2] || data2==nil)
      {
        //its welcome image or blank image
         }
      else
       {
        // banner image from server.
        restaurantDetailViewController *restaurantDetailView;
    
    if (IS_IPHONE_5)
    {
        restaurantDetailView=[[restaurantDetailViewController alloc] initWithNibName:@"restaurantDetailViewController" bundle:nil];
    }
    else
    {
        restaurantDetailView=[[restaurantDetailViewController alloc] initWithNibName:@"restaurantDetailViewControlleri4" bundle:nil];
    }
    
    NSMutableArray   *categryArray = [[singletonClass sharedInstance] getCategories];
    if (self.isCurrentBannerHighCost==YES) {
        if (self.currentAnimatedAdd==[self.highCostAdvertisementImages[@"response"] count]) {
            self.currentAnimatedAdd--;
         }
            restaurantDetailView.catNameString=[categryArray objectAtIndex:[self.highCostAdvertisementImages[@"response"][self.currentAnimatedAdd][@"catid"][0]intValue]];
            restaurantDetailView.restaurantId=[self.highCostAdvertisementImages[@"response"][self.currentAnimatedAdd][@"id"]intValue];
            restaurantDetailView.advertisementImages=self.highCostAdvertisementImages;
           restaurantDetailView.clickedBannerId = self.highCostAdvertisementImages[@"response"][self.currentAnimatedAdd][@"add_id"];
       NSLog(@"highCostAdvertisementImages clicked with index =%d",self.currentAnimatedAdd);
    }
    else{
        if (self.currentAnimatedAdd==[self.lowCostAdvertisementImages[@"response"] count]) {
            self.currentAnimatedAdd--;
        }
        restaurantDetailView.catNameString=[categryArray objectAtIndex:[self.lowCostAdvertisementImages[@"response"][self.currentAnimatedAdd][@"catid"][0]intValue]];
        restaurantDetailView.restaurantId=[self.lowCostAdvertisementImages[@"response"][self.currentAnimatedAdd][@"id"]intValue];
        restaurantDetailView.advertisementImages=self.lowCostAdvertisementImages;
        restaurantDetailView.clickedBannerId = self.lowCostAdvertisementImages[@"response"][self.currentAnimatedAdd][@"add_id"];
       NSLog(@"lowCostAdvertisementImages clicked with index =%d",self.currentAnimatedAdd);

    }
    UIViewController *controller=(UIViewController*)(gesture.delegate);
    
    [controller.navigationController pushViewController:restaurantDetailView animated:YES];
 }
}
@end
