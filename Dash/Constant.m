//
//  Constant.m
//  Dash
//
//  Created by stplmacmini3 on 03/07/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import "Constant.h"

NSString * const kHTTPClientURLStringBaseLive = @"http://api.dashapp.ca/";
NSString * const kHTTPClientURLStringBaseDemo = @"http://dashapp.systech-soft.com/";

@implementation Constant

NS_ENUM(NSUInteger, OXHTTPClientURLServerType){
    OXHTTPClientURLServerTypeLive = 0,
    OXHTTPClientURLServerTypeDemo
};

+(NSUInteger)clientBaseURL{
#if DEBUG
    return OXHTTPClientURLServerTypeDemo;
#else
    return OXHTTPClientURLServerTypeLive;
#endif
}

+(NSString*)sharedURL{
    switch ([self clientBaseURL]) {
        case OXHTTPClientURLServerTypeLive:
            return kHTTPClientURLStringBaseLive;
            break;
        case OXHTTPClientURLServerTypeDemo:
            return kHTTPClientURLStringBaseLive;
        default:
            return @"http://invalid.url";
            break;
    }
}


+(NSString*)baseURLWithPath:(NSString *)urlPath
{
    return [NSString stringWithFormat:@"%@%@", [self sharedURL], urlPath];
}

@end
