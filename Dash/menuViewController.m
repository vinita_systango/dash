//
//  menuViewController.m
//  Dash
//
//  Created by CS_Mac4 on 15/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import "menuViewController.h"
#import "restaurantDetailViewController.h"
CGFloat  firstX;
CGFloat  firstY;
CGFloat  lastScale;
int xValue=0;

@interface menuViewController ()

@end

@implementation menuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:NO];
     [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:_bckBtn]];
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(callService)
                                        object:nil];
    [queue addOperation:operation];
    

}

-(void)popToHome
{
    [self .navigationController popToRootViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton *homeButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    
    [homeButton setBackgroundImage:[UIImage imageNamed:@"home_icon_black"] forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(popToHome) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *homeBarBtn=[[UIBarButtonItem alloc] initWithCustomView:homeButton];
    
    
    [self.navigationItem setRightBarButtonItem:homeBarBtn];
     imgsCount=0;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
  
    self.title = ([[singletonClass sharedInstance] isDiningSectionSelected]) ? @"MENU" : @"$";

    
    if (IS_IPHONE_5)
    {
        _advertseImgView=[[BannerView sharedBannerInstance] getAdvertisementWithRect:CGRectMake(0, 440, 320, 66) OnView:self ];
    }
    else
    {
        _advertseImgView=[[BannerView sharedBannerInstance] getAdvertisementWithRect:CGRectMake(0, 350, 320, 66) OnView:self];
    }
     [self.view addSubview:_advertseImgView];
   
    
    if (!IS_IPHONE_5)
       {
        _pageControl.frame=CGRectMake(_pageControl.frame.origin.x, 300, _pageControl.frame.size.width,  _pageControl.frame.size.height);
        
    }
  
    
    // Do any additional setup after loading the view from its nib.
}




-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint touch_point = [touch locationInView:self.view];
    spcltyImageView.center=touch_point;
    
  }

-(void)handlePinch:(UIPinchGestureRecognizer*)gestureRecognizer
{
    if([gestureRecognizer state] == UIGestureRecognizerStateBegan) {
        // Reset the last scale, necessary if there are multiple objects with different scales
        lastScale = [gestureRecognizer scale];
    }
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan ||
        [gestureRecognizer state] == UIGestureRecognizerStateChanged)
    {
        
        CGFloat currentScale = [[[gestureRecognizer view].layer valueForKeyPath:@"transform.scale"] floatValue];
        
        // Constants to adjust the max/min values of zoom
        const CGFloat kMaxScale = 3.0;
        const CGFloat kMinScale =1.0;
        
        CGFloat newScale = 1 -  (lastScale - [gestureRecognizer scale]); // new scale is in the range (0-1)
        newScale = MIN(newScale, kMaxScale / currentScale);
        newScale = MAX(newScale, kMinScale / currentScale);
        CGAffineTransform transform = CGAffineTransformScale([[gestureRecognizer view] transform], newScale, newScale);
        [gestureRecognizer view].transform = transform;
        
      //  _menuScrollview.contentSize=CGSizeMake(newScale, newScale);
        
        lastScale = [gestureRecognizer scale];  // Store the previous scale factor for the next pinch gesture call
    }
   
}




- (void)pinched:(UIPinchGestureRecognizer *)sender {
    
    if (sender.scale >1.0f && sender.scale < 2.5f) {
        CGAffineTransform transform = CGAffineTransformMakeScale(sender.scale, sender.scale);
        
        UIImageView *imageview=(UIImageView *)sender.view;
        
        imageview.transform = transform;
    }
}

//To rotate

-(void)viewWillDisappear:(BOOL)animated
{
    [advertsmntTimer invalidate];
}


-(void)getServerResponse
{
   
    if ([menuDictionary[@"response"][@"item_img"][0] isEqualToString:@"-1"])
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        return;
    }
    
    NSMutableArray *modifyableArray = [[NSMutableArray alloc] initWithArray:menuDictionary[@"response"][@"item_img"]];
    for (NSString *string  in modifyableArray)
    {
        if ([string isEqualToString:@""])
        {
            [modifyableArray removeObject:string];

        }
    }
    
    if ([modifyableArray count]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:@"Menu does not exist for this restaurant " delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        
        [alert show];
    }
    else{
    _pageControl.numberOfPages=[modifyableArray count];
   
     CGRect innerScrollFrame = _menuScrollview.bounds;
    for (int i=0; i<[modifyableArray count]; i++)
    {
        spcltyImageView=[[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 320, _menuScrollview.frame.size.height)];
        [spcltyImageView setUserInteractionEnabled:YES
         ];
        
        
        NSString *UrlString = [Constant baseURLWithPath:[NSString stringWithFormat:@"cmsAdmin/panel/item_images/%@",modifyableArray[i]]];
        
        UrlString=[UrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        spcltyImageView.imageURL=[NSURL URLWithString:UrlString];
        
        spcltyImageView.tag = 1;
        
        
        
        UIScrollView *pageScrollView = [[UIScrollView alloc] initWithFrame:innerScrollFrame];
        pageScrollView.minimumZoomScale = 1.0f;
        pageScrollView.maximumZoomScale = 2.0f;
        pageScrollView.zoomScale = 1.0f;
        pageScrollView.contentSize = spcltyImageView.bounds.size;
        pageScrollView.delegate = self;
        pageScrollView.showsHorizontalScrollIndicator = NO;
        pageScrollView.showsVerticalScrollIndicator = NO;
        
        [pageScrollView addSubview:spcltyImageView];
        
        [_menuScrollview addSubview:pageScrollView];
        
        innerScrollFrame.origin.x += innerScrollFrame.size.width;
       
        
    }
    _menuScrollview.contentSize = CGSizeMake(innerScrollFrame.origin.x +
                                             innerScrollFrame.size.width-320, _menuScrollview.bounds.size.height);
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

    }
    
}

-(void)startAdvertisementTimer
{
    if ([[[BannerView sharedBannerInstance]lowCostAdvertisementImages][@"response"][0][@"id"]intValue]!=-1)
    {
        advertsmntTimer= [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(animateAdvertisements) userInfo:nil repeats:YES];
        
    }
}

-(void)animateAdvertisements
{
    [[BannerView sharedBannerInstance]animateAdvertisements:_advertseImgView ofType:NO];
    
}

-(void)callService
{

    [self.navigationController.navigationBar setHidden:NO];
    if ( ![self.paidStatus isEqualToString:@"PAID"])
    {
    [self performSelectorOnMainThread:@selector(startAdvertisementTimer) withObject:nil waitUntilDone:NO];

        
    }
    menuDictionary=  [[singletonClass sharedInstance] getMenuList:[NSString stringWithFormat:@"%d",_restaurantId]];
   
    [self performSelectorOnMainThread:@selector(getServerResponse) withObject:nil waitUntilDone:NO];
    
    
   }

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return [scrollView viewWithTag:1];
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
} 

-(void)panned:(UIPanGestureRecognizer *)sender
{
    //[self.view bringSubviewToFront:[(UIPanGestureRecognizer*)sender view]];
    UIImageView *imgVw=(UIImageView *)sender.view;
    
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:imgVw];
    
    if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan)
    {
        firstX = [[sender view] center].x;
     
        firstY = [[sender view] center].y;
      
    }
    
    if (firstX>0)
    {
        
    }
    translatedPoint = CGPointMake(firstX+translatedPoint.x, firstY+translatedPoint.y);
    
    [[sender view] setCenter:translatedPoint];
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageNum = (int)(scrollView.contentOffset.x / scrollView.frame.size.width);
    _pageControl.currentPage=pageNum;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)bckButtonClckd:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
