//
//  RemoteNotificationManager.m
//  Dash
//
//  Copyright (c) 2014 Systango. All rights reserved.
//

#import "RemoteNotificationManager.h"

@implementation RemoteNotificationManager

#pragma mark - Init method

-(id)init
{
    self = [super init];
    if(self)
    {
        // add observer for APNs
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceivePushNotification:) name:@"pushNotification" object:nil];
        
        // add observer for Application become active
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(appReturnsActive) name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
    }
    return self;
}

#pragma mark - Singleton method

+ (RemoteNotificationManager *)sharedInstance
{
    static RemoteNotificationManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Public method

- (void)clearNotificationTrayBlock:(void (^)(void))block clearAll:(BOOL)clearAll
{
    if(clearAll)
    {
        int valueToSubtract = 0;
        if([[singletonClass sharedInstance] isDiningSectionSelected])
        {
            valueToSubtract = self.dBadge;
            self.dBadge = 0;
        }
        else
        {
            valueToSubtract = self.eBadge;
            self.eBadge = 0;
        }
        
        self.badge = MAX(0, (self.badge - valueToSubtract));
        
        [[UIApplication sharedApplication] updateBadgeNumbers:self.badge];
        
        [self updateNotificationBadgeCount:0];
    }
    
    block();
}

- (void)updateNotificationBadgeCountWithAppBadge:(NSInteger)appBadge sectionBadge:(int)sectionBadge block:(void (^)(void))block
{
    [[UIApplication sharedApplication] updateBadgeNumbers:appBadge];
    NSLog(@" the badge count is  %ld",(long)appBadge );
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateNotificationBadgeCount:sectionBadge];
        block();
    });
}

- (void)parseBadgesResponse:(NSMutableDictionary *)badgesInfo
{
    if([badgesInfo hasValueForKey:@"response"])
    {
        NSDictionary *response = [badgesInfo valueForKey:@"response"];
        
        self.dBadge = [[response getValueForKey:@"diningBadgeCount"] intValue];
        self.eBadge = [[response getValueForKey:@"entBadgeCount"] intValue];
        self.badge = [[response getValueForKey:@"badge"] intValue];
        
        [self updateNotificationBadgeCountWithAppBadge:self.badge sectionBadge:[self currentSectionBadge] block:^{}];
    }
}

- (int)currentSectionBadge
{
    return ([[singletonClass sharedInstance] isDiningSectionSelected] ? self.dBadge : self.eBadge);
}

#pragma mark - Private method

- (void)updateNotificationBadgeCount:(int)sectionBadge
{
    UIViewController *topViewController = [[singletonClass sharedInstance] getTopControllerOnWindow];
    
    if([topViewController respondsToSelector:@selector(updateNotificationBadgeCount:)])
        [(BaseDinningViewController *)topViewController updateNotificationBadgeCount:sectionBadge];
}

- (void)displayNotificationViewController
{
    ViewController *rootViewController = [[singletonClass sharedInstance] getSelectedCategoryViewController];
    [rootViewController displayNotificationView];
}

- (BOOL)isNotificationControllerPresented
{
    UIViewController *topViewController = [[singletonClass sharedInstance] getTopControllerOnWindow];
    
    if([topViewController isKindOfClass:[NotificationViewController class]])
    {
        // perform API to fetch data
        [(NotificationViewController *)topViewController getNotfcations];
        
        return YES;
    }
    
    return NO;
}

- (void)fetchBadgesBlock:(void (^)(void))block
{
    NSMutableDictionary *badgesInfo= [[singletonClass sharedInstance] getAllBadges:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] :[[NSUserDefaults standardUserDefaults] objectForKey:@"cityId"]];
    
    [self parseBadgesResponse:badgesInfo];
}

#pragma mark - Observer methods

- (void)didReceivePushNotification:(NSNotification *)notification
{
    if (notification != nil) {
        [self processNotificationUserInfo:notification.userInfo];
    }
}

- (void)processNotificationUserInfo:(NSDictionary *)userInfo
{
    
    NSLog(@"userInfo: %@: ", userInfo);
    
    if([userInfo hasValueForKey:@"aps"])
    {
        NSDictionary *aps = ([userInfo valueForKey:@"aps"]);
        
        self.badge = [[aps getValueForKey:@"badge"] intValue];
        self.dBadge = [[aps getValueForKey:@"diningBadgeCount"] intValue];
        self.eBadge = [[aps getValueForKey:@"entBadgeCount"] intValue];
        int section = [[aps getValueForKey:@"section"] intValue];
        
        if([[aps getValueForKey:@"actionType"] intValue] == 0) // actionType:0 for Normal Push Notification & 1 to update just the badge Count
        {
            SelectedSection newSection = (section == 2) ? EntertainmentSection : DiningSection;
            
            if(((singletonClass *)[singletonClass sharedInstance]).selectedSection != newSection)
            {
                [[singletonClass sharedInstance] assignSelectedSection:newSection];
                [[singletonClass sharedInstance] assignRootViewControllerWithAnimation:NO];
            }
            
            [self displayNotificationViewController];
        }
        else if([[aps getValueForKey:@"actionType"] intValue] == 2){
            int cityId = [[aps getValueForKey:@"city_id"] intValue];
            int currentCityId=[[[NSUserDefaults standardUserDefaults] objectForKey:@"cityId"] intValue];
            if (cityId==currentCityId) {
                [[NSUserDefaults standardUserDefaults] setBool:![[aps getValueForKey:@"isEntertainmentEnabled"] boolValue] forKey:@"isEntertainmentEnabled"];
                [[singletonClass sharedInstance] assignEntertainmentDisabled:![[aps getValueForKey:@"isEntertainmentEnabled"] boolValue]];
                // push notification for EntertainmentStatusChanged
                [[NSNotificationCenter defaultCenter] postNotificationName:@"EntertainmentStatusChanged" object:nil];
            }

        }
        else
        {
            if([self isNotificationControllerPresented])
            {
                [self clearNotificationTrayBlock:^{} clearAll:YES];
            }
            else
            {
                [self updateNotificationBadgeCountWithAppBadge:self.badge sectionBadge:[self currentSectionBadge] block:^{}];
            }
        }
    }

    
}

- (void)appReturnsActive
{
    [self fetchBadgesBlock:^{
        if([self isNotificationControllerPresented])
        {
            [self clearNotificationTrayBlock:^{} clearAll:YES];
        }
        else
        {
            [self updateNotificationBadgeCount:[self currentSectionBadge]];
        }
    }];
}



@end
