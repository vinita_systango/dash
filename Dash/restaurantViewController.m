//
//  restaurantViewController.m
//  Dash
//
//  Created by CS_Mac4 on 12/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import "restaurantViewController.h"
#import "customTableCell.h"
#import "restaurantDetailViewController.h"
#import "settingsViewController.h"
#import "restaurantDetailViewController.h"
#import "clientContactViewController.h"
#import "DailyDealSearchViewController.h"

@interface restaurantViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) IBOutlet UIButton * searchButton;
@property (nonatomic, weak) IBOutlet UIButton * notificationsButton;
@property (nonatomic, weak) IBOutlet UIButton * contactButton;
@property (nonatomic, weak) IBOutlet UIButton * settingsButton;


@end

@implementation restaurantViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [self.navigationController.navigationBar setHidden:NO];
      [self.navigationController setNavigationBarHidden: NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:
     UIStatusBarAnimationSlide];
    
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(callService)
                                        object:nil];
    [queue addOperation:operation];
    [super viewWillAppear:animated];
    
}

- (void)setupButtonImages
{
    [[self.searchButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.settingsButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.contactButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.notificationsButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
}

-(void)popToHome
{
    [advertsmntTimer invalidate];

    [self .navigationController popToRootViewControllerAnimated:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *homeButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    
    [homeButton setBackgroundImage:[UIImage imageNamed:@"home_icon_black"] forState:UIControlStateNormal];
    
    [homeButton addTarget:self action:@selector(popToHome) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *homeBarBtn=[[UIBarButtonItem alloc] initWithCustomView:homeButton];
    
    [self.navigationItem setRightBarButtonItem:homeBarBtn];
    
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor darkGrayColor],
                                               NSForegroundColorAttributeName,
                                               [UIFont fontWithName:@"Rockwell" size:16.0],
                                               NSFontAttributeName,
                                               nil];
    
    
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    
    self.title=@"DASH";
    
    categryArray = [[singletonClass sharedInstance] getCategories];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:_bckBtn]];
    
    restaurantDictionary=[[NSMutableDictionary alloc] init];
    
    _advertseImgView=[[BannerView sharedBannerInstance] getAdvertisementWithRect:CGRectMake(0, 1, _advertseImgView.bounds.size.width, _advertseImgView.bounds.size.height) OnView:self ];
    
    [self.view addSubview:_advertseImgView];
    
    [_activityIndctr startAnimating];
    
    _bottomBar=[[UIView alloc] init];
    
    if (IS_IPHONE_5)
    {
        _bottomBar.frame=CGRectMake(0, 456, _advertseImgView.bounds.size.width, 48);;
        
    }
    else
    {
        _bottomBar.frame=CGRectMake(0, 368, _advertseImgView.bounds.size.width, 48);;
    }

  
    
    _bottomBarView.frame=CGRectMake(0, 0, 320, 48);
    [_bottomBar addSubview:_bottomBarView];
 
   [self.view addSubview:_bottomBar];

    [self setupButtonImages];
    
    // Do any additional setup after loading the view from its nib.
  
}

-(void)startAdvertisementTimer
{
    if ([[[BannerView sharedBannerInstance]highCostAdvertisementImages][@"response"][0][@"id"]intValue]!=-1)
    {
        advertsmntTimer= [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(animateAdvertisements) userInfo:nil repeats:YES];
        
    }
}

-(void)animateAdvertisements
{
    [[BannerView sharedBannerInstance]animateAdvertisements:_advertseImgView ofType:YES];
    
}
-(void)callService
{
    [self performSelectorOnMainThread:@selector(startAdvertisementTimer) withObject:nil waitUntilDone:NO];
    
    [self performSelectorOnMainThread:@selector(getServerResponse) withObject:nil waitUntilDone:NO];

}

-(void)getServerResponse
{
    [_activityIndctr stopAnimating];
    _activityIndctr.hidden=YES;
    
    [self fetchData];
    
    if ([dataArray[0][@"reg_id"]intValue]==-1 || [dataArray[0][@"id"]intValue]==-1)
    {
         NSString * selectedCategory = [[singletonClass sharedInstance] isDiningSectionSelected] ? @"restaurant" : @"place";
        UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Sorry!" message:[NSString stringWithFormat:@"no %@ available for %@", selectedCategory, categryArray[_categryId]] delegate:Nil cancelButtonTitle:Nil otherButtonTitles:@"OK", nil];
        [alertView show];
    }
   else if (![self.serchTextField.text isEqualToString:@""]) {
        [self searchRestaurantsWithText:self.serchTextField.text];
    }

    [_restaurantTableView reloadData];

}
- (void)reloadTableViewDetails
{
   
    dispatch_async(dispatch_get_main_queue(), ^{
        [_restaurantTableView reloadData];
    });
}

- (void)fetchData
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isAll"]==YES)
    {
        
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]);
        
        dataArray= [[singletonClass sharedInstance] getAllRestaurant:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]:[[NSUserDefaults standardUserDefaults] objectForKey:@"cityId"]];
        
        backupDataArray=[[NSMutableArray alloc] initWithArray:dataArray];
    }
    else
    {
        dataArray=  [[singletonClass sharedInstance] getRestaurantsByCat:[NSString stringWithFormat:@"%d",_categryId]:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]:[[NSUserDefaults standardUserDefaults] objectForKey:@"cityId"]];
        backupDataArray=[[NSMutableArray alloc] initWithArray:dataArray];
      
        
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    if ([advertsmntTimer isValid]) {
        [advertsmntTimer invalidate];
        
    }}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recr0e0a00ted.
}

#define tableViewDelegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([dataArray count] != 0 && ([dataArray[0][@"reg_id"]intValue]==-1 || [dataArray[0][@"id"]intValue]==-1)){
        return 0;
    }
    return [dataArray  count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
     NSString *CellIdentifier = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    
    customTableCell *cell =(customTableCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[customTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
              }
    cell.notificationONOFF.hidden=NO;
    [cell.notificationONOFF addTarget:self action:@selector(swtchNotfctnOnOff:) forControlEvents:UIControlEventTouchUpInside];
    cell.notificationONOFF.tag=indexPath.row;

    
    if ([dataArray [indexPath.row ][@"block_status"] isEqualToString:@"block"])
    {
        [cell.notificationONOFF setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
    }
    else
    {
          [cell.notificationONOFF setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
    }
    
    
    NSString *UrlString = [Constant baseURLWithPath:[NSString stringWithFormat:@"cmsAdmin/panel/main_pics/%@",dataArray [indexPath.row ][@"main_pic"]]];
    
    UrlString=[UrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    cell.asyncImageView.imageURL=[NSURL URLWithString:UrlString];
    
    cell.restaurantNameLabel.text=dataArray [indexPath.row ][@"res_nam"];
    
    cell.addressLabel.text=dataArray [indexPath.row ][@"address"];
  cell.categryLabel.text=categryArray[_categryId];

    
    return cell;
}

-(void)swtchNotfctnOnOff:(id) sender
{
    tempNotificationBtn=sender;
    NSString *alertMessage;
    
    if ([dataArray [[sender tag]][@"block_status"] isEqualToString:@"block"])
    {
        isBlocked=YES;
        alertMessage=[NSString stringWithFormat:@"Notifications for '%@' enabled ?",dataArray [[sender tag]][@"res_nam"]];
    }
    else
    {
        
         isBlocked=NO;
        alertMessage=[NSString stringWithFormat:@"Notifications for '%@' disabled ?",dataArray [[sender tag]][@"res_nam"]];

    }
    
    selectedRestId=[[NSString alloc] init];
    selectedRestId=dataArray [[sender tag] ][@"reg_id"];
    
    UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:@"" message:alertMessage delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes",@"No", nil];
    
       [alrt show];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    restaurantDetailViewController *restaurantDetailView;
    
    if (IS_IPHONE_5)
    {
        restaurantDetailView=[[restaurantDetailViewController alloc] initWithNibName:@"restaurantDetailViewController" bundle:nil];
    }
    else
    {
        restaurantDetailView=[[restaurantDetailViewController alloc] initWithNibName:@"restaurantDetailViewControlleri4" bundle:nil];
        
    }
    
    restaurantDetailView.restaurantId=[dataArray [indexPath.row][@"reg_id"]intValue];

    restaurantDetailView.catNameString=categryArray[_categryId];
    [self.navigationController pushViewController:restaurantDetailView animated:YES];
}
#define alertViewDelegate

-(void)getBlockdata
{
    [self fetchData];
    if (![self.serchTextField.text isEqualToString:@""]) {
        [self searchRestaurantsWithText:self.serchTextField.text];
    }
    
    [self scrollTableView];
}

- (void)scrollTableView{
    CGPoint contentOffSet = [_restaurantTableView contentOffset];
    contentOffSet.y = contentOffSet.y - 0.5f;
    dispatch_async(dispatch_get_main_queue(), ^{
        [_restaurantTableView setContentOffset:contentOffSet animated:NO];
    });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (isBlocked)
    {
        if (buttonIndex==0)
        {
            
            [[singletonClass sharedInstance]blockRestaurant:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] : selectedRestId];
            
            [tempNotificationBtn setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];
            NSOperationQueue *queue = [NSOperationQueue new];
            NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                                initWithTarget:self
                                                selector:@selector(getBlockdata)
                                                object:nil];
            [queue addOperation:operation];
            
        }
        else
        {
            [tempNotificationBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];

        }
        

    }
    else
    {
        if (buttonIndex==0)
        {
             [[singletonClass sharedInstance]blockRestaurant:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] : selectedRestId];
            [tempNotificationBtn setImage:[UIImage imageNamed:@"off.png"] forState:UIControlStateNormal];
           
            NSOperationQueue *queue = [NSOperationQueue new];
            NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                                initWithTarget:self
                                                selector:@selector(getBlockdata)
                                                object:nil];
            [queue addOperation:operation];

        }
        else
        {
            [tempNotificationBtn setImage:[UIImage imageNamed:@"on.png"] forState:UIControlStateNormal];

        }

    }
    
   
}



#define scrollViewDelegates


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.initialContentOffset = scrollView.contentOffset.y;
    self.previousContentDelta = 0.f;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat prevDelta = self.previousContentDelta;
    CGFloat delta = scrollView.contentOffset.y - self.initialContentOffset;
    if (delta > 0.f && prevDelta <= 0.f)
    {
        
        [UIView transitionWithView:self.view duration:0.1 options:UIViewAnimationOptionTransitionNone animations:^{
            
            [self.navigationController setNavigationBarHidden: YES animated:YES];
            
            _advertseImgView.frame=CGRectMake(0, 1, _advertseImgView.bounds.size.width, _advertseImgView.bounds.size.height);
            
            _bottomBar.frame=CGRectMake(0, 575,320,48);;
            
            [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:
             UIStatusBarAnimationSlide];
            
            
        } completion:nil];
        
          }
    else if (delta < 0.f && prevDelta >= 0.f)
    {
        
        [UIView transitionWithView:self.view duration:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            [self.navigationController setNavigationBarHidden: NO animated:YES];
            _bottomBar.hidden=NO;
            if (IS_IPHONE_5)
            {
                _bottomBar.frame=CGRectMake(0, 456, _advertseImgView.bounds.size.width, 48);;
                
            }
            else
            {
                _bottomBar.frame=CGRectMake(0, 368, _advertseImgView.bounds.size.width, 48);;
            }

            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:
             UIStatusBarAnimationSlide];
            
            
        } completion:nil];
        
        
    }
    self.previousContentDelta = delta;
}




- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_serchTextField resignFirstResponder];
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];

    if([searchStr isEqualToString:@""])
    {
        dataArray =[[NSMutableArray alloc] initWithArray:backupDataArray];
    }
    
    else {
        
         [self searchRestaurantsWithText:searchStr];
    }
    [_restaurantTableView reloadData];
    
//    }
//    else
//    {
//        dataArray =[[NSMutableArray alloc] initWithArray:backupDataArray];
//        [_restaurantTableView reloadData];
//    }
//    if ([sortedFriendsArray count] != 0)
//    {
//        
//        
//        NSLog(@"sortedFriendsArray%@",sortedFriendsArray);
//        NSLog(@"sortedFriendsArray%d",[sortedFriendsArray count]);
//        
//        //        int remainingUser=[sortedFriendsArray count]-noOfLoadImages;
//        //
//        //
//        //        NSString *newString = [[NSString stringWithFormat:@"%d",remainingUser] stringByReplacingOccurrencesOfString:@"-" withString:@""];
//        //
//        //
//        //        if ([newString intValue]<6)
//        //        {
//        //            plusValue=[newString intValue];
//        //        }
//        [tempProfileDataArray removeAllObjects];
//        
//        for (int i=0; i<[sortedFriendsArray  count]; i++)
//        {
//            [tempProfileDataArray addObject:[sortedFriendsArray objectAtIndex:i]];
//            
//        }
//        NSLog(@"tempProfileDataArray%@",tempProfileDataArray);
//        [_tableview reloadData];
//        
//        //[self.tableview reloadData];
//    }
//    else
//    {
//        [tempProfileDataArray removeAllObjects];
//        
//        for (int i=0; i<noOfLoadImages; i++)
//        {
//            [tempProfileDataArray addObject:[[profileDataDictionary valueForKey:@"response"] objectAtIndex:i]];
//            //count++;
//        }
//        
//        
//        // noOfLoadImages=noOfLoadImages+7;
//        
//        [_tableview reloadData];
    
        
        //        [arraySearchPlaces removeAllObjects];
        //        [self.tblResults reloadData];
   // }
    
    
   
    
    return YES;
}

- (void)searchRestaurantsWithText:(NSString *)searchText
{
    
    NSMutableArray *sortedRestaurantsArray =[[NSMutableArray alloc] init];
    
    // restaurantDictionary[@"response"][indexPath.row ][@"res_nam"]
    
    
    for (int i = 0; i<[backupDataArray  count]; i++)
    {
        NSString *strPlaceName = [backupDataArray  [i] valueForKey:@"res_nam"];
        
        if ([[strPlaceName lowercaseString] rangeOfString:[searchText lowercaseString]].location != NSNotFound)
        {
            [sortedRestaurantsArray addObject:backupDataArray[i] ];
        }
    }
    
    dataArray =[[NSMutableArray alloc] init];
    dataArray=sortedRestaurantsArray;
    
}


#pragma mark IBAction methods

- (IBAction)bckButtonClckd:(id)sender
{
    if ([[singletonClass sharedInstance] getIsEntertainmentDisabled]==YES)
    {
        [[singletonClass sharedInstance] assignSelectedSection:DiningSection];
        [[singletonClass sharedInstance] assignRootViewControllerWithAnimation:NO];
    }
    [self.navigationController popViewControllerAnimated:YES];
}



- (IBAction)settingsBtnClckd:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isRestaurntListView"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    settingsViewController *settingsView=[[settingsViewController alloc] initWithNibName:@"settingsViewController" bundle:nil];
    [self.navigationController pushViewController:settingsView animated:YES];
}

- (IBAction)ownerContctBtnClckd:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isRestaurntListView"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
       clientContactViewController *clientContctview=[[clientContactViewController alloc] initWithNibName:@"clientContactViewController" bundle:nil];
    [self.navigationController pushViewController:clientContctview animated:YES];

}

- (IBAction)notifctnBtnClckd:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isRestaurntListView"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
//    NotificationViewController *notfctnView=[[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
//    [self.navigationController pushViewController:notfctnView animated:YES];

    [super displayNotificationView];
}

- (IBAction)searchButtonClicked:(id)sender
{
    DailyDealSearchViewController * dailiesSearchView = [[DailyDealSearchViewController alloc] initWithNibName:@"DailyDealsSearchView" bundle:nil];
    [self.navigationController pushViewController:dailiesSearchView animated:YES];
}

- (IBAction)entertainmentButtonClicked:(id)sender
{
    [super displayEntertainmentHomeView];
}

- (IBAction)diningButtonClicked:(id)sender
{
    [super displayDiningHomeView];
}

- (IBAction)menuButtonClicked:(id)sender
{
    [super menuButtonTapped];
}


@end
