//
//  socialViewController.h
//  Dash
//
//  Created by CS_Mac4 on 18/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface socialViewController : UIViewController
{
    UIWebView *socialNetworkWebView;
}
- (IBAction)bckButtonClckd:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *bckBtn;
@property (strong, nonatomic) NSString *socialUrlString;
@end
