//
//  singletonClass.m
//  Dash
//
//  Created by CS_Mac4 on 12/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import "singletonClass.h"
#import "Header.h"
#import "Reachability.h"
#import "DailyDealsSearchRequest.h"

@implementation singletonClass

static singletonClass *shareInstance=Nil;

- (void)assignEntertainmentDisabled:(BOOL)disabled{
    
    self.isEntertainmentDisabled=disabled;
}

- (BOOL)getIsEntertainmentDisabled
{
    return self.isEntertainmentDisabled;

}

- (NSString *)getSelectedSectionId
{
    return (self.selectedSection == DiningSection) ? @"1" : @"2";
}

- (BOOL)isDiningSectionSelected
{
    return (self.selectedSection == DiningSection);
}

- (void)assignSelectedSection:(SelectedSection)selectedSection
{
    self.selectedSection = selectedSection;
}

- (UINavigationController *)assignRootViewControllerWithAnimation:(BOOL)isAnimated
{
    ViewController *viewControllerToBeShown = [self getSelectedCategoryViewController];
    UINavigationController *nvc = [[UINavigationController alloc] initWithRootViewController:viewControllerToBeShown];
    
    if(!isAnimated)
    {
        [((AppDelegate *)ApplicationDelegate).window setRootViewController:nvc];
    }
    else
    {
        UIView *myView1 = ((AppDelegate *)ApplicationDelegate).window.rootViewController.view;
        
        UIView *myView2 = viewControllerToBeShown.view;
        
        myView2.frame = ((AppDelegate *)ApplicationDelegate).window.bounds;
        
        
        [((AppDelegate *)ApplicationDelegate).window addSubview:myView2];
        
        
        CATransition* transition = [CATransition animation];
        transition.startProgress = 0;
        transition.endProgress = 1.0;
        transition.type = kCATransitionPush;
        transition.subtype = (self.selectedSection == EntertainmentSection) ? kCATransitionFromRight : kCATransitionFromLeft;
        transition.duration = 0.25;
        
        // Add the transition animation to both layers
        [myView1.layer addAnimation:transition forKey:@"transition"];
        [myView2.layer addAnimation:transition forKey:@"transition"];
        
        myView1.hidden = YES;
        myView2.hidden = NO;
        
        
        ((AppDelegate *)ApplicationDelegate).window.rootViewController = nvc;
    }
    
    return nvc;
}

- (ViewController *)getSelectedCategoryViewController
{
    if(self.selectedSection == EntertainmentSection)
    {
        if(!self.entertainmentCategoryViewController)
        {
            NSString *nibName = (IS_IPHONE_5) ? @"ViewController" : @"ViewControlleri4";
            self.entertainmentCategoryViewController = [[ViewController alloc] initWithNibName:nibName bundle:nil];
        }
        
        return self.entertainmentCategoryViewController;
    }
    else
    {
        if(!self.diningCategoryViewController)
        {
            NSString *nibName = (IS_IPHONE_5) ? @"ViewController" : @"ViewControlleri4";
            self.diningCategoryViewController = [[ViewController alloc] initWithNibName:nibName bundle:nil];
        }
        
        return self.diningCategoryViewController;
    }
}

- (UIViewController *)getTopControllerOnWindow
{
    UINavigationController *navController = (UINavigationController *)((AppDelegate *)ApplicationDelegate).window.rootViewController;
    UIViewController *topViewController = [navController topViewController];
    NSLog(@"topViewController: %@", topViewController);
    
    return topViewController;
}

-(BOOL)isInternetConnected
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        NSLog(@"There IS NO internet connection");
        return NO;
    }
    else
    {
        NSLog(@"There IS internet connection");
        return YES;
    }
    
}
+(id)sharedInstance
{
    if (shareInstance==Nil)
    {
        shareInstance=[[singletonClass alloc]init];
        
    }
    return shareInstance;
}

-(NSMutableArray *)blockRestaurant : (NSString *)userId : (NSString *)resId
{
    
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:serviceBlockRestaurant,userId,resId]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response= nil;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data)
    {
        NSError *localError = nil;
        NSMutableArray *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        
       // NSLog(@"Json Data=\n%@",resultDictionary);
        return [resultDictionary valueForKey:@"response"];
    }
    return nil;
}


-(NSMutableArray *)getCities
{
        
    NSURL *url=[NSURL URLWithString:serviceGetCities];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response= nil;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data)
    {
        NSError *localError = nil;
        NSMutableArray *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        
       // NSLog(@"Json Data=\n%@",resultDictionary);
        return [resultDictionary valueForKey:@"response"];
    }
    return nil;
    
    
}
-(NSMutableArray*)getDinningPopupArray
{
    NSArray *CategoryIDArray=[[NSMutableArray alloc] initWithObjects:@"14",@"1",@"2",@"16",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"15",nil];
    
    NSArray * isPopUpShowed=[[NSMutableArray alloc] initWithObjects: @"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",nil];
    
    NSMutableArray *DinningCategories=[[NSMutableArray alloc]init];
    for (int i=0; i<[CategoryIDArray count]; i++) {
        NSMutableDictionary *dic =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CategoryIDArray objectAtIndex:i],@"catId",[isPopUpShowed objectAtIndex:i],@"isPopUpShowed", nil];
        
        [DinningCategories addObject:dic];
    }
    return [DinningCategories copy];

}

-(NSMutableArray*)getEntertainmentPopupArray
{
    NSArray *CategoryIDArray=[[NSMutableArray alloc] initWithObjects:@"17",@"18",@"27",@"19",@"20",@"21",@"22",@"23",@"24",@"26",@"25",nil];
   NSArray * isPopUpShowed=[[NSMutableArray alloc] initWithObjects: @"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",@"NO",nil];
    
    NSMutableArray *EntertainmentCategories=[[NSMutableArray alloc]init];
    for (int i=0; i<[CategoryIDArray count]; i++) {
        NSMutableDictionary *dic =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CategoryIDArray objectAtIndex:i],@"catId",[isPopUpShowed objectAtIndex:i],@"isPopUpShowed", nil];
        
        [EntertainmentCategories addObject:dic];
    }
    return [EntertainmentCategories copy];
}

- (NSArray *)getDinningCategories
{
    NSArray * CategoryNameArr=[[NSMutableArray alloc] initWithObjects: @"ALLERGY SENSITIVE",@"CAFE",@"CASUAL DINING" ,@"CATERING",@"DELIVERY",@"ETHNIC"
                               ,@"FAST FOOD",@"LOUNGES",@"NIGHTLIFE ",@"PIZZA",@"PUBS",@"RURAL",@"SEAFOOD",@"SEASONAL",@"STEAKHOUSE",@"TREATS",nil];
    
    NSArray *CategoryIDArray=[[NSMutableArray alloc] initWithObjects:@"14",@"1",@"2",@"16",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"15",nil];
    NSArray * CategoryIconArray=[[NSMutableArray alloc] initWithObjects: @"sensetive_icon.png",@"cafe_icon.png",@"casualDining_icon.png",@"catering_icon.png",@"delivery_icon.png",@"ethnic_icon.png"
                            ,@"fastfood_icon.png",@"launge_icon.png",@"nightlife_icon.png ",@"pizza_icon.png",@"pubs_icon.png",@"rurel_icon.png",@"seafood_icon.png",@"seasonal_icon.png",@"steakHouse_icon.png",@"treats_icon.png",nil];
    NSMutableArray *DinningCategories=[[NSMutableArray alloc]init];
    for (int i=0; i<[CategoryIDArray count]; i++) {
        NSMutableDictionary *dic =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CategoryNameArr objectAtIndex:i],@"CategoryName",[CategoryIDArray objectAtIndex:i],@"CategoryId",[CategoryIconArray objectAtIndex:i],@"CategoryIcon", nil];
        
        [DinningCategories addObject:dic];
    }
    return [DinningCategories copy];
}

- (NSArray *)getEntertainmentCategories
{
    NSArray * CategoryNameArr=[[NSMutableArray alloc] initWithObjects:@"ADULTS ",@"CAMPING",@"EDUCATIONAL",@"EVENTS",@"GOLF",@"INDOOR",@"KIDS",@"OUTDOOR",@"SPORTS",@"SPORTS TEAMS",@"THEATRE",nil];
    
    NSArray *CategoryIDArray=[[NSMutableArray alloc] initWithObjects:@"17",@"18",@"27",@"19",@"20",@"21",@"22",@"23",@"24",@"26",@"25",nil];
    
    NSArray * CategoryIconArray=[[NSMutableArray alloc] initWithObjects:@"adults_icon.png ",@"camping_icon.png",@"education_icon.png",@"event_icon.png",@"golf_icon.png",@"indoor_icon.png",@"kids_icon.png",@"outdoor_icon.png",@"sport_icon.png",@"sportTeam_icon.png",@"theater_icon.png",nil];
    

    NSMutableArray *EntertainmentCategories=[[NSMutableArray alloc]init];
    for (int i=0; i<[CategoryIDArray count]; i++) {
        NSMutableDictionary *dic =[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CategoryNameArr objectAtIndex:i],@"CategoryName",[CategoryIDArray objectAtIndex:i],@"CategoryId",[CategoryIconArray objectAtIndex:i],@"CategoryIcon", nil];
        
        [EntertainmentCategories addObject:dic];
    }
    return [EntertainmentCategories copy];
}

- (NSMutableArray *)getCategories
{
    return [[NSMutableArray alloc] initWithObjects:@"ALL",@"CAFE",@"CASUAL DINING",@"DELIVERY",@"ETHNIC",@"FAST FOOD",@"LOUNGES",@"NIGHTLIFE",@"PIZZA",@"PUBS",@"RURAL",@"SEA FOOD",@"SEASONAL",@"STEAK HOUSE",@"ALLERGY SENSITIVE",@"TREATS",@"CATERING",
            @"ADULTS", @"CAMPING", @"EVENTS", @"GOLF", @"INDOOR", @"KIDS", @"OUTDOOR", @"SPORTS", @"THEATRE", @"SPORTS TEAMS", @"EDUCATIONAL", nil];
}

-(NSDictionary *)getStatusForCategories : (NSString *)userId : (NSString *)sectionID
{
    
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:serviceGetStatusForCategories,userId,sectionID]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response= nil;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data)
    {
        NSError *localError = nil;
        NSDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        
          NSLog(@"Json Data=\n%@",resultDictionary);
        return [resultDictionary valueForKey:@"response"];
    }
    return nil;
    
}
//-(NSMutableArray *)updateStatusForCategories : (NSString *)userId : (NSArray *)statusArray :(int)sectionId
//{
//    NSError *error = nil;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusArray options:NSJSONWritingPrettyPrinted error:&error];
//    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    NSString *urlStr=[NSString stringWithFormat:serviceUpdateStatusForCategories,userId,sectionId,jsonString];
//    NSURL *url=[NSURL URLWithString:urlStr];
//    
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url] ;
//    [request setHTTPMethod:@"GET"];
//    NSURLResponse *response= nil;
//    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//    if(data)
//    {
//        NSError *localError = nil;
//        NSMutableArray *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
//        
//        NSLog(@"Json Data=\n%@",resultDictionary);
//        return [resultDictionary valueForKey:@"response"];
//    }
//    return nil;
//    
//    
//}

-(NSMutableArray *)updateStatusForCategories : (NSString *)userId : (NSArray *)statusArray :(int)sectionId
{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusArray options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSURL *url=[NSURL URLWithString:serviceUpdateStatusForCategories];
    
    NSString *post =[[NSString alloc] initWithFormat:@"user_id=%@&section_id=%d&cat_status=%@",userId,sectionId,jsonString];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];

    NSURLResponse *response= nil;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data)
    {
        NSError *localError = nil;
        NSMutableArray *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        
        NSLog(@"Json Data=\n%@",resultDictionary);
        return [resultDictionary valueForKey:@"response"];
    }
    return nil;
    
    
}
-(NSMutableArray*)getAllCategoryPopUpImages:(NSString *)sectionId :(NSString *)cityId
{
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:serviceGetAllCategoryPopUpImages,cityId,sectionId]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response= nil;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data)
    {
        NSError *localError = nil;
        NSMutableArray *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        
        NSLog(@"Json CategoryPopUpImages=\n%@",resultDictionary);
        if ([[resultDictionary valueForKey:@"response"] isKindOfClass:[NSArray class]]) {
            return [resultDictionary valueForKey:@"response"];
        }
        return nil;
    }
    return nil;
}

-(NSMutableArray *)getRestaurantsByCat : (NSString *)CategoryId : (NSString *)userId : (NSString *)cityId
{
    
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:serviceGetRestaurantsByCat,CategoryId,userId,cityId]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response= nil;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data)
    {
        NSError *localError = nil;
        NSMutableArray *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        
        NSLog(@"Json Dataddfd=\n%@",resultDictionary);
        return [resultDictionary valueForKey:@"response"];
    }
    return nil;
    
    
}

-(NSMutableDictionary *)getRestaurantDetail : (NSString *)restaurntId : (NSString *)userId : (NSString *)bannerId : (NSString *)sectionId
{
    NSURL *url;
    if (bannerId == nil) {
        url = [NSURL URLWithString:[NSString stringWithFormat:serviceGetSingleRestaurantWithoutBannerId,restaurntId, userId, sectionId]];
    }
    else{
        url=[NSURL URLWithString:[NSString stringWithFormat:serviceGetSingleRestaurant,restaurntId, userId, bannerId, sectionId]];
    }
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response= nil;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data)
    {
        NSError *localError = nil;
        NSMutableDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        //NSLog(@"Json Data=\n%@",resultDictionary);
        return resultDictionary;
    }
    return nil;
}

-(NSMutableDictionary *)getRestaurantHours : (NSString *)restaurntId
{
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:serviceStoreHours,restaurntId]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response= nil;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data)
    {
        NSError *localError = nil;
        NSMutableDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
       // NSLog(@"Json Data=\n%@",resultDictionary);
        return resultDictionary;
    }
    return nil;
    
    
}

-(NSMutableDictionary *)getAllAdvrtsmnt : (NSString *)status :(NSString *)cityId
{
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:serviceGetAllAdvrtsmnt,status,cityId, [[singletonClass sharedInstance] getSelectedSectionId]]];
    NSLog(@"url =%@",url);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response= nil;
    
    
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data)
    {
        NSError *localError = nil;
        NSMutableDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        
       // NSLog(@"Json Data=\n%@",resultDictionary);
        return resultDictionary;
    }
    return nil;
}


-(NSMutableDictionary *)getCouponDetail : (NSString *)restaurntId
{
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:serviceGetCouponDetail,restaurntId]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response= nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data)
    {
        NSError *localError = nil;
        NSMutableDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        
       // NSLog(@"Json Data=\n%@",resultDictionary);
        return resultDictionary;
    }
    return nil;
    
    
    }

  -(NSMutableDictionary *)getSpecialtyDetail : (NSString *)restaurntId
{
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:serviceGetSpeclty,restaurntId]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response= nil;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data)
    {
        NSError *localError = nil;
        NSMutableDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        
       // NSLog(@"Json Data=\n%@",resultDictionary);
        return resultDictionary;
    }
    return nil;
    
    
}


-(NSMutableDictionary *)getDealDetail : (NSString *)restaurntId
{
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:serviceGetDealDetail,restaurntId]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response= nil;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data)
    {
        NSError *localError = nil;
        NSMutableDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&localError];
        
       // NSLog(@"Json Data=\n%@",resultDictionary);
  
        return resultDictionary;
    }
    return nil;
    
}

-(NSMutableDictionary *)getMenuList : (NSString *)restaurntId
{
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:serviceGetMenuList,restaurntId]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response= nil;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data)
    {
        NSError *localError = nil;
        NSMutableDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        
        NSLog(@"Json Dataddfd=\n%@",resultDictionary);
        return resultDictionary;
    }
    return nil;
    
    
}

-(NSMutableDictionary *)registerUser : (NSString *)tokenId
{
    NSURL *url=[NSURL URLWithString:serviceRegisterUser];
    
    NSString *post =[[NSString alloc] initWithFormat:@"token_id=%@&b_status=Yes&l_status=Yes&d_status=Yes&s_status=Yes&v=1.5.5",tokenId];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    // now lets make the connection to the web
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSMutableDictionary *parsedData;
    if (returnData)
    {
        NSError *localError = nil;
        parsedData = [NSJSONSerialization JSONObjectWithData:returnData options:0 error:&localError];
        
        NSLog(@"Returning Result = %@",parsedData);
        
        
      
        [[NSUserDefaults standardUserDefaults] setObject:parsedData[@"response"][@"id"] forKey:@"userId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"%@",returnString);
        
    }
    return parsedData;
    
}


-(NSMutableArray *)getAllRestaurant:(NSString *)userId :(NSString *)cityId
{
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:serviceGetAllRestaurants,userId,cityId, [[singletonClass sharedInstance] getSelectedSectionId]]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response= nil;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data)
    {
        NSError *localError = nil;
        NSMutableArray *resultArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        
       // NSLog(@"Json Data=\n%@",resultArray);
        return [resultArray valueForKey:@"response"];
    }
    return nil;
}

-(NSMutableDictionary *)updateSettings :(NSString *)selctdCtyID : (NSString *)regId
{
   NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:serviceUpdateSettings,selctdCtyID,regId]];
    
    NSLog(@"%@",url);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response= nil;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data)
    {
        NSError *localError = nil;
        NSMutableDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
       NSLog(@"JSON Output: %@", jsonString);
       // NSLog(@"Json Data=\n%@",resultDictionary);
        return resultDictionary;
    }
    return nil;
    
}


-(NSMutableDictionary *)getAllNotifications : (NSString *)regId :(NSString *)selctdCtyID
{
    
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:serviceGetAllNotifications,regId,selctdCtyID, [[singletonClass sharedInstance] getSelectedSectionId]]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response= nil;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *returnString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    if(data)
    {
        NSError *localError = nil;
        NSMutableDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        
       //NSLog(@"Json Data=\n%@",resultDictionary);
        
       //NSLog(@"Json returnString=\n%@",returnString);
        return resultDictionary;
    }
    return nil;
}

-(NSMutableDictionary *)getAllBadges : (NSString *)userId :(NSString *)selctdCtyID
{
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:serviceGetAllBadgesCount, userId, selctdCtyID]];
    
    NSLog(@"%@",url);
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSError *error = nil;
    NSURLResponse *response= nil;
    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(data)
    {
        NSError *localError = nil;
        NSMutableDictionary *resultDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
        NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"JSON Output: %@", jsonString);
        //NSLog(@"Json Data=\n%@",resultDictionary);
        return resultDictionary;
    }
    return nil;
    
}


- (void)searchDailyDealsWithRequest:(DailyDealsSearchRequest *)searchRequest withCompletionBlock:(void (^)( NSData *data, NSError *connectionError ))returnBlock
{
    NSURL *url = [self generateUrlForDailySearch:searchRequest];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue new] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        returnBlock(data, connectionError);
        
    }];
}

- (NSURL *)generateUrlForDailySearch:(DailyDealsSearchRequest *)searchRequest
{
    NSMutableCharacterSet *seperatorSet = [NSMutableCharacterSet whitespaceAndNewlineCharacterSet];
    NSString* modifiedSearchText = [[[searchRequest.searchText componentsSeparatedByCharactersInSet:seperatorSet] componentsJoinedByString:@" "] mutableCopy];
    NSString * string = [NSString stringWithFormat:serviceSearchDailyDeal, @(searchRequest.searchDay), modifiedSearchText, searchRequest.cityId, [[singletonClass sharedInstance] getSelectedSectionId]];
    
    return [NSURL URLWithString:[string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
}



@end
