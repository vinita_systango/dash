//
//  NotificationSettingTableCell.h
//  Dash
//
//  Created by NR on 21/09/2015.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationSettingTableCell : UITableViewCell
@property(nonatomic, strong) IBOutlet UILabel * CategoryName;
@property(nonatomic, strong) IBOutlet UIImageView * CategoryIcon;
@property(nonatomic, strong) IBOutlet UIButton * NotiONOffBtn;

@end
