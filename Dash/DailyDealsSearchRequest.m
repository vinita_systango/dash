//
//  SearchDetails.m
//  Dash
//
//  Created by stplmacmini8 on 6/8/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import "DailyDealsSearchRequest.h"

@implementation DailyDealsSearchRequest

#pragma mark init methods
- (instancetype) initWithSearchText:(NSString *) searchText searchDay:(NSInteger)searchDay cityId:(NSString *)cityId
{
    self = [super init];
    if (self) {
        _searchText = searchText;
        _searchDay = searchDay;
        _cityId = cityId;
    }
    return self;
}


@end
