//
//  UpdateNotificationSettingViewController.m
//  Dash
//
//  Created by NR on 10/09/2015.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import "UpdateNotificationSettingViewController.h"
#import "settingsViewController.h"
#import "RadioButton.h"
@interface UpdateNotificationSettingViewController ()
@property(nonatomic,strong) NSMutableArray *categoryArray,* comparisionArray;
@property(nonatomic,strong) NSDictionary *statusResponse;
@property (nonatomic,assign)BOOL checkBoxSelected,isCheckBoxAdded;
@end

@implementation UpdateNotificationSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.isCheckBoxAdded=NO;
    self.categoryArray=[[NSMutableArray alloc]init];
    
    UIButton *bckBtn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 22, 18)];
        
    [bckBtn setBackgroundImage:[UIImage imageNamed:@"back_black_btn"] forState:UIControlStateNormal];
        
    [bckBtn addTarget:self action:@selector(popToBack) forControlEvents:UIControlEventTouchUpInside];
        
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:bckBtn]];
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor darkGrayColor],
                                               NSForegroundColorAttributeName,
                                               [UIFont fontWithName:@"Rockwell" size:16.0],
                                               NSFontAttributeName,
                                               nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    
    
    if(self.selectedNotificationSection == DiningSection)
    {
       
         self.title=@"DINING SETTINGS";
    }
    else{
        self.title=@"ENTERTAINMENT SETTINGS";
    }
    
    [self getStatusFromServer];
   
   
   }

-(void)getStatusFromServer
{
   
       NSArray *CategoryArray;
    if(self.selectedNotificationSection == DiningSection)
    {
        CategoryArray=[[singletonClass sharedInstance]getDinningCategories];
        
    }
    else{
        CategoryArray=[[singletonClass sharedInstance]getEntertainmentCategories];
    }
    
  
    if ((![[NSUserDefaults standardUserDefaults] boolForKey:@"hasDinningNotificationLaunchedOnce"] && self.selectedNotificationSection == DiningSection)  || (![[NSUserDefaults standardUserDefaults] boolForKey:@"hasEntertainmentNotificationLaunchedOnce"] && self.selectedNotificationSection == EntertainmentSection))
    {
      NSMutableArray *tempArray=[[NSMutableArray alloc]init];
            for (NSMutableDictionary *dic1 in CategoryArray) {
                NSString *status=self.statusResponse[@"allcat_status"];
                if (status==nil) {
                    status=@"1";
                }
                [dic1 setValue:status forKey:@"category_status"];
                NSMutableDictionary *dic2=[[NSMutableDictionary alloc]initWithObjectsAndKeys:status,@"category_status",dic1[@"CategoryId"],@"id",dic1[@"CategoryName"],@"cat_nam",[NSString stringWithFormat:@"%d",self.selectedNotificationSection],@"section",dic1[@"CategoryIcon"],@"cat_icon", nil];
                [tempArray addObject:[dic2 mutableCopy]];
            }
           self.categoryArray=tempArray;
        }
    else{
        self.statusResponse=[[singletonClass sharedInstance]getStatusForCategories:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] :[NSString stringWithFormat:@"%d",self.selectedNotificationSection]];
        
        self.comparisionArray =[[NSMutableArray alloc]init];
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"cat_nam"  ascending:YES];
        NSArray *tempArray=[self.statusResponse[@"cat_status"] sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
        for (int i=0;i<[tempArray count];i++) {
            NSMutableDictionary *mDic = [[tempArray objectAtIndex:i] mutableCopy];
            NSDictionary *oldDic =[CategoryArray objectAtIndex:i];
            [mDic setValue:[oldDic objectForKey:@"CategoryName"] forKey:@"cat_nam"];
            [mDic setValue:[oldDic objectForKey:@"CategoryIcon"] forKey:@"cat_icon"];
            
            [self.categoryArray addObject:mDic];
        }

        if ( ![self.statusResponse[@"allcat_status"] isEqualToString:@"1"])
        {
            NSMutableArray *tempArray=[[NSMutableArray alloc]init];
            for (NSMutableDictionary *dic1 in CategoryArray) {
                NSString *status=self.statusResponse[@"allcat_status"];
                if (status==nil) {
                    status=@"1";
                }
                [dic1 setValue:status forKey:@"category_status"];
                NSMutableDictionary *dic2=[[NSMutableDictionary alloc]initWithObjectsAndKeys:status,@"category_status",dic1[@"CategoryId"],@"id",dic1[@"CategoryName"],@"cat_nam",[NSString stringWithFormat:@"%d",self.selectedNotificationSection],@"section",dic1[@"CategoryIcon"],@"cat_icon", nil];
                [tempArray addObject:[dic2 mutableCopy]];
            }
            self.categoryArray=nil;
            self.categoryArray=tempArray;
        }
    }
             [self.settingTable reloadData];

}

-(void)popToBack
{
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"hasDinningNotificationLaunchedOnce"] && self.selectedNotificationSection == DiningSection)
        
    {
        
        NSMutableArray *tempArray=[[NSMutableArray alloc]init];
        for (NSMutableDictionary * dic in self.categoryArray)
        {
            NSDictionary *dic2=[[NSDictionary alloc]initWithObjectsAndKeys:dic[@"category_status"],@"status",dic[@"id"],@"cat_id" ,nil];
            [tempArray addObject:[dic2 mutableCopy]];
            
        }
        [[singletonClass sharedInstance]updateStatusForCategories:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]:tempArray :self.selectedNotificationSection];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasDinningNotificationLaunchedOnce"];
        
    }
    
    else if (![[NSUserDefaults standardUserDefaults] boolForKey:@"hasEntertainmentNotificationLaunchedOnce"] && self.selectedNotificationSection == EntertainmentSection)
        
    {
        
        NSMutableArray *tempArray=[[NSMutableArray alloc]init];
        for (NSMutableDictionary * dic in self.categoryArray)
        {
            NSDictionary *dic2=[[NSDictionary alloc]initWithObjectsAndKeys:dic[@"category_status"],@"status",dic[@"id"],@"cat_id" ,nil];
            [tempArray addObject:[dic2 mutableCopy]];
            
        }
        [[singletonClass sharedInstance]updateStatusForCategories:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]:tempArray :self.selectedNotificationSection];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasEntertainmentNotificationLaunchedOnce"];
        
        
        
    }
    else {
    
       if ([self.comparisionArray count]>0) {
           NSMutableArray *tempArray=[[NSMutableArray alloc]init];
           for (NSMutableDictionary * dic in self.comparisionArray)
           {
               NSDictionary *dic2=[[NSDictionary alloc]initWithObjectsAndKeys:dic[@"category_status"],@"status",dic[@"id"],@"cat_id" ,nil];
               [tempArray addObject:[dic2 mutableCopy]];
               
           }
           [[singletonClass sharedInstance]updateStatusForCategories:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]:tempArray :self.selectedNotificationSection];
       
    }
    }
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated{
    [self.settingTable reloadData];
}
-(void) onRadioButtonValueChanged:(RadioButton*)sender
{
    UILabel *lbl=(UILabel*)sender.titleLabel;
    if ([lbl.text isEqualToString:@"ON"])
    {
        for (NSMutableDictionary *dic1 in self.categoryArray) {
            
            [dic1 setValue:[NSNumber numberWithInt:1] forKey:@"category_status"];
        }
       
        
    }
    else
    {
        for (NSMutableDictionary *dic1 in self.categoryArray) {
            
            [dic1 setValue:[NSNumber numberWithInt:0] forKey:@"category_status"];
        }
        
    }
    self.comparisionArray=nil;
    self.comparisionArray=self.categoryArray;
    [self.settingTable reloadData];

}

-(void)addRadioButtonGroupOnView:(UIView*)tableCellContentView
{
    
    NSMutableArray* buttons = [NSMutableArray arrayWithCapacity:2];
    CGRect btnRect = CGRectMake(165, 20, 70, 30);
    
    for (NSString* optionTitle in @[@"ON", @"OFF"])
    {
        RadioButton* btn = [[RadioButton alloc] initWithFrame:btnRect];
        [btn addTarget:self action:@selector(onRadioButtonValueChanged:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag=
        btnRect.origin.x = CGRectGetMaxX(btnRect)+10;
        [btn setTitle:optionTitle forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:17];
        [btn setImage:[UIImage imageNamed:@"unchecked.png"] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateSelected];
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 6, 0, 0);
        [tableCellContentView addSubview:btn];
        btn.hidden=YES;
        [buttons addObject:btn];
    }
    
    [buttons[0] setGroupButtons:buttons]; // Setting buttons into the group
    
}
-(void)checkboxSelected:(id)sender
{
    self.checkBoxSelected = !self.checkBoxSelected; /* Toggle */
    [(UIButton*)sender setSelected:self.checkBoxSelected];
    NSLog(@"inside checkboxSelected");
        UIView*contentView= [(UIButton*)sender superview];
        for (UIView *view in [contentView subviews]){
            if ([view isKindOfClass:[RadioButton class]]) {
                if (self.checkBoxSelected==YES)
                {
                view.hidden=NO;
                }
                else
                {
                view.hidden=YES;

                }
            }
        }
   
   }
-(void)addCheckBoxOnView:(UIView*)tableCellContentView{
    UIButton *checkbox ;
   
    checkbox = [[UIButton alloc] initWithFrame:CGRectMake(15,23,25,25)];
                
                [checkbox setImage:[UIImage imageNamed:@"unchecked-icon.png"]
                                    forState:UIControlStateNormal];
                [checkbox setImage:[UIImage imageNamed:@"checked-icon.png"] forState:UIControlStateSelected];

               [checkbox addTarget:self action:@selector(checkboxSelected:) forControlEvents:UIControlEventTouchUpInside];
                [tableCellContentView addSubview:checkbox];
}
#define tableViewDelegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    return [self.categoryArray  count]+1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
  
    if (indexPath.row==0)
      {
          UILabel *lblAll = [[UILabel alloc]initWithFrame:CGRectMake(55, 25,180, 20)];

          UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
          if (cell == nil)
          {
              cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
              cell.selectionStyle=UITableViewCellSelectionStyleNone;
              
          }
          if (self.isCheckBoxAdded==NO) {
              [self addRadioButtonGroupOnView:cell.contentView];
              [self addCheckBoxOnView:cell.contentView];
              self.isCheckBoxAdded=YES;
          }
         
          
          lblAll.text=@"ALL";
          lblAll.font = [UIFont systemFontOfSize:17];
          
          lblAll.textColor = [UIColor blackColor];
          lblAll.textAlignment = NSTextAlignmentLeft;
          [cell.contentView addSubview:lblAll];
       return cell;
      }
    else
    {
        
        NotificationSettingTableCell *cell =(NotificationSettingTableCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:@"NotificationSettingTableCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        }
       
        int index=(int)indexPath.row-1;

        cell.CategoryName.text=self.categoryArray [index][@"cat_nam"];
        cell.NotiONOffBtn.tag=index;
       int status=[(self.categoryArray [index ][@"category_status"]) intValue];

        if (status==0)
        {
            [cell.NotiONOffBtn setImage:[UIImage imageNamed:@"off"] forState:UIControlStateNormal ];
        }
        else
        {
            [cell.NotiONOffBtn setImage:[UIImage imageNamed:@"on"] forState:UIControlStateNormal ];
        }
        [cell.NotiONOffBtn addTarget:self action:@selector(switchOnOffNotification:) forControlEvents:UIControlEventTouchUpInside];

        cell.CategoryIcon.image=[UIImage imageNamed:self.categoryArray [index][@"cat_icon"]];
        return cell;
        
    }
   
   
}

-(void)switchOnOffNotification:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    NSMutableDictionary *dic=[self.categoryArray objectAtIndex:btn.tag];
    int status=[[dic objectForKey:@"category_status"]intValue];
    BOOL found=NO;
    
    if ([self.comparisionArray count]>0) {
        for (NSMutableDictionary *dic2 in self.comparisionArray)
        {
            if ([[dic2 objectForKey:@"id"] isEqualToString:[dic objectForKey:@"id"]]) {
                found=YES;
                if (status==0)
                {
                    [dic2 setValue:[NSNumber numberWithInt:1] forKey:@"category_status"];
                }
                else
                {
                    [dic2  setValue:[NSNumber numberWithInt:0] forKey:@"category_status"];
                    
                }
            }
        }
      }
    if (found==NO) {
        [self.comparisionArray addObject:dic];
    }
   
    
    if (status==0)
    {
        [dic setValue:[NSNumber numberWithInt:1] forKey:@"category_status"];
        [btn setImage:[UIImage imageNamed:@"on"] forState:UIControlStateNormal];

    }
    else
    {
        [dic  setValue:[NSNumber numberWithInt:0] forKey:@"category_status"];
        [btn setImage:[UIImage imageNamed:@"off"] forState:UIControlStateNormal];

        
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
