//
//  ViewController.h
//  Dash
//
//  Created by CS_Mac4 on 12/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseDinningViewController.h"
#import "AsyncImageView.h"

@interface ViewController : BaseDinningViewController<UIScrollViewDelegate>
{
    AsyncImageView *_advertseImgView;
    NSTimer *advertsmntTimer;
    UIView *popUpView;
}
- (IBAction)categryButtonClckd:(id)sender;
@property CGFloat previousContentDelta;

@property CGFloat previousScrollViewYOffset;


@property CGRect originalFrame;


@property (strong, nonatomic) IBOutlet UIView *popUpView;
@property(strong,nonatomic) IBOutlet AsyncImageView * popUpImageview;
@property (strong, nonatomic) IBOutlet UIButton *forwardButton;
@property (strong, nonatomic) IBOutlet UIView *catBottomBar;
@property (strong, nonatomic) IBOutlet UIView *bottomBarView;
- (IBAction)allBtnClckd:(id)sender;
- (IBAction)settingsBtnClckd:(id)sender;
- (IBAction)notificationsBtnClckd:(id)sender;
- (IBAction)contactsBtnclckd:(id)sender;

@property CGFloat initialContentOffset;
@end
