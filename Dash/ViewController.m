//
//  ViewController.m
//  Dash
//
//  Created by CS_Mac4 on 12/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import "ViewController.h"
#import "restaurantViewController.h"
#import "settingsViewController.h"
#import "restaurantDetailViewController.h"
#import "clientContactViewController.h"
#import "DailyDealSearchViewController.h"

static const CGFloat kBottomBarHeight = 48.0;
static const CGFloat kAdvertisementImageViewHeight = 68.0;

@interface ViewController ()
{
    CGFloat bottomBarTopPos;
    CGFloat screenWidth;
    CGFloat screenHeight;
}
@property (strong, nonatomic) IBOutlet UIScrollView *diningCategoryScrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *entertainmentCategoryScrollView;
@property (nonatomic, weak) IBOutlet UIButton * searchButton;
@property (nonatomic, weak) IBOutlet UIButton * notificationsButton;
@property (nonatomic, weak) IBOutlet UIButton * contactButton;
@property (nonatomic, weak) IBOutlet UIButton * settingsButton;
@property (weak, nonatomic) IBOutlet UIButton *entertainmentBottomButton;
@property (weak, nonatomic) IBOutlet UIButton *diningBottomButton;


@end


@implementation ViewController
@synthesize popUpView,popUpImageview;

#pragma mark - View Controller Life Cycle

-(BOOL)getEntertainmentDisabledStatus
{
    return [[singletonClass sharedInstance] getIsEntertainmentDisabled];
}
-(void)updateEntertainmentStatus
{
    if ([self getEntertainmentDisabledStatus]==NO)
    {
        self.entertainmentBottomButton.userInteractionEnabled = !self.entertainmentBottomButton.isSelected;
        self.entertainmentBottomButton.titleLabel.textColor=[UIColor blackColor];
    }
    else
    {
        self.entertainmentBottomButton.userInteractionEnabled = NO;
        self.entertainmentBottomButton.titleLabel.textColor=[UIColor lightGrayColor];
        
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    screenWidth = self.view.frame.size.width;
    screenHeight = self.view.frame.size.height;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.title=@"DASH";
    BannerView *bannerView=[BannerView sharedBannerInstance];
    _advertseImgView=[bannerView getAdvertisementWithRect:CGRectMake(0, 0, screenWidth, kAdvertisementImageViewHeight) OnView:self ];
    [bannerView fetchBannerLowCostImagesFromServer];

    [self.view addSubview:_advertseImgView];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]]];
    
    _catBottomBar=[[UIView alloc] init];
    
    bottomBarTopPos = screenHeight - kBottomBarHeight;
    
    _catBottomBar.frame = CGRectMake(0, bottomBarTopPos, screenWidth, kBottomBarHeight);
    
    _bottomBarView.frame = CGRectMake(0, 0, screenWidth, kBottomBarHeight);
    
    
    [_catBottomBar addSubview:_bottomBarView];
    [self.view addSubview:_catBottomBar];
    
    self.originalFrame =  _bottomBarView.frame;
    
    [self setupButtonImages];
    
    
    // Do any additional setup after loading the view, typically from a nib.
    
        self.diningBottomButton.selected = ([[singletonClass sharedInstance] selectedSection] == DiningSection);
        self.entertainmentBottomButton.selected = !self.diningBottomButton.isSelected;
        self.diningBottomButton.userInteractionEnabled = !self.diningBottomButton.isSelected;
        self.diningCategoryScrollView.hidden = !self.diningBottomButton.isSelected;
        self.entertainmentCategoryScrollView.hidden = !self.entertainmentBottomButton.isSelected;
        self.entertainmentBottomButton.userInteractionEnabled = !self.entertainmentBottomButton.isSelected;

    [self underlineSelectedCategory];
    
    // add observer for EntertainmentStatusChanged
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateEntertainmentStatus) name:@"EntertainmentStatusChanged" object:nil];
    }
-(void)viewDidAppear:(BOOL)animated
{
    if ([self getEntertainmentDisabledStatus]==NO)
    {
        self.entertainmentBottomButton.userInteractionEnabled = !self.entertainmentBottomButton.isSelected;
        self.entertainmentBottomButton.titleLabel.textColor=[UIColor blackColor];
    }
    else
    {
        self.entertainmentBottomButton.userInteractionEnabled = NO;
        self.entertainmentBottomButton.titleLabel.textColor=[UIColor lightGrayColor];
        
    }
    [self underlineSelectedCategory];
}
-(void)viewWillAppear:(BOOL)animated
{
    if ([self getEntertainmentDisabledStatus]==NO)
    {
        self.entertainmentBottomButton.userInteractionEnabled = YES;
        self.entertainmentBottomButton.titleLabel.textColor=[UIColor blackColor];
    }
    else
    {
        self.entertainmentBottomButton.userInteractionEnabled = NO;
        self.entertainmentBottomButton.titleLabel.textColor=[UIColor lightGrayColor];
        
    }
    [self underlineSelectedCategory];

    BannerView *bannerView=[BannerView sharedBannerInstance];
    bannerView.highCostAdvertisementImages=nil;
    [bannerView fetchBannerHighCostImagesFromServer];

    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"cityId"]==nil )
    {
        UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:@"" message:@"Please select city from settings" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alrt show];
        
    }
    
    _diningCategoryScrollView.delegate=self;
    _entertainmentCategoryScrollView.delegate=self;
   
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(callService)
                                        object:nil];
    [queue addOperation:operation];
    
    [self.navigationController.navigationBar setHidden:YES];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:
     UIStatusBarAnimationNone];
    
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:
     UIStatusBarAnimationNone];
    [super viewWillDisappear:animated];
    if ([advertsmntTimer isValid]) {
        [advertsmntTimer invalidate];

    }
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setupButtonImages
{
    [[self.searchButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.settingsButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.contactButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.notificationsButton imageView] setContentMode: UIViewContentModeScaleAspectFit];

}
- (void)underlineSelectedCategory
{
    if ([[singletonClass sharedInstance] isDiningSectionSelected]) {
        NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:@"Dining"];
        [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
        [self.diningBottomButton setAttributedTitle:titleString forState:UIControlStateNormal];
    }
    
    else{
        NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:@"Entertainment"];
        [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
        [self.entertainmentBottomButton setAttributedTitle:titleString forState:UIControlStateNormal];
    }
    
}
-(void)startAdvertisementTimer
{
    if ([[[BannerView sharedBannerInstance]highCostAdvertisementImages][@"response"][0][@"id"]intValue]!=-1)
    {
        advertsmntTimer= [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(animateAdvertisements) userInfo:nil repeats:YES];
        
    }
}

-(void)animateAdvertisements
{
    [[BannerView sharedBannerInstance]animateAdvertisements:_advertseImgView ofType:YES];

}

-(void)getCategoryPopUpImagesFromServer
{
    
    NSMutableArray *imagesArray=  [[singletonClass sharedInstance] getAllCategoryPopUpImages:[[singletonClass sharedInstance] getSelectedSectionId]:[[NSUserDefaults standardUserDefaults] objectForKey:@"cityId"]];
    if (imagesArray!=nil) {
  
    for (NSDictionary *dic in imagesArray){
        NSString *imagePath=[dic objectForKey:@"image"];
        NSString* CategoryId=[dic objectForKey:@"catId"];
        NSPredicate *p = [NSPredicate predicateWithFormat:@"catId = %@", CategoryId];
        NSArray *matchedDicts;
        if ([[singletonClass sharedInstance] isDiningSectionSelected]) {
            matchedDicts = [[ApplicationDelegate dinningPopUpArray] filteredArrayUsingPredicate:p];
            
        }
        else {
            matchedDicts = [[ApplicationDelegate entertainmentPopUpArray] filteredArrayUsingPredicate:p];
            
        }
        NSMutableDictionary *dic =[matchedDicts objectAtIndex:0];
        [dic setValue:imagePath forKey:@"image"];
    }
    
//        NSLog(@"dinning =%@",[[ApplicationDelegate dinningPopUpArray] description]);
//        NSLog(@"entertainment =%@",[[ApplicationDelegate entertainmentPopUpArray] description]);

    }
    else{
        for (NSMutableDictionary *dic in [ApplicationDelegate dinningPopUpArray]){
            [dic removeObjectForKey:@"image"];
        }
        for (NSMutableDictionary *dic in [ApplicationDelegate entertainmentPopUpArray]){
            [dic removeObjectForKey:@"image"];
        }

    }
}
-(void)callService
{
    [self performSelectorOnMainThread:@selector(getCategoryPopUpImagesFromServer) withObject:nil waitUntilDone:NO];

    [self performSelectorOnMainThread:@selector(startAdvertisementTimer) withObject:nil waitUntilDone:NO];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(![[singletonClass sharedInstance] isInternetConnected])
    {
        [self netCoonectivityAlert];
        return;
    }
    
    settingsViewController *settingsView;
    if (IS_IPHONE_5)
    {
        settingsView=[[settingsViewController alloc] initWithNibName:@"settingsViewController" bundle:nil];
    }
    else {
        settingsView=[[settingsViewController alloc] initWithNibName:@"settingsViewControlleri4" bundle:nil];
    }
    
    [self.navigationController pushViewController:settingsView animated:YES];
}

- (void)didReceiveMemoryWarning
{
    //  1,200
    [super didReceiveMemoryWarning];
    
    
    // Dispose of any resources that can be recreated.
}


#define scrollViewDelegates

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.initialContentOffset = scrollView.contentOffset.y;
    self.previousContentDelta = 0.f;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat prevDelta = self.previousContentDelta;
    CGFloat delta = scrollView.contentOffset.y - self.initialContentOffset;
    if (delta > 0.f && prevDelta <= 0.f)
    {
        
        [UIView transitionWithView:self.view duration:0.1 options:UIViewAnimationOptionTransitionNone animations:^{
            
            _advertseImgView.frame = CGRectMake(0, 0, screenWidth, _advertseImgView.bounds.size.height);
            
            _catBottomBar.frame = CGRectMake(0, 570, screenWidth, kBottomBarHeight);
            
            
        } completion:nil];
        
    }
    else if (delta < 0.f && prevDelta >= 0.f)
    {
        [UIView transitionWithView:self.view duration:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            _catBottomBar.frame = CGRectMake(0, bottomBarTopPos, screenWidth, kBottomBarHeight);
            
        }
                        completion:nil];
        
        
    }
    self.previousContentDelta = delta;
}

-(void)netCoonectivityAlert
{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Internet is not connected!" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    
    [alert show];
}
-(IBAction)removePopUpView:(id)sender
{
    [self.popUpView removeFromSuperview];
     [self.popUpImageview setImageURL:nil];
    [self.popUpImageview removeFromSuperview];

    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isAll"];
    [[NSUserDefaults standardUserDefaults]synchronize ];
    
    restaurantViewController *restaurantView=[[restaurantViewController alloc] initWithNibName:@"restaurantViewController" bundle:nil];
    restaurantView.categryId=(int)[sender tag];
    [self.navigationController pushViewController:restaurantView animated:YES];
}
-(void)addPopUpView
{
    [self.popUpView addSubview:self.popUpImageview];
    [self.view addSubview:self.popUpView];

}
-(void)showPopUpWithImagePath:(NSString*)imagePath
{
            imagePath=[imagePath stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            NSString* UrlString = [Constant baseURLWithPath:[NSString stringWithFormat:@"cmsAdmin/panel/%@",imagePath]];
              NSLog(@"UrlString=%@",UrlString);
         [self.popUpImageview setImageWithURL:[NSURL URLWithString:UrlString]];
            [self.popUpImageview setContentMode:UIViewContentModeScaleAspectFit];
            [self performSelector:@selector(addPopUpView) withObject:nil afterDelay:0];

    
}
#pragma mark IBAction Methods

- (IBAction)allBtnClckd:(id)sender
{
    
  if(![[singletonClass sharedInstance] isInternetConnected])
  {
      [self netCoonectivityAlert];
      return;
  }
    [advertsmntTimer invalidate];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isAll"];
    [[NSUserDefaults standardUserDefaults]synchronize ];
    restaurantViewController *restaurantView;
    if (IS_IPHONE_5) {
        restaurantView=[[restaurantViewController alloc] initWithNibName:@"restaurantViewController" bundle:nil];
    }
    else
    {
        restaurantView=[[restaurantViewController alloc] initWithNibName:@"restaurantViewControlleri4" bundle:nil];
    }
   
    restaurantView.categryId = (int)[sender tag];
    [self.navigationController pushViewController:restaurantView animated:YES];
}

- (IBAction)categryButtonClckd:(id)sender
{
    
    NSPredicate *p = [NSPredicate predicateWithFormat:@"catId = %@", [NSString stringWithFormat:@"%d",(int)[sender tag]]];
    NSArray *matchedDicts;
    
    if ([[singletonClass sharedInstance] isDiningSectionSelected])
    {
        matchedDicts = [[ApplicationDelegate dinningPopUpArray] filteredArrayUsingPredicate:p];
        
    }
    else {
        matchedDicts = [[ApplicationDelegate entertainmentPopUpArray] filteredArrayUsingPredicate:p];
        
    }
    NSLog(@"matchedDicts =%@",[matchedDicts description]);
    NSLog(@"dinningPopUpArray =%@",[[ApplicationDelegate dinningPopUpArray] description]);
    
    
    NSMutableDictionary *dic =[matchedDicts objectAtIndex:0];
    
    if ([dic objectForKey:@"image"]!=nil && [[dic objectForKey:@"isPopUpShowed"] isEqualToString:@"NO"] )
    {
        self.forwardButton.tag=(int)[sender tag];
        [self showPopUpWithImagePath:[dic objectForKey:@"image"]];
        [dic setObject:@"YES" forKey:@"isPopUpShowed"];

    }
    
    else
    {
    if(![[singletonClass sharedInstance] isInternetConnected])
    {
        [self netCoonectivityAlert];
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isAll"];
    [[NSUserDefaults standardUserDefaults]synchronize ];
    
    restaurantViewController *restaurantView=[[restaurantViewController alloc] initWithNibName:@"restaurantViewController" bundle:nil];
    restaurantView.categryId=(int)[sender tag];
    [self.navigationController pushViewController:restaurantView animated:YES];
    }
}

- (IBAction)settingsBtnClckd:(id)sender
{
    if(![[singletonClass sharedInstance] isInternetConnected])
    {
        [self netCoonectivityAlert];
        return;
    }
    [super displaySettingsView];
    
    
}

- (IBAction)notificationsBtnClckd:(id)sender
{
    if(![[singletonClass sharedInstance] isInternetConnected])
    {
        [self netCoonectivityAlert];
        return;
    }
    [super displayNotificationView];
}

- (IBAction)contactsBtnclckd:(id)sender
{
    if(![[singletonClass sharedInstance] isInternetConnected])
    {
        [self netCoonectivityAlert];
        return;
    }
    clientContactViewController *clientContctview;
    if (IS_IPHONE_5)
    {
       clientContctview =[[clientContactViewController alloc] initWithNibName:@"clientContactViewController" bundle:nil];
        
    }
    else
    {
          clientContctview =[[clientContactViewController alloc] initWithNibName:@"clientContactViewControlleri4" bundle:nil];
    }
    
    [self.navigationController pushViewController:clientContctview animated:YES];
}

- (IBAction)searchButtonClicked:(id)sender
{
    [super displayDailySearchView];
}

- (IBAction)menuButtonClicked:(id)sender
{
    [super menuButtonTapped];
}

- (IBAction)entertainmentButtonClicked:(id)sender
{
    [[singletonClass sharedInstance] assignSelectedSection:EntertainmentSection];
    [[singletonClass sharedInstance] assignRootViewControllerWithAnimation:YES];
}

- (IBAction)diningButtonTapped:(id)sender
{
    [[singletonClass sharedInstance] assignSelectedSection:DiningSection];
    [[singletonClass sharedInstance] assignRootViewControllerWithAnimation:YES];
    
}


@end
