//
//  dealDetailViewController.h
//  Dash
//
//  Created by CS_Mac4 on 15/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface dealDetailViewController : UIViewController
{
    NSMutableDictionary *dailyDealDictionary;
    int imgsCount;
    AsyncImageView *_advertseImgView;
    NSTimer *advertsmntTimer;
    int randomValue;

}
@property (strong, nonatomic) IBOutlet UIScrollView *dealsScrollvw;
@property (strong, nonatomic) IBOutlet UILabel *dayLabel;
@property (strong, nonatomic) IBOutlet UILabel *descrptnLabel;
@property (strong, nonatomic) IBOutlet UILabel *itemLabel;
@property (strong, nonatomic)NSMutableDictionary *advertisementImages;
- (IBAction)bckButtonClckd:(id)sender;
@property int restaurantId;
@property (strong, nonatomic) IBOutlet UIButton *bckBtn;
@property (strong, nonatomic) NSString *paidStatus;
@end
