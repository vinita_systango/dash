//
//  UIApplication+Dash.m
//  Dash
//
//  Copyright (c) 2014 Systango. All rights reserved.
//

#import "UIApplication+Dash.h"

@implementation UIApplication (Dash)

- (void)clearBadgeNumbers
{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)updateBadgeNumbers:(NSInteger)badgeNumber
{
//    [self clearBadgeNumbers];
//    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:-1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeNumber];
}


- (BOOL)isApplicationActive
{
    return ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive);
}

@end
