//
//  DailyDealSearchResult.h
//  Dash
//
//  Created by stplmacmini8 on 6/9/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DailyDealSearchResult : NSObject

@property (nonatomic, assign) NSInteger deal_Id;
@property (nonatomic, strong) NSString * deal_desc;
@property (nonatomic, strong) NSString * restaurantName;
@property (nonatomic, assign) NSInteger restaurantId;
@property (nonatomic, strong)NSString *restaurantPaidStatus;
- (instancetype) initWithSearchResults:(NSDictionary *) searchResults;

@end
