//
//  DialyDealSearchViewController.m
//  Dash
//
//  Created by stplmacmini8 on 6/5/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import "DailyDealSearchViewController.h"
#import "customTableCell.h"
#import "DailyDealsSearchRequest.h"
#import "DailyDealSearchResult.h"
#import "restaurantDetailViewController.h"
#import "DailyDealSearchResultsCell.h"
#import "settingsViewController.h"
#import "clientContactViewController.h"
#import "dealDetailViewController.h"
static const CGFloat kAdvertisementImageViewHeight = 68.0;

@interface DailyDealSearchViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

{
    int randomValue;
    AsyncImageView * advertiseImgView;
}

@property (nonatomic, strong) UITableView * selectDaysTableView;
@property (nonatomic, weak) IBOutlet UITableView * daySelectTableView;
@property (nonatomic, weak) IBOutlet UITableView * searchResultsTableView;
@property (nonatomic, weak) IBOutlet UIButton * selectDayButton;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView * activityIndicator;
@property (nonatomic, assign) NSInteger searchDay;
@property (nonatomic, strong) NSMutableArray * searchResults;
@property (nonatomic, strong) NSArray * daysArray;
@property (nonatomic, strong) NSArray * viewControllers;
@property (nonatomic, weak) IBOutlet UIButton * searchButton;
@property (nonatomic, weak) IBOutlet UIButton * notificationsButton;
@property (nonatomic, weak) IBOutlet UIButton * contactButton;
@property (nonatomic, weak) IBOutlet UIButton * settingsButton;
@property (nonatomic, strong) NSMutableDictionary *advertisementImages;
@property (nonatomic, weak) NSTimer * advertsmntTimer;
@property (nonatomic, assign) int imgsCount;
@property (nonatomic, weak) IBOutlet UITextField * searchTextField;


@end

static NSString * kDaySelectCellIdentifier = @"DaySelectCell";
static NSString * kSearchResultsCellIdentifier = @"DailySearchResultCell";


@implementation DailyDealSearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupNavigationBar];
    [self setupButtonImages];
    advertiseImgView=[[BannerView sharedBannerInstance] getAdvertisementWithRect:CGRectMake(0, 0, self.view.frame.size.width, kAdvertisementImageViewHeight) OnView:self];
    [self.view addSubview:advertiseImgView];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:NO];
    self.viewControllers = [self.navigationController viewControllers];
    [self setupInstanceVariables];
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(callService)
                                        object:nil];
    [queue addOperation:operation];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"cityId"]) {
        [self getDailySearchResultsForDay:[self getCurrentDay]];

    }
    else{
        [self displayNoCitySelected];
    }
}
-(void)callService
{
    [self performSelectorOnMainThread:@selector(startAdvertisementTimer) withObject:nil waitUntilDone:NO];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self.advertsmntTimer invalidate];
    [super viewWillDisappear:animated];
}

- (void)setupInstanceVariables
{
    self.daysArray = @[@"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday", @"Sunday"];
    
    if(!self.searchResults){
        self.searchResults = [NSMutableArray array];
    }
    self.imgsCount = 0;
    [self.selectDayButton setTitle:self.daysArray[[self getCurrentDay] - 1] forState:UIControlStateNormal];
    self.searchDay = [self getCurrentDay];
    self.searchTextField.text = @"";
}

#pragma mark advertisement banner related methods


-(void)startAdvertisementTimer
{
    if ([[[BannerView sharedBannerInstance]highCostAdvertisementImages][@"response"][0][@"id"]intValue]!=-1)
    {
        self.advertsmntTimer= [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(animateAdvertisements) userInfo:nil repeats:YES];
        
    }
}

-(void)animateAdvertisements
{
    [[BannerView sharedBannerInstance]animateAdvertisements:advertiseImgView ofType:YES];
    
}


- (void)popViewControllerWithAnimation
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)setupButtonImages
{
    [[self.searchButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.settingsButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.contactButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.notificationsButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
}

- (NSInteger)getCurrentDay
{
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    if (comp.weekday == 1) {
        return 7; // if sunday, return 7.
    }
    else
    {
    return comp.weekday - 1 ; // 1 = Monday, 2 = Tue, etc.
    }
}

#pragma mark IBAction methods
- (IBAction)selectDayBtnClicked:(id)sender
{
    self.daySelectTableView.hidden = !self.daySelectTableView.hidden;
}

- (IBAction)notificationButtonClicked:(id)sender
{
    NotificationViewController *notfctnView;
    if (IS_IPHONE_5)
    {
        notfctnView=[[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
    }
    else
    {
        notfctnView=[[NotificationViewController alloc] initWithNibName:@"NotificationViewControlleri4" bundle:nil];
        
    }
    
    for (UIViewController *vc in self.viewControllers) {
        if ([vc isKindOfClass:[NotificationViewController class]])
        {
            
            [self.navigationController popToViewController:vc animated:YES];
            return;
        }
    }
    [self.navigationController pushViewController:notfctnView animated:YES];
}

- (IBAction)entertainmentButtonClicked:(id)sender
{
    [super displayEntertainmentHomeView];
}


- (IBAction)settingsButtonClicked:(id)sender
{
    
    settingsViewController *settingsView;
    if (IS_IPHONE_5)
    {
        settingsView=[[settingsViewController alloc] initWithNibName:@"settingsViewController" bundle:nil];
    }
    else {
        settingsView=[[settingsViewController alloc] initWithNibName:@"settingsViewControlleri4" bundle:nil];
    }
    for (UIViewController *vc in self.viewControllers) {
        if ([vc isKindOfClass:[settingsViewController class]])
        {
            
            [self.navigationController popToViewController:vc animated:YES];
            return;
            
        }
    }
    [self.navigationController pushViewController:settingsView animated:YES];
}

- (IBAction)contactButtonClicked:(id)sender
{
    clientContactViewController *clientContctview;
    if (IS_IPHONE_5)
    {
        clientContctview =[[clientContactViewController alloc] initWithNibName:@"clientContactViewController" bundle:nil];
    }
    else
    {
        clientContctview =[[clientContactViewController alloc] initWithNibName:@"clientContactViewControlleri4" bundle:nil];
    }
    for (UIViewController *vc in self.viewControllers) {
        if ([vc isKindOfClass:[clientContactViewController class]])
        {
            [self.navigationController popToViewController:vc animated:YES];
            return;
        }
    }
    [self.navigationController pushViewController:clientContctview animated:YES];
}

- (IBAction)diningButtonClicked:(id)sender
{
    [super displayDiningHomeView];
}

- (IBAction)menuButtonClicked:(id)sender
{
    [super menuButtonTapped];
}


#pragma mark TableViewDataSource methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == self.daySelectTableView) {
        
        return [self cellForDaysTableView:self.daySelectTableView forIndexPath:indexPath];
    }
    
    else if (tableView == self.searchResultsTableView){
        
        return [self cellForSearchResultsTableView:tableView forIndexPath:indexPath];
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(DailyDealSearchResultsCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchResultsTableView) {
        DailyDealSearchResult * searchResult = self.searchResults[indexPath.row];
        cell.dealDescription.text = searchResult.deal_desc;
        cell.restaurantName.text = searchResult.restaurantName;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.daySelectTableView) {
        return [self.daysArray count];
    }
    else if (tableView == self.searchResultsTableView){
        return [self.searchResults count];
    }
    return 0;
}

#pragma mark tableView Cell setup methods

- (UITableViewCell *) cellForDaysTableView :(UITableView *) daySelectTableView forIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [daySelectTableView dequeueReusableCellWithIdentifier:kDaySelectCellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kDaySelectCellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = self.daysArray[indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:17];
    return cell;
    
}

- (UITableViewCell *)cellForSearchResultsTableView:(UITableView *) tableView forIndexPath:(NSIndexPath *)indexPath
{
    DailyDealSearchResultsCell *cell =(DailyDealSearchResultsCell *) [tableView dequeueReusableCellWithIdentifier:kSearchResultsCellIdentifier];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"DailyDealSearchResultsCell" bundle:nil] forCellReuseIdentifier:kSearchResultsCellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:kSearchResultsCellIdentifier];
    }
    return cell;
    
}

#pragma mark tableView delegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.daySelectTableView) {
        [self.selectDayButton setTitle:self.daysArray[indexPath.row] forState:UIControlStateNormal];
        self.daySelectTableView.hidden = YES;
        
        //Monday corresponds to 1 ... so add 1 to the selected indexpath.row.
        self.searchDay = indexPath.row + 1;
        [self getDailySearchResultsOnSelectingDay];
    }
    else{
        NSLog(@"self.searchResults=%@ ",self.searchResults);
        DailyDealSearchResult * selectedSearchResult = self.searchResults[indexPath.row];
        [self showRestarauntDetailWithId:selectedSearchResult.restaurantId andPaidStatus:selectedSearchResult.restaurantPaidStatus];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchResultsTableView) {
        
        return 95;
    }
    return 45;
}

- (void)reloadTableViewDetails
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.searchResultsTableView reloadData];
    });
}

//TODO: Change the name of the method.
- (void)showRestarauntDetailWithId:(NSInteger)restaurantId andPaidStatus:(NSString*)paidStatus
{
    
    dealDetailViewController *dealDetail=[[dealDetailViewController alloc] initWithNibName:@"dealDetailViewController" bundle:nil];
    dealDetail.restaurantId = (int)restaurantId;
    dealDetail.paidStatus=paidStatus;
    [self.navigationController pushViewController:dealDetail animated:YES];

}

- (void)setupNavigationBar
{
    self.navigationItem.title = @"SEARCH DEALS";
    [self createBackBarButton];
    [self createHomeBarButton];
}

- (void)createBackBarButton
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 20.0f, 25.0f)];
    [backButton setImage:[UIImage imageNamed:@"back_black_btn"]forState: UIControlStateNormal];
    [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backButtonItem;
}

- (void)createHomeBarButton{
    
    UIButton *homeButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [homeButton setBackgroundImage:[UIImage imageNamed:@"home_icon_black"] forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(popToHome) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *homeBarBtn=[[UIBarButtonItem alloc] initWithCustomView:homeButton];
    [self.navigationItem setRightBarButtonItem:homeBarBtn];
}

- (void)popToHome
{
    [self .navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark Text feild delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self performSearchWith:textField.text];
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (([textField.text length] == 1) && [string isEqualToString:@""] ) {
        [self.searchResults removeAllObjects];
        [self getDailySearchResultsForDay:self.searchDay];
    }
    return YES;
}

- (void)performSearchWith:(NSString *)searchText
{
    if (self.searchDay) {
        [self getDailySearchResultsWithText:searchText];
    }
    else
    {
        [self displayNoDaySelected];
    }
}

#pragma mark Network calls

- (void)getDailySearchResultsWithText:(NSString *)searchString
{
    [self detailsInProgress:YES];
    DailyDealsSearchRequest * dailyDealSearch = [[DailyDealsSearchRequest alloc] initWithSearchText:searchString searchDay:self.searchDay cityId:[[NSUserDefaults standardUserDefaults] objectForKey:@"cityId"]];
    [self getDailySearchResultsWithRequest:dailyDealSearch];
    
}

- (void)getDailySearchResultsForDay:(NSInteger)currentDay
{
    [self detailsInProgress:YES];
    DailyDealsSearchRequest * dailyDealSearch = [[DailyDealsSearchRequest alloc] initWithSearchText:@"" searchDay:currentDay cityId:[[NSUserDefaults standardUserDefaults] objectForKey:@"cityId"]];
    [self getDailySearchResultsWithRequest:dailyDealSearch];
}

- (void)getDailySearchResultsOnSelectingDay
{
    if ([self.searchTextField.text isEqualToString:@""]) {
        [self getDailySearchResultsForDay:self.searchDay];
    }
    else{
        [self getDailySearchResultsWithText:self.searchTextField.text];
    }
}



- (void)getDailySearchResultsWithRequest:(DailyDealsSearchRequest *)searchRequest
{
    
    [[singletonClass sharedInstance] searchDailyDealsWithRequest:searchRequest withCompletionBlock:^(NSData *data, NSError *connectionError) {
        if (data != nil) {
            NSError *localError = nil;
            NSDictionary *returnDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
            [self getSearchResultsFromResponseJSON:returnDictionary];
            [self detailsInProgress:NO];
            [self reloadTableViewDetails];
        }
    }];
}

- (void)getSearchResultsFromResponseJSON:(NSDictionary *) respone
{
    self.searchResults = [NSMutableArray array];
    if ([respone hasValueForKey:@"response"] && [respone[@"response"] count] > 0 ) {
        
        for (id result in respone[@"response"]) {
            DailyDealSearchResult * dealSearchResult = [[DailyDealSearchResult alloc] initWithSearchResults:result];
            [self.searchResults addObject:dealSearchResult];
        }
    }
    else{
        [self displayNoResultsFound];
    }
}

- (void)detailsInProgress:(BOOL)state
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (state) {
            [self.activityIndicator startAnimating];
        } else {
            [self.activityIndicator stopAnimating];
            
        }
    });
}

# pragma mark UIAlertView methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    for (UIViewController*vc in [self.navigationController viewControllers]) {
        if ([vc isKindOfClass: [settingsViewController class]]){
            [[self navigationController] popToViewController:vc animated:YES];
        }
    }}

- (void)displayNoCitySelected
{
    UIAlertView * noResultsFoundAlert = [[UIAlertView alloc] initWithTitle:@"No Results Found" message:@"Please select a city" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [noResultsFoundAlert show];
    });
}

- (void)displayNoResultsFound
{
    UIAlertView * noResultsFoundAlert = [[UIAlertView alloc] initWithTitle:@"No Results Found" message:@"Please modify your search" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [noResultsFoundAlert show];
    });
}

- (void)displayNoDaySelected
{
    UIAlertView * noDaySelectedAlert = [[UIAlertView alloc] initWithTitle:@"Day Not Selected" message:@"Please select a day to seach the deals." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [noDaySelectedAlert show];
    });
}





@end
