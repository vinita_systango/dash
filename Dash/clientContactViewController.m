//
//  clientContactViewController.m
//  Dash
//
//  Created by CS_Mac4 on 14/10/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import "clientContactViewController.h"
#import "settingsViewController.h"
#import "DailyDealSearchViewController.h"


@interface clientContactViewController ()

@property (nonatomic, weak) IBOutlet UIButton * searchButton;
@property (nonatomic, weak) IBOutlet UIButton * notificationsButton;
@property (nonatomic, weak) IBOutlet UIButton * contactButton;
@property (nonatomic, weak) IBOutlet UIButton * settingsButton;


@end

@implementation clientContactViewController


-(void)popToHome
{
    [self .navigationController popToRootViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    UIButton *homeButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    
    [homeButton setBackgroundImage:[UIImage imageNamed:@"home_icon_black"] forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(popToHome) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *homeBarBtn=[[UIBarButtonItem alloc] initWithCustomView:homeButton];
    
    
    [self.navigationItem setRightBarButtonItem:homeBarBtn];
    
    socialView=[[socialViewController alloc] initWithNibName:@"socialViewController" bundle:nil];
    
    self.title=@"CONTACT DETAILS";
     [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:_bckBtn]];
    [self setupButtonImages];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    viewControllers = [self.navigationController viewControllers];
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)callButtonClickd:(id)sender
{
    NSString *phNo = [_callButton currentTitle];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl])
    {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
       UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [calert show];
    }
}
- (IBAction)emailButtonClicked:(id)sender
{
    if ([MFMailComposeViewController canSendMail]) {
        
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        
        [mailViewController setToRecipients:[NSArray arrayWithObject:[_emailButton currentTitle ]]];
        
        [mailViewController setSubject:@"Subject Goes Here."];
        [mailViewController setMessageBody:@"Your message goes here." isHTML:NO];
        
        
        [self presentViewController:mailViewController animated:YES completion:nil];
        
     
        
    }
    
    else {
        
        NSLog(@"Device is unable to send email in its current state.");
        
    }

}

- (void)setupButtonImages
{
    [[self.searchButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.settingsButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.contactButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.notificationsButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    
}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    [self dismissViewControllerAnimated:YES
                             completion:nil];
    
}

#pragma mark IBAction methods

- (IBAction)websteLinkBtnClkd:(id)sender
{
    if (![[UIApplication sharedApplication] openURL:[NSURL URLWithString:[_websteLinkBtn currentTitle]]])
{
    NSLog(@"Failed to open url:");
} 

}
- (IBAction)fbBtnClckd:(id)sender
{
    socialView.socialUrlString=@"https://www.facebook.com/dashappca";
    [self.navigationController pushViewController:socialView animated:YES];
    

}

- (IBAction)twtrBtnClckd:(id)sender
{
    socialView.socialUrlString=@"https://www.twitter.com/dashappca";
    [self.navigationController pushViewController:socialView animated:YES];
    
}


- (IBAction)bckBtnClckd:(id)sender
{
    //[self.navigationController popViewControllerAnimated:YES];
    
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isRestaurntListView"])
//    {
//        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isRestaurntListView"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.navigationController popViewControllerAnimated:YES];
//    }
//    else
//        [self.navigationController popToRootViewControllerAnimated:YES];
    
 
}

- (IBAction)settingsBtnClckd:(id)sender
{
    settingsViewController *settingsView;
    if (IS_IPHONE_5)
    {
        settingsView=[[settingsViewController alloc] initWithNibName:@"settingsViewController" bundle:nil];
    }
    else {
        settingsView=[[settingsViewController alloc] initWithNibName:@"settingsViewControlleri4" bundle:nil];
    }
    
    
    for (UIViewController *vc in viewControllers) {
        if ([vc isKindOfClass:[settingsViewController class]])
        {
            
            [self.navigationController popToViewController:vc animated:YES];
            return;
            
        }
        
    }
 
    [self.navigationController pushViewController:settingsView animated:YES];    //[self presentViewController:notfctnView animated:YES completion:nil];

}

- (IBAction)notifctnBtnClckd:(id)sender {
  
    NotificationViewController *notfctnView;
    if (IS_IPHONE_5)
    {
        notfctnView=[[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
    }
    else
    {
        notfctnView=[[NotificationViewController alloc] initWithNibName:@"NotificationViewControlleri4" bundle:nil];
        
    }
    
    for (UIViewController *vc in viewControllers) {
        if ([vc isKindOfClass:[NotificationViewController class]])
        {
            
            [self.navigationController popToViewController:vc animated:YES];
            return;
            
        }
        
    }

    [self.navigationController pushViewController:notfctnView animated:YES];
    

    //[self presentViewController:settingsView animated:YES completion:nil];

}

- (IBAction)searchButtonClicked:(id)sender
{
    
    DailyDealSearchViewController * dailiesSearchView = [[DailyDealSearchViewController alloc] initWithNibName:@"DailyDealsSearchView" bundle:nil];
    
    for (UIViewController *vc in viewControllers) {
        if ([vc isKindOfClass:[DailyDealSearchViewController class]])
        {
            [self.navigationController popToViewController:vc animated:YES];
            return;
        }
    }
    [self.navigationController pushViewController:dailiesSearchView animated:YES];
}

- (IBAction)entertainmentButtonClicked:(id)sender
{
    [super displayEntertainmentHomeView];
}

- (IBAction)diningButtonClicked:(id)sender
{
    [super displayDiningHomeView];
}

- (IBAction)menuButtonClicked:(id)sender
{
    [super menuButtonTapped];
}


@end
