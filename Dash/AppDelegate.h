//
//  AppDelegate.h
//  Dash
//
//  Created by CS_Mac4 on 12/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ApplicationDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) NSMutableArray *dinningPopUpArray;
@property (strong, nonatomic) NSMutableArray *entertainmentPopUpArray;

@property (strong, nonatomic) UIWindow *window;
@end
