//
//  Header.h
//  Dash
//
//  Created by CS_Mac4 on 12/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#ifndef Dash_Header_h
#define Dash_Header_h


#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define serviceStoreHours [Constant baseURLWithPath:@"ws/return_score_hours.php?rescode=%@"]

#define serviceBlockRestaurant [Constant baseURLWithPath:@"ws/insert_block_restaurant_new.php?res_usr_id=%@&res_id=%@"]

#define serviceGetCities [Constant baseURLWithPath:@"ws/return_all_city_new.php"]

#define serviceGetRestaurantsByCat [Constant baseURLWithPath:@"ws/cat_byrestaurant_new.php?cat_id=%@&user_id=%@&city_id=%@"]

#define serviceGetAllCategoryPopUpImages [Constant baseURLWithPath:@"ws/getCategoryImage.php?city_id=%@&selected_section=%@"] //Priyanka: needs to change it 

#define serviceGetAllRestaurants [Constant baseURLWithPath:@"ws/return_allrestaurant_new.php?user_id=%@&city_id=%@&section_id=%@"]

#define serviceGetSingleRestaurant [Constant baseURLWithPath:@"ws/return_singlerestaurant.php?res_id=%@&user_id=%@&banner_id=%@&section_id=%@"]

#define serviceGetSingleRestaurantWithoutBannerId [Constant baseURLWithPath:@"ws/return_singlerestaurant.php?res_id=%@&user_id=%@&section_id=%@"]


#define serviceGetAllAdvrtsmnt [Constant baseURLWithPath:@"ws/ret_all_advertisement_new.php?status=%@&city_id=%@&section_id=%@"]

#define serviceGetCouponDetail [Constant baseURLWithPath:@"ws/res_coupon_detail.php?reg_id=%@"]

#define serviceGetDealDetail [Constant baseURLWithPath:@"ws/res_dealdetail.php?reg_id=%@"]

#define serviceGetSpeclty [Constant baseURLWithPath:@"ws/res_speciality_details.php?reg_id=%@"]

#define serviceGetMenuList [Constant baseURLWithPath:@"ws/res_menu.php?reg_id=%@"]

#define serviceRegisterUser [Constant baseURLWithPath:@"ws/usr_resgister_new.php"]

#define serviceGetAllNotifications [Constant baseURLWithPath:@"ws/return_all_noti_details_new.php?id=%@&city_id=%@&section_id=%@"]

#define serviceUpdateSettings [Constant baseURLWithPath:@"ws/update_status_user.php?user_selected_city_id=%@&user_id=%@"]

#define serviceSearchDailyDeal [Constant baseURLWithPath:@"ws/res_deal_searchresult.php?day_id=%@&search_text=%@&city_id=%@&section_id=%@"]

#define serviceGetAllBadgesCount [Constant baseURLWithPath:@"ws/getBadges.php?user_id=%@&city_id=%@"]

#define serviceGetStatusForCategories [Constant baseURLWithPath:@"ws/return_category_status.php?user_id=%@&section_id=%@"]

#define serviceUpdateStatusForCategories [Constant baseURLWithPath:@"ws/update_category_status.php"]

#define serviceUpdateAllCategoryStatus [Constant baseURLWithPath:@"ws/update_allcategory_status.php?user_id=%@&section_id=%@&status=%@"]



#endif
