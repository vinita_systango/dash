//
//  settingsViewController.m
//  Dash
//
//  Created by CS_Mac4 on 10/10/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import "settingsViewController.h"
#import "clientContactViewController.h"
#import "DailyDealSearchViewController.h"
@interface settingsViewController ()

@property (nonatomic, weak) IBOutlet UIButton * searchButton;
@property (nonatomic, weak) IBOutlet UIButton * notificationsButton;
@property (nonatomic, weak) IBOutlet UIButton * contactButton;
@property (nonatomic, weak) IBOutlet UIButton * settingsButton;


@end

@implementation settingsViewController

-(void)viewWillAppear:(BOOL)animated
{
   
       viewControllers = [self.navigationController viewControllers];
    
    
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(callService)
                                        object:nil];
    [queue addOperation:operation];
     [super viewWillAppear:animated];
    
}

- (void)viewDidLayoutSubviews
{
    _settingsScrollview.contentSize=CGSizeMake(320, 630);
}
-(void)callService
{
    citiesArray=[[NSMutableArray alloc] initWithArray:[[singletonClass sharedInstance] getCities]];
       [self performSelectorOnMainThread:@selector(getServerResponse) withObject:nil waitUntilDone:NO];
    
    NSLog(@"%@",citiesArray);

}

-(void)getServerResponse
{
   
     NSLog(@"%@",citiesArray);
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"cityId"]);
    
    
    [citiesTableView reloadData];
}
-(BOOL)getEntertainmentDisabledStatus
{
    return [[singletonClass sharedInstance] getIsEntertainmentDisabled];
}
-(void)popToHome
{
    
    [self .navigationController popToRootViewControllerAnimated:YES];
   
}


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"cityId"]==nil )
    {
        [_citiesBtnProp setTitle:@"     Select city" forState:UIControlStateNormal];
        
    }
    else
    {
        
        [_citiesBtnProp setTitle:[NSString stringWithFormat:@"     %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"cityName"]]  forState:UIControlStateNormal];
        
    }

    
    citiesTableView=[[UITableView alloc] initWithFrame:CGRectMake(0, 135, 320, 200)];
    citiesTableView.hidden=YES;
    citiesTableView.delegate=self;
    citiesTableView.dataSource=self;
    
    [self.view addSubview:citiesTableView];
    
        UIButton *homeButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        
        [homeButton setBackgroundImage:[UIImage imageNamed:@"home_icon_black"] forState:UIControlStateNormal];
        [homeButton addTarget:self action:@selector(popToHome) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *homeBarBtn=[[UIBarButtonItem alloc] initWithCustomView:homeButton];
        
        [self.navigationItem setRightBarButtonItem:homeBarBtn];
        
        [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:_bckBtn]];
    

    self.title=@"SELECT CITY";

    UIImage *clearImage = [[UIImage imageNamed:@"lines.png"] stretchableImageWithLeftCapWidth:14.0 topCapHeight:0.0];
    [_SLIDER setMinimumTrackImage:clearImage forState:UIControlStateNormal];
    [_SLIDER setMaximumTrackImage:clearImage forState:UIControlStateNormal];
    [_SLIDER setThumbImage:[UIImage imageNamed:@"circle.png"] forState:UIControlStateNormal];
    
    

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isFirst"]==YES)
    {
        _brkfstSwtchBtn.on=
        [[NSUserDefaults standardUserDefaults] boolForKey:@"breakfast"];
        
        
        _LnchSwtchBtn.on=
        [[NSUserDefaults standardUserDefaults] boolForKey:@"lunch"];
        
        _supperSwtchBtn.on=
        [[NSUserDefaults standardUserDefaults] boolForKey:@"supper"];
        
        _nightLfeSwtchBtn.on=
        [[NSUserDefaults standardUserDefaults] boolForKey:@"dinner"];
        
        _allOnSwtchBtn.on=
        [[NSUserDefaults standardUserDefaults] boolForKey:@"all"];
        
        
        _SLIDER.value =
        [[NSUserDefaults standardUserDefaults]integerForKey:@"sliderValue"];

    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"breakfast"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"lunch"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"supper"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"dinner"];
        [[NSUserDefaults standardUserDefaults]setInteger:2.95 forKey:@"sliderValue"];
        [[NSUserDefaults standardUserDefaults] synchronize];

    }
    [self setupButtonImages];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupButtonImages
{
    [[self.searchButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.settingsButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.contactButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [[self.notificationsButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)bckButtonClckd:(id)sender
{
    if ([self getEntertainmentDisabledStatus]==YES)
    {
        [[singletonClass sharedInstance] assignSelectedSection:DiningSection];
        [[singletonClass sharedInstance] assignRootViewControllerWithAnimation:NO];
    }
     @try
    {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirst"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isRestaurntListView"])
        {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isRestaurntListView"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
    @catch (NSException *exception)
    {
        UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:@"Sorry!" message:[exception reason] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alrt show];
        
    }
    
    @finally
    {
        
    }

   }

-(void)runService
{
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"] );
    
    mydictnry= [[singletonClass sharedInstance] updateSettings:[[NSUserDefaults standardUserDefaults] objectForKey:@"cityId"]:[[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]                                                  ];
    
    [[RemoteNotificationManager sharedInstance] parseBadgesResponse:mydictnry];
    
    NSDictionary *response = [mydictnry valueForKey:@"response"];
    [[singletonClass sharedInstance] assignEntertainmentDisabled:![[response getValueForKey:@"isEntertainmentEnabled"] boolValue]];
    [[NSUserDefaults standardUserDefaults] setBool:![[response getValueForKey:@"isEntertainmentEnabled"] boolValue] forKey:@"isEntertainmentEnabled"];
    [self performSelectorOnMainThread:@selector(getNewdata) withObject:nil waitUntilDone:YES];

}

-(void)getNewdata
{
    NSLog(@"%@",mydictnry[@"response"][@"message"]);
    
    for (NSMutableDictionary *dic in [ApplicationDelegate dinningPopUpArray]){
       [dic setValue:@"NO" forKey:@"isPopUpShowed"];
    }
    for (NSMutableDictionary *dic in [ApplicationDelegate entertainmentPopUpArray]){
        [dic setValue:@"NO" forKey:@"isPopUpShowed"];
    }
    
    UIAlertView *alrt=[[UIAlertView alloc] initWithTitle:@"" message:mydictnry[@"response"][@"message"] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alrt show];

}
- (IBAction)sliderVlueChanged:(id)sender
{
   
//    int value = _SLIDER.value;
//    
//    _SLIDER.value = value;
  //  [self performSelector:@selector(changeSliderValue) withObject:nil afterDelay:0.9];
    
    
 }
    
    //1.92
    //2.99
    //3.97
    
    
      //NSLog(@"%d",(int)_SLIDER.value );


- (IBAction)brkfastBtnSwtchd:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:[_brkfstSwtchBtn isOn] forKey:@"breakfast"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self checkSwtchBtnValue];
    
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(runService)
                                        object:nil];
    [queue addOperation:operation];
}

- (IBAction)lunchBtnSwtchd:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:[_LnchSwtchBtn isOn] forKey:@"lunch"];
    [[NSUserDefaults standardUserDefaults] synchronize];
     [self checkSwtchBtnValue];
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(runService)
                                        object:nil];
    [queue addOperation:operation];
}

- (IBAction)supperBtnSwtchd:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:[_supperSwtchBtn isOn] forKey:@"supper"];
    [[NSUserDefaults standardUserDefaults] synchronize];
     [self checkSwtchBtnValue];
    
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(runService)
                                        object:nil];
    [queue addOperation:operation];
}

- (IBAction)nightlfeBtnSwtchd:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:[_nightLfeSwtchBtn isOn] forKey:@"dinner"];
    [[NSUserDefaults standardUserDefaults] synchronize];
     [self checkSwtchBtnValue];
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(runService)
                                        object:nil];
    [queue addOperation:operation];
}

- (IBAction)allOnBtnSwthcd:(id)sender
{
 
    [[NSUserDefaults standardUserDefaults] setBool:[_allOnSwtchBtn isOn] forKey:@"all"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([_allOnSwtchBtn isOn])
    {
        [self setSwtchBtnValue:YES];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"breakfast"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"lunch"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"supper"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"dinner"];
    }
    else
    {
        [self setSwtchBtnValue:NO];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"breakfast"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"lunch"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"supper"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"dinner"];
    }
    
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(runService)
                                        object:nil];
    [queue addOperation:operation];
}
-(void)checkSwtchBtnValue
{
    if ([_brkfstSwtchBtn isOn] & [_LnchSwtchBtn isOn] & [_supperSwtchBtn isOn] & [_nightLfeSwtchBtn isOn])
    {
         _allOnSwtchBtn.on=YES;
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"all"];
        [[NSUserDefaults standardUserDefaults] synchronize];

    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"all"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        _allOnSwtchBtn.on=NO;

    }
}

-(void)setSwtchBtnValue:(BOOL )value
{
    _brkfstSwtchBtn.on=value;
    
    _LnchSwtchBtn.on=value;
    
    _supperSwtchBtn.on=value;
    
    _nightLfeSwtchBtn.on=value;
    
   
    
}

#pragma mark IBAction methods

- (IBAction)contanctBtnClckd:(id)sender
{
    clientContactViewController *clientContctview;
    if (IS_IPHONE_5)
    {
        clientContctview =[[clientContactViewController alloc] initWithNibName:@"clientContactViewController" bundle:nil];
        
    }
    else
    {
        clientContctview =[[clientContactViewController alloc] initWithNibName:@"clientContactViewControlleri4" bundle:nil];
    }
    
    for (UIViewController *vc in viewControllers) {
        if ([vc isKindOfClass:[clientContactViewController class]])
        {
            
            [self.navigationController popToViewController:vc animated:YES];
            return;
            
        }
        
    }
  [self.navigationController pushViewController:clientContctview animated:YES];
    
  
}

- (IBAction)notificationBtnClckd:(id)sender
{
    
   
    NotificationViewController *notfctnView;
    if (IS_IPHONE_5)
    {
        notfctnView=[[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
    }
    else
    {
        notfctnView=[[NotificationViewController alloc] initWithNibName:@"NotificationViewControlleri4" bundle:nil];
        
    }
    
    for (UIViewController *vc in viewControllers) {
        if ([vc isKindOfClass:[NotificationViewController class]])
        {
            [self.navigationController popToViewController:vc animated:YES];
            return;
        }
    
    }
    
    [self.navigationController pushViewController:notfctnView animated:YES];
    



}

- (IBAction)searchButtonClicked:(id)sender
{
    
    DailyDealSearchViewController * dailiesSearchView = [[DailyDealSearchViewController alloc] initWithNibName:@"DailyDealsSearchView" bundle:nil];
    
    for (UIViewController *vc in viewControllers) {
        if ([vc isKindOfClass:[DailyDealSearchViewController class]])
        {
            [self.navigationController popToViewController:vc animated:YES];
            return;
        }
        
    }

    [self.navigationController pushViewController:dailiesSearchView animated:YES];
    
    
}

- (IBAction)entertainmentButtonClicked:(id)sender
{
    [super displayEntertainmentHomeView];
}


- (IBAction)didEndEdtng:(id)sender {
}
- (IBAction)touchUpSlider:(id)sender
{
    int value = _SLIDER.value;
    
    _SLIDER.value = value;
    
    [[NSUserDefaults standardUserDefaults]setInteger:value forKey:@"sliderValue"];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (IBAction)selectCityBtnClckd:(id)sender
{
    
    if (citiesTableView.hidden==YES) {
        citiesTableView.hidden=NO;
    }
    else
    {
         citiesTableView.hidden=YES;
    }
}

- (IBAction)diningButtonClicked:(id)sender
{
    [super displayDiningHomeView];
}

- (IBAction)menuButtonClicked:(id)sender
{
    [super menuButtonTapped];
}


#define tableViewDelegates


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
     NSLog(@"%@",citiesArray);
    return [citiesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
     NSLog(@"%@",citiesArray);
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    cell.textLabel.text=citiesArray[indexPath.row][@"city_nam"];
    cell.textLabel.font= [UIFont fontWithName:@"ProximaNova-Regular" size:17];

    
  
    
    return cell;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_citiesBtnProp setTitle:[NSString stringWithFormat:@"     %@",citiesArray[indexPath.row][@"city_nam"]] forState:UIControlStateNormal];
    
    [[NSUserDefaults standardUserDefaults] setObject:citiesArray[indexPath.row][@"city_nam"] forKey:@"cityName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:citiesArray[indexPath.row][@"city_id"] forKey:@"cityId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
      citiesTableView.hidden=YES;
    
    NSOperationQueue *queue = [NSOperationQueue new];
    NSInvocationOperation *operation = [[NSInvocationOperation alloc]
                                        initWithTarget:self
                                        selector:@selector(runService)
                                        object:nil];
    [queue addOperation:operation];
    
}




@end
