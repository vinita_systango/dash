//
//  NotificationSettingsViewController.h
//  Dash
//
//  Created by NR on 10/09/2015.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface NotificationSettingsViewController : UIViewController

-(IBAction)dinningNotificationClicked:(id)sender;
-(IBAction)entertainmentNotificationClicked:(id)sender;

@end
