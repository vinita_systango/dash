//
//  clientContactViewController.h
//  Dash
//
//  Created by CS_Mac4 on 14/10/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "socialViewController.h"
#import "BaseDinningViewController.h"

@interface clientContactViewController : BaseDinningViewController<MFMailComposeViewControllerDelegate>
{
    
        NSArray *viewControllers;
    
    socialViewController *socialView;
}
- (IBAction)callButtonClickd:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *callButton;
- (IBAction)emailButtonClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *emailButton;
- (IBAction)websteLinkBtnClkd:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *websteLinkBtn;
- (IBAction)fbBtnClckd:(id)sender;
- (IBAction)twtrBtnClckd:(id)sender;
- (IBAction)bckBtnClckd:(id)sender;
- (IBAction)settingsBtnClckd:(id)sender;
- (IBAction)notifctnBtnClckd:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *bckBtn;

@end
