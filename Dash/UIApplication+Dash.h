//
//  UIApplication+Dash.h
//  Dash
//
//  Copyright (c) 2014 Systango. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (Dash)

- (void)clearBadgeNumbers;

- (void)updateBadgeNumbers:(NSInteger)badgeNumber;

- (BOOL)isApplicationActive;

@end
