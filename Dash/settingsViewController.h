//
//  settingsViewController.h
//  Dash
//
//  Created by CS_Mac4 on 10/10/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseDinningViewController.h"

@interface settingsViewController : BaseDinningViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *viewControllers;
    NSMutableDictionary *mydictnry;
    NSMutableArray *citiesArray;
    
    NSMutableDictionary *data;
    UITableView *citiesTableView;
}
- (IBAction)brkfastBtnSwtchd:(id)sender;

- (IBAction)lunchBtnSwtchd:(id)sender;
- (IBAction)supperBtnSwtchd:(id)sender;

- (IBAction)nightlfeBtnSwtchd:(id)sender;

- (IBAction)allOnBtnSwthcd:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *settingsScrollview;
@property (strong, nonatomic) IBOutlet UIButton *citiesBtnProp;

@property (strong, nonatomic) IBOutlet UISwitch *brkfstSwtchBtn;
@property (strong, nonatomic) IBOutlet UISwitch *LnchSwtchBtn;
@property (strong, nonatomic) IBOutlet UISwitch *supperSwtchBtn;
@property (strong, nonatomic) IBOutlet UISwitch *nightLfeSwtchBtn;
@property (strong, nonatomic) IBOutlet UISwitch *allOnSwtchBtn;
- (IBAction)contanctBtnClckd:(id)sender;
- (IBAction)notificationBtnClckd:(id)sender;
- (IBAction)didEndEdtng:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *bckBtn;
- (IBAction)sliderVlueChanged:(id)sender;
@property (strong, nonatomic) IBOutlet UISlider *SLIDER;
- (IBAction)touchUpSlider:(id)sender;
- (IBAction)selectCityBtnClckd:(id)sender;
- (IBAction)bckButtonClckd:(id)sender;
@end
