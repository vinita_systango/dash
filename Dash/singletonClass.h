//
//  singletonClass.h
//  Dash
//
//  Created by CS_Mac4 on 12/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"

typedef enum{
    DiningSection = 1,
    EntertainmentSection
} SelectedSection;


@class DailyDealsSearchRequest;

@interface singletonClass : NSObject

@property (assign, nonatomic) SelectedSection selectedSection;
@property (assign, nonatomic) BOOL isEntertainmentDisabled;
@property (strong, nonatomic) ViewController *diningCategoryViewController;
@property (strong, nonatomic) ViewController *entertainmentCategoryViewController;

+(id)sharedInstance;

- (NSString *)getSelectedSectionId;

- (BOOL)isDiningSectionSelected;

- (void)assignEntertainmentDisabled:(BOOL)disabled;

- (BOOL)getIsEntertainmentDisabled;

-(NSMutableArray*)getDinningPopupArray;

-(NSMutableArray*)getEntertainmentPopupArray;

- (void)assignSelectedSection:(SelectedSection)selectedSection;

- (NSArray *)getEntertainmentCategories;

- (NSArray *)getDinningCategories;

- (UINavigationController *)assignRootViewControllerWithAnimation:(BOOL)isAnimated;

- (ViewController *)getSelectedCategoryViewController;

- (UIViewController *)getTopControllerOnWindow;

-(BOOL)isInternetConnected;
-(NSMutableArray *)getRestaurantsByCat : (NSString *)CategoryId : (NSString *)userId : (NSString *)cityId;

-(NSMutableDictionary *)getRestaurantDetail : (NSString *)restaurntId : (NSString *)userId : (NSString *)bannerId : (NSString *)sectionId;
-(NSMutableDictionary *)getAllAdvrtsmnt : (NSString *)status :(NSString *)cityId;
-(NSMutableArray*)getAllCategoryPopUpImages:  (NSString *)sectionId : (NSString *)cityId;
-(NSMutableDictionary *)getCouponDetail : (NSString *)restaurntId;
-(NSMutableDictionary *)getSpecialtyDetail : (NSString *)restaurntId;
-(NSMutableDictionary *)getDealDetail : (NSString *)restaurntId;
-(NSMutableDictionary *)registerUser : (NSString *)tokenId;
-(NSMutableDictionary *)getMenuList : (NSString *)restaurntId;
-(NSMutableArray *)getAllRestaurant:(NSString *)userId :(NSString *)cityId;
-(NSMutableArray *)blockRestaurant : (NSString *)userId : (NSString *)resId;
-(NSMutableDictionary *)updateSettings :(NSString *)selctdCtyID : (NSString *)regId;
-(NSMutableDictionary *)getRestaurantHours : (NSString *)restaurntId;
-(NSMutableDictionary *)getAllNotifications : (NSString *)regId :(NSString *)selctdCtyID;
-(NSMutableDictionary *)getAllBadges : (NSString *)userId :(NSString *)selctdCtyID;
-(NSMutableArray *)getCities;
- (NSMutableArray *)getCategories;
-(NSMutableArray *)updateStatusForCategories : (NSString *)userId : (NSArray *)statusArray :(int)sectionId;
-(NSDictionary *)getStatusForCategories : (NSString *)userId : (NSString *)sectionID;
- (void)searchDailyDealsWithRequest:(DailyDealsSearchRequest *)searchRequest withCompletionBlock:(void (^)( NSData *data, NSError *connectionError ))returnBlock;

@end

