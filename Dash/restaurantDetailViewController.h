//
//  restaurantDetailViewController.h
//  Dash
//
//  Created by CS_Mac4 on 15/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "socialViewController.h"
#import <MessageUI/MessageUI.h>

@interface restaurantDetailViewController : UIViewController<MFMailComposeViewControllerDelegate>
{
    NSMutableDictionary *restaurantDictionary;
    int imgsCount;
    AsyncImageView *_advertseImgView;
    socialViewController *socialView;
    NSTimer *advertsmntTimer;
    NSMutableDictionary *storeHoursDictionary;
    UIActivityIndicatorView *spinner;
    
    int randomValue;
}


- (IBAction)clockBtnClckd:(id)sender;
- (IBAction)websiteBtnClckd:(id)sender;
- (IBAction)popToRoot:(id)sender;
- (IBAction)singleTap:(id)sender;
- (IBAction)menuButtonClicked:(id)sender;
- (IBAction)couponButtonClicke:(id)sender;
- (IBAction)specialtyBtnClckd:(id)sender;
- (IBAction)dailyButtonClicked:(id)sender;
- (IBAction)bckButtonClckd:(id)sender;
- (IBAction)emailBtnClckd:(id)sender;
- (IBAction)fbBtnClckd:(id)sender;
- (IBAction)callBtnClckd:(id)sender;
- (IBAction)twterBtnClckd:(id)sender;
- (IBAction)adressBtnClckd:(id)sender;
- (IBAction)bubbleBtnClckd:(id)sender;


@property (strong, nonatomic) IBOutlet UIView *storeHoursBubble;
@property (strong, nonatomic) IBOutlet UILabel *restaurantNameLabel;
@property int restaurantId;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UILabel *contactNO;
@property (strong, nonatomic) IBOutlet UILabel *emailLabel;
@property (strong, nonatomic) IBOutlet UIButton *emailBtn_Prop;
@property (strong, nonatomic) NSString *catNameString;
@property (strong, nonatomic) IBOutlet UIButton *clockBtn_prop;
@property (strong, nonatomic) NSMutableDictionary *advertisementImages;
@property (strong, nonatomic) IBOutlet UILabel *categryName;
@property (strong, nonatomic) IBOutlet UIScrollView *scrolView;
@property (strong, nonatomic) IBOutlet UIImageView *restaurantImageView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageCtrl;
@property (strong, nonatomic) IBOutlet UIButton *contctNumberBtn;
@property (strong, nonatomic) IBOutlet UIButton *WbsteLnkBtn;
@property (strong, nonatomic) IBOutlet UIButton *EmailBtn;
@property (strong, nonatomic) IBOutlet UIButton *bckBtn;
@property (strong, nonatomic) IBOutlet UIButton *instgrmBtn;
@property (strong, nonatomic) IBOutlet UIButton *pintrstBtn;
@property (strong, nonatomic) IBOutlet UIButton *TwtrBtn;
@property (strong, nonatomic) IBOutlet UIButton *addressBtn;
@property (strong, nonatomic) IBOutlet UIButton *WebsteBtn;
@property (strong, nonatomic) IBOutlet UIButton *fbButton;
@property (strong, nonatomic) IBOutlet UIButton *menuBtn;
@property (strong, nonatomic) IBOutlet UIButton *dailyBtn;
@property (strong, nonatomic) IBOutlet UIButton *specialsBtn;
@property (strong, nonatomic) IBOutlet UIButton *couponBtn;
@property (strong, nonatomic) IBOutlet UIImageView *bubbleImgVw;
@property (strong, nonatomic) NSString * clickedBannerId;

@end
