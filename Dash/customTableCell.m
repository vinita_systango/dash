//
//  customTableCell.m
//  Dash
//
//  Created by CS_Mac4 on 12/09/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import "customTableCell.h"


@implementation customTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    
    [self addControls];
    [self addControlsForNotification];
    return self;
}

-(void)addControls
{
    self.asyncImageView =[[AsyncImageView alloc] initWithFrame:CGRectMake(5, 5, 89.5, 89.5)];
    [self addSubview: self.asyncImageView];
    
    _restaurantNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(107, 15,159, 50)];
    _restaurantNameLabel.adjustsFontSizeToFitWidth = YES;
    _restaurantNameLabel.minimumScaleFactor = 10.0f/12.0f;
    _restaurantNameLabel.clipsToBounds = YES;
    _restaurantNameLabel.backgroundColor = [UIColor clearColor];
    _restaurantNameLabel.textColor = [UIColor blackColor];
    _restaurantNameLabel.textAlignment = NSTextAlignmentCenter;
    _restaurantNameLabel.numberOfLines = 2;
    
    [_restaurantNameLabel setFont: [UIFont fontWithName:@"Rockwell" size:21]];
    [self addSubview:_restaurantNameLabel];
    
    
    _addressLabel = [[UILabel alloc]initWithFrame:CGRectMake(107, 70,159, 17)];
    _addressLabel.adjustsFontSizeToFitWidth = YES;
    _addressLabel.minimumScaleFactor = 10.0f/12.0f;
    _addressLabel.clipsToBounds = YES;
    _addressLabel.backgroundColor = [UIColor clearColor];
    _addressLabel.textColor = [UIColor blackColor];
    _addressLabel.textAlignment = NSTextAlignmentCenter;
     [_addressLabel setFont: [UIFont fontWithName:@"ProximaNova-Regular" size:18]];
    [self addSubview:_addressLabel];
    
    
    _categryLabel = [[UILabel alloc]initWithFrame:CGRectMake(107, 92,159, 17)];
    _categryLabel.adjustsFontSizeToFitWidth = YES;
    _categryLabel.minimumScaleFactor = 10.0f/12.0f;
    _categryLabel.clipsToBounds = YES;
    _categryLabel.backgroundColor = [UIColor clearColor];
    _categryLabel.textColor = [UIColor colorWithRed:0.3882 green:0.6902 blue:0.8118 alpha:1];
    [_categryLabel setFont: [UIFont fontWithName:@"Vollkorn-Italic" size:16]];

    _categryLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_categryLabel];
    
    _notificationONOFF= [[UIButton alloc] initWithFrame: CGRectMake(265, 30, 50, 50)];
    [self addSubview:_notificationONOFF];
    _notificationONOFF.hidden=YES;
    
    
}

-(void)addControlsForNotification
{
    UIImageView *cellBgImageView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
    cellBgImageView.image=[UIImage imageNamed:@"placeholder.png"];
    [self addSubview:cellBgImageView];
    
    
    _labelRestaurantName = [[UILabel alloc]initWithFrame:CGRectMake(5, 15,220, 23)];
    _labelRestaurantName.adjustsFontSizeToFitWidth = YES;
    _labelRestaurantName.minimumScaleFactor = 10.0f/12.0f;
    _labelRestaurantName.clipsToBounds = YES;
    _labelRestaurantName.backgroundColor = [UIColor clearColor];
    _labelRestaurantName.textColor = [UIColor darkGrayColor];
    _labelRestaurantName.textAlignment = NSTextAlignmentLeft;
    [_labelRestaurantName setFont: [UIFont fontWithName:@"Rockwell" size:23]];
    [cellBgImageView addSubview:_labelRestaurantName];
    
    
   _availabilityNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 40,310, 90)];
    _availabilityNameLabel.adjustsFontSizeToFitWidth = YES;
    _availabilityNameLabel.minimumScaleFactor = 10.0f/12.0f;
    _availabilityNameLabel.clipsToBounds = YES;
    _availabilityNameLabel.numberOfLines=5;
    _availabilityNameLabel.backgroundColor = [UIColor clearColor];
    _availabilityNameLabel.textColor = [UIColor darkGrayColor];
    _availabilityNameLabel.textAlignment = NSTextAlignmentLeft;
    [_availabilityNameLabel setFont: [UIFont fontWithName:@"ProximaNova-Regular" size:15]];
    _availabilityNameLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [cellBgImageView addSubview:_availabilityNameLabel];

    _timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(245, 20,80, 17)];
    _timeLabel.adjustsFontSizeToFitWidth = YES;
    _timeLabel.minimumScaleFactor = 10.0f/12.0f;
    _timeLabel.clipsToBounds = YES;
    _timeLabel.backgroundColor = [UIColor clearColor];
    _timeLabel.textColor = [UIColor colorWithRed:0.3882 green:0.6902 blue:0.8118 alpha:1];
    [_timeLabel setFont: [UIFont fontWithName:@"Vollkorn-Italic" size:13]];
    
    _timeLabel.textAlignment = NSTextAlignmentLeft;
    [cellBgImageView addSubview:_timeLabel];
    
    
   

    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
