//
//  UpdateNotificationSettingViewController.h
//  Dash
//
//  Created by NR on 10/09/2015.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationSettingTableCell.h"

@interface UpdateNotificationSettingViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (assign, nonatomic) SelectedSection selectedNotificationSection;
@property (nonatomic,weak)IBOutlet UITableView *settingTable;
@end
