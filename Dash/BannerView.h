//
//  BannerView.h
//  Dash
//
//  Created by NR on 03/09/2015.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BannerViewDelegate <NSObject>
-(void)openAdvertisementLink;

@end

@interface BannerView : NSObject
@property (assign,nonatomic)BOOL isCurrentBannerHighCost;
@property (assign,nonatomic)int randomValue,nextHighCostImage,nextLowCostImage,currentAnimatedAdd;
@property(strong,nonatomic) NSMutableDictionary *highCostAdvertisementImages;
@property(strong,nonatomic) NSMutableDictionary *lowCostAdvertisementImages;
+(id)sharedBannerInstance;
-(AsyncImageView*)getAdvertisementWithRect:(CGRect)frame OnView:(id)delegate;
-(void)animateAdvertisements:(AsyncImageView*)advertiseMentView ofType:(BOOL)isHighCost;
@property (weak, nonatomic) id<BannerViewDelegate> delegate;
-(void)fetchBannerLowCostImagesFromServer;
-(void)fetchBannerHighCostImagesFromServer;
@end
