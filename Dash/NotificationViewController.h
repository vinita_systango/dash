//
//  NotificationViewController.h
//  Dash
//
//  Created by CS_Mac4 on 07/10/14.
//  Copyright (c) 2014 CS_Mac4. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseDinningViewController.h"


@interface NotificationViewController : BaseDinningViewController
{
    NSMutableDictionary *notificationsDictnry;
    NSArray *viewControllers;

}
@property (strong, nonatomic) IBOutlet UIButton *bckBtn;
- (IBAction)backBtnClckd:(id)sender;
- (IBAction)contanctBtnClckd:(id)sender;
- (IBAction)settingsBtnClckd:(id)sender;
-(void)getNotfcations;
@property (strong, nonatomic) IBOutlet UITableView *notfcatnsTableView;
@end
