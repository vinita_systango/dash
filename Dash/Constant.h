//
//  Constant.h
//  Dash
//
//  Created by stplmacmini3 on 03/07/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constant : NSObject

+(NSString*)baseURLWithPath:(NSString *)urlPath;

@end
