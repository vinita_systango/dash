//
//  NSDictionary+HasValueForKey.h
//  Dash
//
//  Created by stplmacmini8 on 6/9/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (HasValueForKey)

- (BOOL)hasValueForKey:(NSString *)key;

- (id)getValueForKey:(NSString *)key;

@end
