//
//  EntertainmentCategoryViewController.m
//  Dash
//
//  Created by stplmacmini8 on 6/16/15.
//  Copyright (c) 2015 CS_Mac4. All rights reserved.
//

#import "EntertainmentCategoryViewController.h"

@interface EntertainmentCategoryViewController ()

@property (nonatomic, weak) IBOutlet UIButton * notificationsButton;

@end

@implementation EntertainmentCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    [self setupButtonImages];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupNavigationBar
{
    self.navigationItem.title = @"Entertainment";
    [self createBackBarButton];
}

- (void)createBackBarButton
{
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 20.0f, 25.0f)];
    [backButton setImage:[UIImage imageNamed:@"back_black_btn"]forState: UIControlStateNormal];
    [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backButtonItem;
}

- (void)popViewControllerWithAnimation
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setupButtonImages
{
    [[self.notificationsButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
}


#pragma mark IBAction methods

- (IBAction)notificationButtonClicked:(id)sender{
    
    [super displayNotificationView];
        
}

- (IBAction)diningButtonClicked:(id)sender
{
    [super displayDiningHomeView];
}

- (IBAction)menuButtonClicked:(id)sender
{
    [super menuButtonTapped];
}



@end
